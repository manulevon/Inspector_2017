﻿//-----------------------------------------------------------------------------
//
//       Теоретический модуль
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Highlighter = HighlightingSystem.Highlighter;

/// <summary>
/// Набор классов теоретического модуля
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Клас описывающий елементы
    /// </summary>
    public class Elements : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на сопрограммы
        /// </summary>
        private MyCoroutine _coroutine;
        /// <summary>
        /// Ссылка на аниматор объекта
        /// </summary>
        public Animator PanelTraining;
        /// <summary>
        /// Ссылка на transform объекта-стрелки
        /// </summary>
        public RectTransform Arrow;
        /// <summary>
        /// Ссылка UI Text 
        /// </summary>
        public Text TextButton;
        /// <summary>
        /// Ссылка на кнопку выхода
        /// </summary>
        public Button ButtonExit;
        /// <summary>
        /// Материал которым необходимо подсветить элемент
        /// </summary>
        public Material HigthligthMaterual;
        /// <summary>
        /// Материалы объекта
        /// </summary>
        public Material[][] MassRenderer;
        /// <summary>
        /// Цвет обводки элемента в компоненте Highlaghter
        /// </summary>
        public Color Color;
        /// <summary>
        /// Ссылка на аудиоклип
        /// </summary>
        public AudioSource Magnitofon;
        /// <summary>
        /// Список позиций осмотра элемента
        /// </summary>
        public List<Transform> ListTarget = new List<Transform>();
        /// <summary>
        /// Список целей осмотра
        /// </summary>
        public List<Transform> ListLook = new List<Transform>();
        /// <summary>
        /// Список субтитров
        /// </summary>
        public List<string> ListSubtitle = new List<string>();
        /// <summary>
        /// Список аудио файлов диктора
        /// </summary>
        public List<AudioClip> ListAudiclips = new List<AudioClip>();
        /// <summary>
        /// Необходимость использования дочерних объектов
        /// </summary>
        public List<bool> IsChild = new List<bool>();
        /// <summary>
        /// список объектов которые необходимо подсвечивать
        /// </summary>
        public List<GameObject> ListHightlightObjects = new List<GameObject>();
        /// <summary>
        /// 
        /// </summary>
        public List<bool> IsPosition = new List<bool>();
        /// <summary>
        /// Номер позиции осмотра
        /// </summary>
        private static int numberposition;
        /// <summary>
        /// Ссылка на UIText для субтитров
        /// </summary>
        public Text subtitle;
        /// <summary>
        /// Время, через которое необходимо переключиться на следующий элемент
        /// </summary>
        public float time = 5f;
        /// <summary>
        /// Проверка открытия информационной панели при старте
        /// </summary>
        public static bool kastil;
        /// <summary>
        /// Остановлено проигрование или нет
        /// </summary>
        private bool stop;
        /// <summary>
        /// Завершение модуля теории
        /// </summary>
        public bool isExit;
        /// <summary>
        /// Номер позиции
        /// </summary>
        public int Numberposition
        {
            get { return numberposition; }
            set
            {
                if (value> ListTarget.Count-1 || value<0)
                {
                    //Debug.Log("Выход за пределы массива");
                    stop = true;
                    TextButton.text = "Продолжить";
                    if (value> ListTarget.Count-1)
                    {
                        isExit = true;
                    }
                     return;
                }
                stop = false;
                isExit = false;
                numberposition = value;
            }
        }

        /// <summary>
        /// Метод выполняемы при запуске скрипта до метода Start()
        /// </summary>
        private void Awake()
        {
            numberposition = 0;
            kastil = true;
            _coroutine = new MyCoroutine();
//            PanelTraining.SetTrigger("PanelOpen");
//            StartCoroutine(_coroutine.InvokeCoroutine(.5f, () => { PanelTraining.SetTrigger("PanelOpen"); }));

            //Добавление элементам компонента подсветки
            foreach (var VARIABLE in ListHightlightObjects)
            {
                if (VARIABLE != null)
                    if (VARIABLE.GetComponent<Highlighter>() == null)
                        VARIABLE.AddComponent<Highlighter>();
                if (VARIABLE != null)
                {
                    foreach (var COLLECTION in VARIABLE.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Запуск анимации появления и исчезновения headPanel 
        /// </summary>
        /// <param name="animator">Компонент аниматор с headPanel</param>
        public void ClosePanelTraining(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                PanelTraining.SetTrigger("PanelCloseOn");
            }
            else if (Arrow.localScale.y == 1)
            {
                PanelTraining.SetTrigger("PanelCloseOff");
            }
            else return;
            //Запись в PlayerPrefs номера сцены
            PlayerPrefs.SetInt("Level", 0);
            //Запуск сопрограммы для проигрывания загрузки
            StartCoroutine(_coroutine.InvokeCoroutine(.5f, () => { SceneManager.LoadScene("Loader"); }));
        }

        /// <summary>
        /// Запуск появления и исчезновения навигационного меню
        /// </summary>
        /// <param name="animator">Компонент аниматор с навигационного меню</param>
        public void HelpApDown(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                animator.SetTrigger("PanelDown");
            }
            if (Arrow.localScale.y == 1)
            {
                animator.SetTrigger("PanelUp");
            }
        }

        /// <summary>
        /// Метод выполняемый при запуске скрипта после метода Awake
        /// </summary>
        void Start()
        {
           ChangeCameraState(Numberposition);
            Invoke("Next", ListAudiclips[numberposition].length);
            StartCoroutine(_coroutine.InvokeCoroutine(.2f, () => { PanelTraining.SetTrigger("PanelOpen"); }));
        }

        /// <summary>
        /// Метод выполняемый каждый кадр
        /// </summary>
        private void Update()
        {
            Debugging();
            //Если модуль теории заканчивается появляется кнопка выход в меню
            if (isExit)
            {
                ButtonExit.GetComponent<Animator>().runtimeAnimatorController =
                    Resources.Load("Exit in Menu") as RuntimeAnimatorController;
            }
            else
            {
                ButtonExit.GetComponent<Animator>().runtimeAnimatorController =
                    Resources.Load("Training") as RuntimeAnimatorController;
            }

        }

        /// <summary>
        /// Отладочный метод переключения по элементам
        /// </summary>
        void Debugging() // TODO: for debugging
        {
            if (Input.GetKeyUp(KeyCode.D))
            {
                StopAllCoroutines();
                Magnitofon.Stop();
                CancelInvoke();
                Next();
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                StopAllCoroutines();
                Magnitofon.Stop();
                CancelInvoke();
                Prew();
            }
        }

        /// <summary>
        /// Изменение состояния камеры
        /// </summary>
        /// <param name="numberposition">Номер позиции осмотра</param>
        void ChangeCameraState(int numberposition)
        {
            //Остановка всех сопрограмм
            StopAllCoroutines();
            //Перемещение камеры к необходимой позиции
            StartCoroutine(MoveCamera(ListTarget[numberposition].transform.position));
            //Пворот камеры к элементу
            StartCoroutine(LookCamera(ListLook[numberposition].transform));
            //Вывод на экран субтитров
            PrintSubtitle(ListSubtitle[numberposition]);
            // Запуск озвучки для текущего элемнта
            PlaySound(ListAudiclips[numberposition]);
            // Включените объводки элемента
            OnOutLine(true, ListHightlightObjects[numberposition]);
            // Включение подсветки элемента
            OnHigthligth(IsChild[numberposition],ListHightlightObjects[numberposition], HigthligthMaterual);
        }

        /// <summary>
        /// Переход к следующему элементу
        /// </summary>
        public void Next()
        {

            Numberposition++;

            // Еслипрограмма остановлена не выполнять метод
            if (stop)
            {
                return;
            }
            // Выключение обводки прошлой позиции
            OnOutLine(false, ListHightlightObjects[numberposition - 1]);
            //Выключение подсветки прошлой позиции
            DefaultMaterials(IsChild[numberposition-1],ListHightlightObjects[numberposition - 1]);
            // Изменение состояния камеры
            ChangeCameraState(Numberposition);
            //Расчет времени для перехода на следующую позицию
            float timePlay;
            if (ListAudiclips[numberposition] == null)
            {
                timePlay = time;
            }
            else
            {
                timePlay = ListAudiclips[numberposition].length < time ? time : ListAudiclips[numberposition].length;
            }
            Invoke("Next", timePlay);
        }

        /// <summary>
        /// Переход к предыдущему элементу
        /// </summary>
        public void Prew()
        {
            Numberposition--;
            if (stop)
            {
                return;
            }
            OnOutLine(false, ListHightlightObjects[numberposition + 1]);
            DefaultMaterials(IsChild[numberposition+1],ListHightlightObjects[numberposition + 1]);
            ChangeCameraState(Numberposition);

            float timePlay;
            if (ListAudiclips[numberposition] == null)
            {
                timePlay = time;
            }
            else
            {
                timePlay = ListAudiclips[numberposition].length < time ? time : ListAudiclips[numberposition].length;
            }
            Invoke("Next", timePlay);
        }

        /// <summary>
        /// Выключенире кнопки
        /// </summary>
        /// <param name="button">Кнопка</param>
        public void InactiveButton(Button button)
        {
            //Выключение компонента Button
            button.enabled = false;
        }


        /// <summary>
        /// Кнопка перехода к следующему элементу
        /// </summary>
        public void NextButton()
        {
            TextButton.text = "Пауза";
            StopAllCoroutines();
            Magnitofon.Stop();
            CancelInvoke();
            Next();
        }

        /// <summary>
        /// Кнопка перехода к предыдущему элементу
        /// </summary>
        public void PrewButton()
        {
            TextButton.text = "Пауза";
            StopAllCoroutines();
            Magnitofon.Stop();
            CancelInvoke();
            Prew();
        }

        /// <summary>
        /// Кнопка перехода на следующую позицию
        /// </summary>
        public void NextPositionButton()
        {
            TextButton.text = "Пауза";
            // StopAllCoroutines();
            Magnitofon.Stop();
            CancelInvoke();
            OnOutLine(false, ListHightlightObjects[Numberposition]);
            DefaultMaterials(IsChild[Numberposition],ListHightlightObjects[Numberposition]);
            if (numberposition != ListLook.Count-1)
            {
                for (int i = numberposition + 1; i < ListLook.Count-1; i++)
                {
                    if (IsPosition[i])
                    {
                        Numberposition = i;
                        break;
                    }
                }
            }
            else
            {
                TextButton.text = "Продолжить";
                isExit = true;
                return;
            }

            ChangeCameraState(Numberposition);

            float timePlay;
            if (ListAudiclips[numberposition] == null)
            {
                timePlay = time;
            }
            else
            {
                timePlay = ListAudiclips[numberposition].length < time ? time : ListAudiclips[numberposition].length;
            }
            Invoke("Next", timePlay);

        }

        /// <summary>
        /// Кнопка перехода на предыдущую позицию
        /// </summary>
        public void PrewPositionButton()
        {
            TextButton.text = "Пауза";
//            StopAllCoroutines();
            Magnitofon.Stop();
            CancelInvoke();
            OnOutLine(false, ListHightlightObjects[Numberposition]);
            DefaultMaterials(IsChild[Numberposition],ListHightlightObjects[Numberposition]);
            if (Numberposition != 0)
            {
                for (int i = Numberposition-1; i >= 0; i--)
                {
                    if (IsPosition[i])
                    {
                        Numberposition = i;
                        break;
                    }
                }
            }
            else
            {
                TextButton.text = "Продолжить";

                return;
            }

            ChangeCameraState(Numberposition);

            float timePlay;
            if (ListAudiclips[Numberposition] == null)
            {
                timePlay = time;
            }
            else
            {
                timePlay = ListAudiclips[Numberposition].length < time ? time : ListAudiclips[Numberposition].length;
            }
            Invoke("Next", timePlay);

        }

        /// <summary>
        /// Установка паузы 
        /// </summary> 
        public void StopPlayButton()
        {
            isExit = false;
            if (TextButton.text == "Пауза")
            {
                TextButton.text = "Продолжить";
                Magnitofon.Pause();
                CancelInvoke();

            }
            else
            {
                TextButton.text = "Пауза";
                Magnitofon.Play();
                if (Magnitofon.clip.length < time)
                {
                    Invoke("Next", time-Magnitofon.time);
                }
                else
                {
                    Invoke("Next", Magnitofon.clip.length-Magnitofon.time);
                }

            }
        }

        /// <summary>
        /// Поворот объекта в точку осмотра. Корутина работает без остановок.
        /// </summary>
        /// <param name="target">Объект,куда необходимо смотреть</param>
        /// <returns></returns>
        public IEnumerator LookCamera(Transform target)
        {
            if (target==null)
            {
                yield return null;
            }
            while (true)
            {
                Vector3 relativePos = target.position - Camera.main.transform.position;
                Quaternion rotation = Quaternion.LookRotation(relativePos);
                Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation,rotation,Time.deltaTime * 5);

                yield return null;
            }
        }

        /// <summary>
        /// Перемещает огбъект в необходимую позицию. Корутина выключится, когда долетит
        /// </summary>
        /// <param name="target">Позиция, в которую необходимо переместить объект</param>
        /// <returns></returns>
        public IEnumerator MoveCamera(Vector3 target)
        {
            if (target == null) yield return null;
            var distance = Vector3.Distance(Camera.main.transform.position, target);

            while (distance > 0.05)
            {
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, target, Time.deltaTime*3);
                yield return null;
            }
            yield return null;
        }

        /// <summary>
        /// Вывод на экран субтитров
        /// </summary>
        /// <param name="subtitle">Текст субтитров</param>
        public void PrintSubtitle(string subtitle)
        {
            if (subtitle==null)return;

            this.subtitle.text = subtitle;
        }

        /// <summary>
        /// Запуск озвучки
        /// </summary>
        /// <param name="audioClip">Звуковая дорожка озвучки</param>
        public void PlaySound(AudioClip audioClip)
        {
            if (audioClip ==null)return;

            Magnitofon.clip = ListAudiclips[numberposition];
            Magnitofon.Play();
//            Magnitofon.PlayOneShot(audioClip,1);
        }

        /// <summary>
        /// Включение подсветки объекта (материалом)
        /// </summary>
        /// <param name="isChild">Необходисость включать подсветку дочерних объектов</param>
        /// <param name="obj">Объект который необходимо подсветить</param>
        /// <param name="material">материал подсветки</param>
        public void OnHigthligth(bool isChild,  GameObject obj, Material material)
        {
            MassRenderer = null;
            int size = 0;

            if (obj == null) return;

            if (obj.GetComponent<Renderer>() != null)
            {

                Material[] materials = obj.GetComponent<Renderer>().materials;
                MassRenderer = new Material[1][];
                MassRenderer[0] = new Material[materials.Length];
                for (int i = 0; i < materials.Length; i++)
                {
                    MassRenderer[0][i] = materials[i];
                    materials[i] = material;
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] childs = obj.transform.GetComponentsInChildren<Renderer>();
                Debug.Log(childs.Length);

                if (MassRenderer != null)
                {
                    size++;
                    Material[] mat = MassRenderer[0];
                    MassRenderer = new Material[childs.Length + size][];
                    MassRenderer[0] = new Material[mat.Length];
                    for (int i = 0; i < mat.Length; i++)
                    {
                        MassRenderer[0][i] = mat[i];
                    }
                    Debug.Log(MassRenderer.Length);
                }
                else
                {
                    MassRenderer = new Material[childs.Length + size][];
                }

                for (int i = 0; i < childs.Length; i++)
                {
                    MassRenderer[i+size] = new Material[childs[i].materials.Length];
                    Material[] materials = childs[i].materials;
                    for (int j = 0; j < childs[i].materials.Length; j++)
                    {
                        MassRenderer[i + size][j] = materials[j];
                        materials[j] = material;
                    }
                    childs[i].materials = materials;
                }
            }
        }


        /// <summary>
        /// Возващение материалов в исходное состояние
        /// </summary>
        /// <param name="isChild">Необходисость выключать подсветку дочерних объектов</param>
        /// <param name="obj">Объект у которого необходимо выключить подсветку</param>
        public void DefaultMaterials(bool isChild, GameObject obj)
        {
            if (obj == null) return;

            int size = 0;
            if (obj.GetComponent<Renderer>() != null)
            {
                size++;
                Material[] materials = obj.GetComponent<Renderer>().materials;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i] = MassRenderer[0][i];
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>();
                for (int j = 0; j <  renderers.Length; j++)
                {
                    Material[] materials = renderers[j].materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = MassRenderer[j + size][i];
                    }
                    renderers[j].materials = materials;
                }
            }
        }

        /// <summary>
        /// Включение обводки объекта
        /// </summary>
        /// <param name="isSet">Включить/выключить</param>
        /// <param name="obj">Объект у которого необходимо включить/выключить объект</param>
        public void OnOutLine(bool isSet ,GameObject obj)
        {
            if (obj == null) return;

            obj.GetComponent<Highlighter>().ConstantParams(Color);
            if (isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOn();
            }
            if (!isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOff();
            }
        }
    }
}