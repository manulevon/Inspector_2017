﻿//-----------------------------------------------------------------------------
//
//       Теоретический модуль
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Набор классов теоретического модуля
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Класс реализующий Loader
    /// </summary>
    public class Loader : MonoBehaviour
    {
        /// <summary>
        /// Метод выполняемый при запуске скрипта
        /// </summary>
        void Start()
        {
            //Загрузка сцены 
            SceneManager.LoadSceneAsync(PlayerPrefs.GetInt("Level"));
        }
    }
}
