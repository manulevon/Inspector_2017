﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Аддитивная подгрузка сцены
/// </summary>
public class LoadSceneAddative : MonoBehaviour {

	/// <summary>
    /// Метод вызывается при включении скрипта до метода Старт
    /// </summary>
	void Awake () {
        //Аддитивно подгружает энвайромент в текущую сцену
        SceneManager.LoadScene(5,LoadSceneMode.Additive);
	}
	
	
}
