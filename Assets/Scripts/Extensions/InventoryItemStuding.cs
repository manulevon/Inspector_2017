﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------

using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace Studying
{
    /// <summary>
    /// Описывает позицию (итерацию) в модуле изучения
    /// </summary>
    [System.Serializable]
    public class InventoryItemStuding
    {
        /// <summary>
        /// Номер элемента
        /// </summary>
        public int NumberElement = 0;
        /// <summary>
        /// Осматриваемый объект
        /// </summary>
        public GameObject GameObject = null;
        /// <summary>
        /// Субтитры
        /// </summary>
        public string Subtitle = "";
        /// <summary>
        /// Озвучка
        /// </summary>
        public AudioClip AudioClip = null;
        /// <summary>
        /// Нееобходимость автоплея
        /// </summary>
        public bool IsPlay = false;
        /// <summary>
        /// Подсветка
        /// </summary>
        public GameObject BackLighting = null;
        /// <summary>
        /// Необходимость использоавания дочерних объектов
        /// </summary>
        public bool IsChilds = false;
        /// <summary>
        /// Является ли итерация началом позиции
        /// </summary>
        public bool IsPosition = false;
        /// <summary>
        /// Активный объект
        /// </summary>
        public GameObject ActionObject = null;
        /// <summary>
        /// Позиция на которуюю необходимо переместиться осмотрщику
        /// </summary>
        public GameObject TargetWalk = null;
        /// <summary>
        /// Имя анимации
        /// </summary>
        public string NameAnimationIdle = "";
        /// <summary>
        /// Кнопка
        /// </summary>
        public GameObject ButtonObject = null;
        /// <summary>
        /// Интерактивная Зона
        /// </summary>
        public GameObject ZoneOperation = null;
        /// <summary>
        /// Имя анимации операции
        /// </summary>
        public string NameAnimationOperation = "";
        /// <summary>
        /// Позиция в направлении которой необходимо повернуть объект
        /// </summary>
        public GameObject TargetMonterRotation = null;
        /// <summary>
        /// Дефектный объект
        /// </summary>
        public GameObject Defect = null;
        /// <summary>
        /// Название анимации ходьбы
        /// </summary>
        public string NameAnimationWalk = "";
        /// <summary>
        /// Озвучка дефектного элемента
        /// </summary>
        public AudioClip DefectAudioClip = null;
        /// <summary>
        /// Автоплей на дефектном элементе
        /// </summary>
        public bool DefectIsPlay = false;
        /// <summary>
        /// Суьтитры дефектног оэлемента
        /// </summary>
        public string DefectSubtitle = "";
        /// <summary>
        /// Необходимость использования дочерних объектов дефектного элемента
        /// </summary>
        public bool DefectIsChild = false;
    }
}