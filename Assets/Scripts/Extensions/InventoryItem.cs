﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Описывает позицию для теоретического модуля
    /// </summary>
    [System.Serializable]
    public class InventoryItem
    {
        /// <summary>
        /// Номер элмента
        /// </summary>
        public int numberElement = 0;
        /// <summary>
        /// Осматриваемый элемент
        /// </summary>
        public GameObject gameObject = null;
        /// <summary>
        /// Субтитры
        /// </summary>
        public string subtitle = "";
        /// <summary>
        /// Озвучка
        /// </summary>
        public AudioClip audioClip = null;
        /// <summary>
        /// Подсветка
        /// </summary>
        public GameObject backLighting = null;
        /// <summary>
        /// Необходимость использования дочерних объектов
        /// </summary>
        public bool isChilds = false;
        /// <summary>
        /// Является ли текущая точка позицией
        /// </summary>
        public bool isPosition = false;
        /// <summary>
        /// Дефектный обект 1
        /// </summary>
        public GameObject defect1 = null;
        /// <summary>
        /// Дефектный обект 2
        /// </summary>
        public GameObject defect2 = null;
        /// <summary>
        /// Дефектный обект 3
        /// </summary>
        public GameObject defect3 = null;

//    public string HashGameObject = null;
//    public string HashBackLighting = null;
    }
}