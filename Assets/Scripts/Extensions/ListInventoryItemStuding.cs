﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace Studying
{
    /// <summary>
    /// Хронит данные для модуля изучения
    /// </summary>
    public class ListInventoryItemStuding : ScriptableObject
    {
        /// <summary>
        /// Список позиций (итераций) осмотра
        /// </summary>
        public List<InventoryItemStuding> InventoryItem = new List<InventoryItemStuding>();
    }
}