﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Набор классов для модуля изучения
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Указатель на функцию
    /// </summary>
    public delegate void UiDelegate();

    /// <summary>
    /// Клас реализующий запуск метода с задержкой
    /// </summary>
    public class MyCoroutine
    {
        /// <summary>
        /// Сопрограмма запускающая метод с задержкой
        /// </summary>
        /// <param name="time">Время задержки</param>
        /// <param name="uiDelegate">Функцию, которую необходимо выполнить</param>
        /// <returns></returns>
        public IEnumerator InvokeCoroutine(float time, UiDelegate uiDelegate)
        {
            yield return new WaitForSeconds(time);

            uiDelegate();
        }
    }
}
