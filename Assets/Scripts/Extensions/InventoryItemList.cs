﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Класс предназначен для хронения данных
    /// </summary>
    public class InventoryItemList : ScriptableObject
    {
        /// <summary>
        /// Список объектов
        /// </summary>
        public List<InventoryItem> InventoryItems = new List<InventoryItem>();
    }
}