﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 20/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;
/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Контроллер осмотра объекта
    /// </summary>
    public class OsmotrObjectController : MonoBehaviour
    {
       /// <summary>
       /// Ссылка на текущий осматриваемый объект
       /// </summary>
       public static GameObject OsmotrObject;

        /// <summary>
        /// Осмвтривается объект или нет
        /// </summary>
        private bool _print = false;
        /// <summary>
        /// Время необходимое для осмотра
        /// </summary>
        public static float TimePrint = .5f;
        /// <summary>
        /// Текущее время осмотра
        /// </summary>
        public static float Time = 0;

        /// <summary>
        /// Метод исполняемый каждый кадр пока зажата мышка кликнув по объекту
        /// </summary>
        void OnMouseDrag()
        {
            if (_print)
            {
                if (TimePrint >= Time)
                {
                    //Расчитываем время осмотра
                    Time += UnityEngine.Time.deltaTime;
                }
                else
                {
                    _print = false;
                }

            }

        }

        /// <summary>
        /// Метод испоняемый при клике по объекту
        /// </summary>
        void OnMouseDown()
        {
            OsmotrObject = gameObject;
            _print = true;
        }

        void OnMouseUp()
        {
            Time = 0;
            _print = false;
        }

        void OnMouseExit()
        {
            Time = 0;
            _print = false;
        }

    }
}
