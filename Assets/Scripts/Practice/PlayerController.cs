﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Класс отвечающий за работу игрока
    /// </summary>
    public class PlayerController : MonoBehaviour
    {
        /// <summary>
        /// Цель куда необходимо переместиться
        /// </summary>
        public static Transform TargetPosition;
        /// <summary>
        /// Цель, в которою необходимо смотреть
        /// </summary>
        public static Transform TargetRotation;

        /// <summary>
        /// Компонент NavMeshAgent игрока
        /// </summary>
        private NavMeshAgent agent;

        /// <summary>
        /// Диапозон ближнего взаимодействия 
        /// </summary>
        public float MeleeRange = 3f;
        /// <summary>
        /// Скорость поворота
        /// </summary>
        public float RotationSpeed = 10f;

        /// <summary>
        /// Меиод исполняемый при запуске скрипта до метода старт
        /// </summary>
        void Awake()
        {
            //Получение компонента с объекта
            agent = GetComponent<NavMeshAgent>();
        }

        /// <summary>
        /// Метод исполняющийся каждый кадр
        /// </summary>
        void Update()
        {
            if (TargetPosition != null)
            {
                //Перемещать пользователя в заданую точку по навигационной сетке
                agent.SetDestination(TargetPosition.position);

                // Есле переместильсь в заданую точку
                if (agent.transform.position == new Vector3(TargetPosition.position.x, agent.transform.position.y,
                        TargetPosition.position.z))
                {
                    // Проверка необходимости вращения в нужном напрвлении
                    if (TargetRotation != null)
                    {
                        // Проверка дстанции
                        if (IsInMeleeRangeOf(TargetPosition))
                        {
                            if (!Rotate(TargetRotation))
                            {
                                TargetPosition = null;
                            }
                        }
                    }
                    else
                    {
                        TargetPosition = null;
                    }
                }
            }
        }

        /// <summary>
        /// Проверка дистанции между игроком и точкой в которуюю он перемещается
        /// </summary>
        /// <param name="target"> Цель куда необходимо переместиться</param>
        /// <returns> Возвращает значение, показывающее дистанция меньше значения ближнего взаимодействия или нет</returns>
        bool IsInMeleeRangeOf(Transform target)
        {
            if (target != null)
            {
                float distance = Vector3.Distance(agent.transform.position, target.position);
                return distance < MeleeRange;
            }
            return false;
        }

        /// <summary>
        /// Поворот объекта в нужное направление
        /// </summary>
        /// <param name="target">Цель, в направлении которой необходимо повернуть объект</param>
        /// <returns>Возвращает булево значение, поворачивается объект или нет</returns>
        bool Rotate(Transform target)
        {
            Vector3 direction = (target.position - agent.transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            Vector3 targetrotation =
                new Vector3(agent.transform.eulerAngles.x, lookRotation.eulerAngles.y, lookRotation.eulerAngles.z);
            Quaternion quaternion = Quaternion.Euler(agent.transform.eulerAngles.x, lookRotation.eulerAngles.y,
                lookRotation.eulerAngles.z);

            if (agent.transform.eulerAngles != targetrotation)
//        if (agent.transform.rotation != quaternion)
            {
//            agent.transform.eulerAngles =
//                Vector3.MoveTowards(agent.transform.eulerAngles, targetrotation, Time.deltaTime * 200);
                agent.transform.rotation =
                    Quaternion.RotateTowards(agent.transform.rotation, quaternion, Time.deltaTime * 200);
                return true;
            }
            return false;
        }
    }
}
