﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Класс, отвечающий за работу звуковых дорожек
    /// </summary>
    public class PlaySoundController : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на компонент AudioSource
        /// </summary>
        public AudioSource AudioSource;
        /// <summary>
        /// Ссылка на аудио клип
        /// </summary>
        public AudioClip AudioClip;
        /// <summary>
        /// Звук удара молотком
        /// </summary>
        public AudioClip SoundMolotok;
        /// <summary>
        /// Ссылка на аниматор поводка
        /// </summary>
        public Animator PovodokAnimator;
        /// <summary>
        /// Молоток
        /// </summary>
        public GameObject Molotok;

        /// <summary>
        /// Метод, выполняемый при включении скрипта до метода Старт 
        /// </summary>
        void Awake()
        {
            AudioSource =
                GetComponent<AudioSource>();
        }
        
        /// <summary>
        /// Запуск анимации и звука поводка
        /// </summary>
        public void PlaySound()
        {
            // 
            PovodokAnimator.SetTrigger("Begin");
            AudioSource.clip = AudioClip;
            AudioSource.Play();
        }

        /// <summary>
        ///  Запуск конечной анимации 
        /// </summary>
        public void EndAnimation()
        {
            PovodokAnimator.SetTrigger("End");
        }

        /// <summary>
        /// Запуск аудио клипа удара молотка
        /// </summary>
        public void PlaySoundMolotok()
        {
            AudioSource.clip = SoundMolotok;
            AudioSource.Play();
        }

        /// <summary>
        /// Запуск анимации удара молотка
        /// </summary>
        public void BeginAnimMolotok()
        {
            Molotok.SetActive(true);
        }

        /// <summary>
        /// Выключение анимации молотка
        /// </summary>
        public void EndAnimMolotok()
        {
            Molotok.SetActive(false);
        }

    }
}
