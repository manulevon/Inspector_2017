﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 20/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Highlighter = HighlightingSystem.Highlighter;
using newLogic;

/// <summary>
/// Набор классов для модуля изучения
/// </summary>
namespace Studying
{
    /// <summary>
    /// Главный контроллер модуля изучения
    /// </summary>
    public class GameController : MonoBehaviour
    {
        #region Variabres and Properties////////////////////////////////////////////////////

        /// <summary>
        /// Аудио контроллер
        /// </summary>
        public PlaySoundController PlaySoundController;

        /// <summary>
        /// Текстура курсора
        /// </summary>
        public Texture2D DefaultCursore;
        
        /// <summary>
        /// Список курсоров текстур
        /// </summary>
        /// Не используется
        public List<Texture2D> ListCursores = new List<Texture2D>();

        /// <summary>
        /// Цвет для подсветки элементов
        /// </summary>
        public Color Color;

        /// <summary>
        /// Цвет для подсветки материала (точек перемещения)
        /// </summary> 
        public Color ColorEmission = new Color(1,0,1,1);
        
        /// <summary>
        /// Ссылка на компонент Transform игрока
        /// </summary>
        public Transform Monter;
        
        /// <summary>
        /// материал для покраски элементов
        /// </summary>
        public Material HigthligthMaterual;
        /// <summary>
        /// материал для подстветки объекта зоны 
        /// </summary>
        public Material FadeMaterial;
        /// <summary>
        /// массив дефолтных материалов для возвтрата материалов окрашенного элемента 
        /// </summary>
        public Material[][] MassRenderer;
        /// <summary>
        /// ссылка на audioSource контроллера игры 
        /// </summary>
        public AudioSource Magnitofon;
        /// <summary>
        /// лист трансформов для подлета камеры 
        /// </summary>
        public List<Transform> ListTarget = new List<Transform>();
        /// <summary>
        /// лист трансформов для наблюдения камеры
        /// </summary>
        public List<Transform> ListLook = new List<Transform>();
        /// <summary>
        /// лист субтитров
        /// </summary>
        public List<string> ListSubtitle = new List<string>();
        /// <summary>
        /// лист аудиоклипов 
        /// </summary>
        public List<AudioClip> ListAudiclips = new List<AudioClip>();
        /// <summary>
        /// лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации 
        /// </summary>
        public List<bool> ListIsPlayThenEmpty = new List<bool>();
        /// <summary>
        /// лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента
        /// </summary>
        public List<bool> ListIsChild = new List<bool>();
        /// <summary>
        /// лист объектов, которых нужно красить и подсвечивать 
        /// </summary>
        public List<GameObject> ListHightlightObjects = new List<GameObject>();
        /// <summary>
        /// лист тоглсов для определения позиции 
        /// </summary>
        public List<bool> ListIsPosition = new List<bool>();
        /// <summary>
        /// лист объектов для активации действий 
        /// </summary>
        public List<GameObject> ListActionObjects = new List<GameObject>();
        /// <summary>
        /// лист трансформов для подхода осмотрщика
        /// </summary>
        public List<Transform> ListTargetWalk = new List<Transform>();
        /// <summary>
        /// лист названий анимации перемещения осмотрщика
        /// </summary>
        public List<string> ListNameAnimationWalk = new List<string>();
        /// <summary>
        /// лист названий idle анимаций осмотрщика 
        /// </summary>
        public List<string> ListNameAnimationIdle = new List<string>();
        /// <summary>
        /// литс трансформов для определения направления обзора осмотрщика 
        /// </summary>
        public List<Transform> ListTransformRotation = new List<Transform>(); 
        /// <summary>
        /// Список кнопок
        /// </summary>
        public List<GameObject> ListButtonObject = new List<GameObject>();
        /// <summary>
        /// Список зон с операциями
        /// </summary>
        public List<GameObject> ListZoneOperations = new List<GameObject>();
        /// <summary>
        /// лист названий анимаций осмотрщика для выполнения каких либо действий 
        /// </summary>
        public List<string> ListNameAnimation = new List<string>(); 
        /// <summary>
        /// Список дефектов
        /// </summary>
        public List<GameObject> ListDefects = new List<GameObject>();
        /// <summary>
        /// лист аудиослипов сопровождающий демострацию дефектов 
        /// </summary>
        public List<AudioClip> ListDefectAudioClips = new List<AudioClip>();
        /// <summary>
        /// лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации 
        /// </summary>
        public List<bool> ListDefectIsPlayThenEmpty = new List<bool>();
        /// <summary>
        /// лист субтитров сопровождающий демонстрацию дефектов
        /// </summary>
        public List<string> ListDefectSubtitles = new List<string>();
        /// <summary>
        /// // лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента дефекта
        /// </summary>
        public List<bool> ListDefectIsChilds = new List<bool>();
        /// <summary>
        /// сслыка на текст для отображения субтитров 
        /// </summary>
        public Text Subtitle;
        /// <summary>
        /// время ожидания если не укзан аудиоклип 
        /// </summary>
        public float Time = 5f; 
        /// <summary>
        /// Текстура лоадера осмотра
        /// </summary>
        public Image Eye;
        /// <summary>
        /// 
        /// </summary>
        public static bool Kastil;
        /// <summary>
        /// Не используется
        /// </summary>
        public bool IsExit; 

        /// <summary>
        /// Минимальное значение используемое для мигания подсветки
        /// </summary>
        public float min = .5f;
        /// <summary>
        /// Максимальное значение ипользуемое для мигания подсветки
        /// </summary>
        public float max = 2;

        /// <summary>
        /// Шаг мигания подсветки
        /// </summary>
        public float step = .05f;
        /// <summary>
        /// переменная для расчета шага при мигании 
        /// </summary>
        public float value = 0;
        /// <summary>
        /// начальное состояние 
        /// </summary>
        public static State StateController = State.Flay;

        /// <summary>
        /// перечислитель состояний
        /// </summary>
        public enum State
        {
            Flay,
            Inspector,
            Movement,
            Operation,
            DisplayingDefect
        } 

        /// <summary>
        /// Номер позиции
        /// </summary>
        public int Numberposition
        {
            get { return _numberposition; }
            set
            {
                if (value > ListTarget.Count - 1 || value < 0)
                {
                    Debug.Log("Выход за пределы массива");
                    _stop = true;
                    if (value > ListTarget.Count - 1)
                    {
                        IsExit = true;
                    }
                    return;
                }
                _stop = false;
                IsExit = false;
                _numberposition = value;
            }
        }

       
        // ссылка на аниматор осмотрщика 
        private Animator _animatorMonter;
        // экземпляр класса своей карутины 
        private MyCoroutine _myCoroutine;
        // название текущей анимации хотьбы
        private string _currentAnimationWalk = null; 
        // Имя текущей анимации
        private string _currentAnimation = null;
        // трасформ текущего осматриваемого элемента
        private Transform _currentLookObject = null;
        // ссылка на создаваемый дефект
        private GameObject _pref = null;
        // итератор
        private int _numberposition; 
        // Не используется
        private bool _kastil;
        // Не используется
        private bool _stop; 


        #endregion /////////////////////////////////////////////////////////////

        #region Method Awake ////////////////////////////////////////////////////

        /// <summary>
        /// Метод выполняемый при запуске скрипта до метода Start
        /// </summary>
        private void Awake()
                {
                    // Установка курсора
                    SetCustomCursor("");
                    
                    // Получение компонента - контроллера звука
                    PlaySoundController = Monter.GetComponent<PlaySoundController>();
                    // Получение компонента аниматора
                    _animatorMonter = Monter.gameObject.GetComponent<Animator>();
                    // Создание объекта в ItemController
                    ItemController.CliclObject = new GameObject();
                    // Создание объекта в OsmotrObjectController
                    OsmotrObjectController.OsmotrObject = new GameObject();
                    // Создание экземпляра класса MyCoroutine
                    _myCoroutine = new MyCoroutine();
                    // Инициализация номера позиции
                    _numberposition = 0;
                    //
                    Kastil = true;

                    // Заполнение списка точек осмотра
                    foreach (Transform Look in ListLook)
                    {
                        if (Look == null)
                        {
                            ListTarget.Add(null);
                            continue;
                        }
                        ListTarget.Add(Look.transform.GetChild(0));
                    }

                    // Заполнение списка компонентами подсветок
                    foreach (var VARIABLE in ListHightlightObjects)
                    {
                        if (VARIABLE == null) continue;
                        if (VARIABLE.GetComponent<Highlighter>() == null)
                            VARIABLE.AddComponent<Highlighter>();
                        if (VARIABLE.GetComponent<OsmotrObjectController>() == null)
                            VARIABLE.AddComponent<OsmotrObjectController>();
                        if (VARIABLE.GetComponents<Collider>().Length != 0)
                        {
                            foreach (var COLLECTION in VARIABLE.GetComponents<Collider>())
                            {
                                COLLECTION.enabled = false;
                            }
                        }
                    }

                    // Заполнение листа активными объектами
                    foreach (var COLLECTION in ListActionObjects)
                    {
                        if (COLLECTION != null)
                        {
                            COLLECTION.gameObject.AddComponent<ItemController>();
                            COLLECTION.gameObject.SetActive(false);
                        }

                    }
                    // Заполнение списка компонентами подсветки дефектных объектов
                    foreach (var VARIABLE in ListDefects)
                    {
                        if (VARIABLE == null) continue;
                        if (VARIABLE.GetComponent<Highlighter>() == null)
                            VARIABLE.AddComponent<Highlighter>();
                    }
                }

        #endregion //////////////////////////////////////////////////////////////

        void Start()
        {
            ChangeState();
        }

        #region State Ispector ///////////////////////////////////////////

        /// <summary>
        /// Метод для осмотра текущего элемента с сопровождением субтитров, звукового ряда, подсветки и покраски
        /// </summary>
        /// <param name="isPlay">тогл, который определяет нужно ли ожидать время, если не указан аудиоклип</param>
        void Inspector(bool isPlay)
        {
            PlaySound(ListAudiclips[Numberposition]);
            PrintSubtitle(ListSubtitle[Numberposition]);
            OnOutLine(true, ListHightlightObjects[Numberposition]);
            OnHigthligth(ListIsChild[Numberposition],ListHightlightObjects[Numberposition], HigthligthMaterual);
            if (ListHightlightObjects[Numberposition] != null)
            {
                _currentLookObject = ListHightlightObjects[Numberposition].GetComponent<Transform>();
            }
            float timePlay = 0;
            if (isPlay)
            {
                if (ListAudiclips[Numberposition] != null)
                {
                    timePlay = ListAudiclips[Numberposition].length < Time ? Time: ListAudiclips[Numberposition].length;
                }
                else
                {
                    timePlay = Time;
                }
            }
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Flay);}));

            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { StartCoroutine(Osmotr(ListHightlightObjects[Numberposition])); }));
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Movement);}));
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { Osmotr(ListHightlightObjects[Numberposition]);}));
        }

        /// <summary>
        /// Сопрограмма запускается при осмотре элемента
        /// </summary>
        /// <param name="osmotrObject">Осматриваемый объект</param>
        /// <returns></returns>
        IEnumerator Osmotr(GameObject osmotrObject)
        {
//            ItemController.CliclObject = new GameObject();

//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
            if (osmotrObject == null)
            {
                yield return null;
            }
            else
            {
                if (osmotrObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in osmotrObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = true;
                    }
                }
//                Eye.transform.position = Camera.main.WorldToScreenPoint(osmotrObject.transform.position);
//                Eye.transform.position = Input.mousePosition;
                while (Eye.fillAmount < 1)
                {

                    if (OsmotrObjectController.OsmotrObject == osmotrObject)
                    {
                        Eye.transform.position = Input.mousePosition;
                        Eye.fillAmount = OsmotrObjectController.Time / OsmotrObjectController.TimePrint;
                    }
                    else
                        Eye.fillAmount = 0;
                    yield return null;
                }
                if (osmotrObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in osmotrObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
                Eye.fillAmount = 0;
                OnOutLine(false, ListHightlightObjects[Numberposition]);
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);
            }
            StateController = State.Movement;
            ChangeState();
            yield return null;
        }

        #endregion ///////////////////////////////////////////////////////

        /// <summary>
        /// Метод, который вызывается после завершения определенных состояний
        /// </summary>
        /// <param name="state">Состояние, в которое необходимо перейти</param>
        void EndTime(State state)
        {
            StateController = state;
            if (state == State.Flay)
            {
                if (_pref != null)
                {
                    Destroy(_pref);
                    _pref = null;
                    if (_currentLookObject != null)
                    {
                        if (_currentLookObject.gameObject.GetComponent<Renderer>() != null)
                            _currentLookObject.gameObject.GetComponent<Renderer>().enabled = true;
                        if (_currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>().Length != 0)
                        {
                            foreach (var COLLECTION in _currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>())
                            {
                                COLLECTION.enabled = true;
                            }
                        }
                    }
                }
                Numberposition++;
            }

            if (state == State.DisplayingDefect)
            {
                GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
            }
            ChangeState();
        }

        #region State Movement ///////////////////////////////////////////////////

        /// <summary>
        /// Сопрограмма для определения того, когда пользователь начнет взаимодействовать с нужным объектом
        /// </summary>
        /// <param name="actionObject">Объект, при взаимодействии с которым начнет происходить перемещение осмотрщика</param>
        /// <returns></returns>
        IEnumerator Movement(GameObject actionObject)
        {
//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);

            ItemController.CliclObject = new GameObject();

            if (actionObject == null)
            {
                yield return null;
            }
            else
            {
                actionObject.gameObject.SetActive(true);
                while (ItemController.CliclObject != actionObject)
                {
//                    HightlightClickObject(actionObject.GetComponent<Renderer>());
                    yield return null;
                }
//                actionObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(0, 0, 0, 1));
                actionObject.gameObject.SetActive(false);
            }
            yield return StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);

            StateController = State.Operation;
            ChangeState();
        }

        /// <summary>
        /// Метод для осуществления мигания объекта, с которым должно произойти взаимодействие
        /// </summary>
        /// <param name="renderer">Рендер объекта, с которым должно произойти взаимодействие</param>
        void HightlightClickObject(Renderer renderer)
        {
            float amount = min;
            value += step;
            float sin = Mathf.Sin(value);
            sin = (sin + 1) / 2;
            amount += sin * (max - min);
            Color color = ColorEmission * amount;
            renderer.material.SetColor("_EmissionColor", color);
            DynamicGI.SetEmissive(renderer, color);
        }

        /// <summary>
        /// Сопрограмма для осуществления перемещения осмотрщика
        /// </summary>
        /// <param name="targetPosition">Трансформ куда нужно подойти</param>
        /// <param name="targetRotation"> В каком направлении смотреть</param>
        /// <returns></returns>
        IEnumerator Move(Transform targetPosition, Transform targetRotation)
        {
            if (targetPosition == null)
            {
                if (targetRotation != null)
                {
                    PlayerController.TargetRotation = targetRotation;
                    yield return null;
                }
                yield return null;
            }
            else
            {
                PlayerController.TargetPosition = targetPosition;
                PlayerController.TargetRotation = targetRotation;
                if (Monter.transform.position != new Vector3(targetPosition.position.x, Monter.transform.position.y,
                        targetPosition.position.z))
                {
                    GetAnimationWalk(ListNameAnimationWalk[_numberposition]);
//                    if (_currentAnimationWalk != null)
//                    {
//                        if (_currentAnimation != _currentAnimationWalk)
//                        {
//                            _animatorMonter.SetTrigger(_currentAnimationWalk);
//                            _currentAnimation = _currentAnimationWalk;
//                        }
//
//                    }

                }
                while (PlayerController.TargetPosition != null)
                {
                    print("Move");
                    yield return null;
                }
            }
            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
//
//            StateController = State.Operation;
//            ChangeState();
        }

        #endregion /////////////////////////////////////////////////////////

        /// <summary>
        /// Метод для получения названия анимации ходьбы, которую нужно использовать
        /// </summary>
        /// <param name="name">Название анимации</param>
        void GetAnimationWalk(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return;
            }
            if (_currentAnimation != name)
            {
                _currentAnimation = name;
                _animatorMonter.SetTrigger(name);
            }
        }

        #region State Operation /////////////////////////////////////////////////////

        /// <summary>
        /// Сопрограмма аутивации зоны
        /// </summary>
        /// <param name="button">Объект который необходимо активировать</param>
        /// <returns></returns>
        IEnumerator Operation(GameObject button)
        {
            ButtonObjectController.ClicButton = new GameObject();
            if (button == null)
            {
                yield return null;
            }
            else
            {
                while (button != ButtonObjectController.ClicButton)
                {
                    yield return null;
                }
                SetCustomCursor(button.transform.GetChild(1).GetComponent<Text>().text);
            }
            StartCoroutine(ClickZone(ListZoneOperations[Numberposition]));
        }

        /// <summary>
        /// Сопрограмма работы зон
        /// </summary>
        /// <param name="zone">Объект на который необходимо нажать</param>
        /// <returns></returns>
        IEnumerator ClickZone(GameObject zone)
        {
            ZoneController.CliclObject = new GameObject();
            if (zone == null)
            {
                yield return null;
            }
            else
            {
                zone.GetComponent<Collider>().enabled = true;
                zone.transform.GetChild(0).gameObject.SetActive(true);
                Material[][] materialsDefault;
                GetMaterialsDefault(zone.transform.GetChild(0).gameObject, out materialsDefault);
                while (zone != ZoneController.CliclObject)
                {
                    FlashingObjectWithZone(zone.transform.GetChild(0).gameObject, FadeMaterial);
                    yield return null;
                }

                zone.GetComponent<Collider>().enabled = false;
                zone.transform.GetChild(0).gameObject.SetActive(false);

                GetDefaultMaterials(zone.transform.GetChild(0).gameObject, materialsDefault);
                SetCustomCursor("");
            }
            yield return StartCoroutine(OperationAnimation(ListNameAnimation[Numberposition]));
            EndTime(State.DisplayingDefect);

        }

        #region ChangeMaterial and Cursore /////////////////////////////////////////////////

        /// <summary>
        /// Получение материалов объекта
        /// </summary>
        /// <param name="flashingObject">Объект который необходимо подсветить</param>
        /// <param name="materialsDefault"> Текущие материалы объекта</param>
        void GetMaterialsDefault(GameObject flashingObject, out Material[][] materialsDefault)
        {
            materialsDefault = null;
            int size = 0;

            if (flashingObject != null)
            {
                if (flashingObject.GetComponent<Renderer>() != null)
                {
                    materialsDefault = new Material[1][];
                    Material[] materials = flashingObject.GetComponent<Renderer>().materials;
                    materialsDefault[0] = new Material[materials.Length];
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materialsDefault[0][i] = materials[i];
                    }
                }
                Renderer[] childs = flashingObject.transform.GetComponentsInChildren<Renderer>();

                if (childs != null)
                {
                    if (materialsDefault != null)
                    {
                        size++;
                        Material[] mat = materialsDefault[0];
                        materialsDefault = new Material[childs.Length + size][];
                        materialsDefault[0] = new Material[mat.Length];
                        for (int i = 0; i < mat.Length; i++)
                        {
                            materialsDefault[0][i] = mat[i];
                        }
                    }
                    else
                    {
                        materialsDefault = new Material[childs.Length + size][];
                    }

                    for (int i = 0; i < childs.Length; i++)
                    {
                        materialsDefault[i + size] = new Material[childs[i].materials.Length];
                        Material[] materials = childs[i].materials;
                        for (int j = 0; j < childs[i].materials.Length; j++)
                        {
                            materialsDefault[i + size][j] = materials[j];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Подсветка зоны
        /// </summary>
        /// <param name="flashingObject">Объект, который необходимо подсветить</param>
        /// <param name="material">Материал подсветки</param>
        void FlashingObjectWithZone(GameObject flashingObject, Material material)
        {
            if (flashingObject != null)
            {
                if (flashingObject.GetComponent<Renderer>() != null)
                {
                    Material[] materials = flashingObject.GetComponent<Renderer>().materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = material;
                        materials[i].color = Color.Lerp(Color.cyan, Color.clear,
                            Mathf.PingPong(UnityEngine.Time.time, 1));
                    }
                    flashingObject.GetComponent<Renderer>().materials = materials;
                }
                var a = flashingObject.GetComponentsInChildren<Renderer>();
                for (int i = 0; i < a.Length; i++)
                {
                    Material[] materials = a[i].materials;
                    for (int j = 0; j < materials.Length; j++)
                    {
                        materials[i] = material;
                        materials[i].color = Color.Lerp(Color.cyan, Color.clear,
                            Mathf.PingPong(UnityEngine.Time.time, 1));
                    }
                    a[i].materials = materials;
                }
            }
        }

        /// <summary>
        /// Устанавливаем стандартные материал
        /// </summary>
        /// <param name="obj">Объект которому необходиме выставитть стандартные материалы</param>
        /// <param name="materialsDefault">Стандартные материалы</param>
        void GetDefaultMaterials(GameObject obj, Material[][] materialsDefault)
        {
            if (obj == null) return;

            int size = 0;
            if (obj.GetComponent<Renderer>() != null)
            {
                size++;
                Material[] materials = obj.GetComponent<Renderer>().materials;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i] = materialsDefault[0][i];
                }
                obj.GetComponent<Renderer>().materials = materials;
            }
            var a = obj.GetComponentsInChildren<Renderer>();
            if (a != null)
            {
                Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>();
                for (int j = 0; j < renderers.Length; j++)
                {
                    Material[] materials = renderers[j].materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = materialsDefault[j + size][i];
                    }
                    renderers[j].materials = materials;
                }
            }
        }

        /// <summary>
        /// Установка курсора
        /// </summary>
        /// <param name="buttonText">Номер курсора</param>
        void SetCustomCursor(string buttonText)
        {
            switch (buttonText)
            {
                case "0":
                    Cursor.SetCursor(ListCursores[0], Vector2.zero, CursorMode.Auto);
                    break;
                case "1":
                    Cursor.SetCursor(ListCursores[1], Vector2.zero, CursorMode.Auto);
                    break;
                case "2":
                    Cursor.SetCursor(ListCursores[2], Vector2.zero, CursorMode.Auto);
                    break;
                case "3":
                    Cursor.SetCursor(ListCursores[3], Vector2.zero, CursorMode.Auto);
                    break;
                case "4":
                    Cursor.SetCursor(ListCursores[4], Vector2.zero, CursorMode.Auto);
                    break;
                case "5":
                    Cursor.SetCursor(ListCursores[5], Vector2.zero, CursorMode.Auto);
                    break;
                default:
                    Cursor.SetCursor(DefaultCursore, Vector2.zero, CursorMode.Auto);
                    break;
            }
        }

        #endregion /////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Сопрограмма для осуществления анимации текущей операции осмотрщиком
        /// </summary>
        /// <param name="nameAnimation"> Название анимации</param>
        /// <returns></returns>
        IEnumerator OperationAnimation(string nameAnimation)
        {
            PlaySoundController.AudioSource.Stop(); // TODO
            float timeAnimation = 0;

            if (string.IsNullOrEmpty(nameAnimation))
            {
//                _kastil = true;
//                yield return null;
            }
            else
            {
//                Monter.GetComponent<Animator>().SetTrigger(nameAnimation);
                GetAnimationWalk(nameAnimation);
                Debug.Log(nameAnimation);

                yield return new WaitForSeconds(.1f);

                timeAnimation = Monter.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
                Debug.Log(timeAnimation);

                yield return new WaitForSeconds(timeAnimation - .1f);
            }
            Debug.Log("END");
            yield return null;

//            StartCoroutine(
//                _myCoroutine.InvokeCoroutine(timeAnimation - .1f, () => { EndTime(State.DisplayingDefect); }));
        }

        #endregion ///////////////////////////////////////////////////////////////////

        /// <summary>
        /// Метод для получения idle анимации
        ///  </summary>
        /// <param name="nameAnimation"> Название анимации</param>
        void GetAnimationIdle(string nameAnimation)
        {
            if (string.IsNullOrEmpty(nameAnimation))
            {
                return;
            }
            if (_currentAnimation != nameAnimation)
            {
                _currentAnimation = nameAnimation;
                _animatorMonter.SetTrigger(nameAnimation);
            }

        }

        #region State DisplayingDefect ///////////////////////////////////////

        /// <summary>
        /// Метод для демонстрации дефектов
        /// </summary>
        /// <param name="isPlay"> Тогл, который определяет нужно ли ожидать заданное время, если не указан аудиоклип</param>
        void DisplayinDefect(bool isPlay)
        {
            PlaySound(ListDefectAudioClips[Numberposition]);
//            Numberposition++;
//            if (_stop)
//            {
//                return;
//            }
//            OnOutLine(false, ListHightlightObjects[_numberposition - 1]);
//            DefaultMaterials(ListIsChild[_numberposition-1],ListHightlightObjects[_numberposition - 1]);
            PrintSubtitle(ListDefectSubtitles[Numberposition]);
            InstantiateDefect(ListDefects[Numberposition]);
            float timePlay = 0f;
            if (ListDefectAudioClips[Numberposition] != null)
            {
                timePlay = ListDefectAudioClips[Numberposition].length < Time ? Time : ListDefectAudioClips[Numberposition].length;
            }
            else
            {
                if (isPlay)
                    timePlay = Time;
            }
            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Flay);}));
        }

        /// <summary>
        /// Метод для замены текущего элемнта на элеиент с дефектом
        /// </summary>
        /// <param name="pref">Дефектный элемент</param>
        void InstantiateDefect(GameObject pref)
        {
            if (pref != null)
            {
                if (_currentLookObject != null)
                {
                    if (_currentLookObject.gameObject.GetComponent<Renderer>() != null)
                        _currentLookObject.gameObject.GetComponent<Renderer>().enabled = false;
                    if (_currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>().Length != 0)
                    {
                        foreach (var COLLECTION in _currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>())
                        {
                            COLLECTION.enabled = false;
                        }
                    }
                    _pref = Instantiate(pref, _currentLookObject.transform.position, pref.transform.rotation);
                    OnOutLine(true, _pref);
                }
            }
        }

        #endregion ////////////////////////////////////////////////////////////

        #region Method ChangeState ///////////////////////////////////
        /// <summary>
        /// Метод для переключения состояния
        /// </summary>
        void ChangeState()
        {
            if (StateController == State.Flay)
            {
                Subtitle.text = "";
                StartCoroutine(MoveCamera(ListTarget[Numberposition]));
                StartCoroutine(LookCamera(ListLook[Numberposition]));
                return;
            }

            if (StateController == State.Inspector)
            {
                Inspector(ListIsPlayThenEmpty[Numberposition]);
                return;
            }

            if (StateController == State.Movement)
            {
                StopAllCoroutines();
                StartCoroutine(Movement(ListActionObjects[Numberposition]));
                return;
            }

            if (StateController == State.Operation)
            {
                StartCoroutine(Operation(ListButtonObject[Numberposition]));
            }

            if (StateController == State.DisplayingDefect)
            {
                DisplayinDefect(ListDefectIsPlayThenEmpty[Numberposition]);
            }
        }
        #endregion //////////////////////////////////////////////////////

       
        private void Update()
        {
            Debugging();
//            ChangeState();
        }

        /// <summary>
        /// Метод используемый для отладки
        /// </summary>
        void Debugging() // TODO: for debugging
        {
            if (Input.GetKeyUp(KeyCode.D))
            {
                ButtonNext();
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                ButtonPrew();
            }
        }

        /// <summary>
        /// Переход к следующему шагу
        /// </summary>
        public void ButtonNext()
        {
            if (ListActionObjects[Numberposition])
                ListActionObjects[Numberposition].SetActive(false);

            OnOutLine(false, ListHightlightObjects[Numberposition]);
            if ( MassRenderer!= null)
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);

            OnOutLine(false, ListDefects[Numberposition]);

            if (ListZoneOperations[Numberposition])
            {
                ListZoneOperations[Numberposition].GetComponent<Collider>().enabled = false;
                ListZoneOperations[Numberposition].transform.GetChild(0).gameObject.SetActive(false);
            }

            StopAllCoroutines();
            Magnitofon.Stop();
            PlaySoundController.AudioSource.Stop(); // TODO
            CancelInvoke();
            StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

            int number = Numberposition;

            StartCoroutine(CheckPosition(number));

            Numberposition++;
            StateController = State.Flay;
            ChangeState();
        }

        /// <summary>
        /// Переход к предыдущему шагу
        /// </summary>
        public void ButtonPrew()
        {
            if (ListActionObjects[Numberposition])
                ListActionObjects[Numberposition].SetActive(false);

            OnOutLine(false, ListHightlightObjects[Numberposition]);
            if ( MassRenderer!= null)
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);

            OnOutLine(false, ListDefects[Numberposition]);

            if (ListZoneOperations[Numberposition])
            {
                ListZoneOperations[Numberposition].GetComponent<Collider>().enabled = false;
                ListZoneOperations[Numberposition].transform.GetChild(0).gameObject.SetActive(false);
            }

            StopAllCoroutines();
            Magnitofon.Stop();
            PlaySoundController.AudioSource.Stop(); // TODO
            CancelInvoke();
            StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

            int number = Numberposition-2;


            StartCoroutine(CheckPosition(number));

            Numberposition--;
            StateController = State.Flay;
            ChangeState();
        }

        /// <summary>
        /// Сопрограмма проверки позиции
        /// </summary>
        /// <param name="number">Номер позиции</param>
        /// <returns></returns>
        IEnumerator CheckPosition(int number)
        {
            while (Monter.transform.position != new Vector3(PlayerController.TargetPosition.position.x,
                       Monter.transform.position.y, PlayerController.TargetPosition.position.z))
            {
                yield return null;
            }
            if (number == 0 || number == ListNameAnimation.Count) // TODO : check true or false
            {
                yield break;
            }
            string nameAnimationOperation = ListNameAnimation[number];

            if (string.IsNullOrEmpty(nameAnimationOperation))
            {
                nameAnimationOperation = ListNameAnimationIdle[number];
            }
            yield return null;

             StartCoroutine(OperationAnimation(nameAnimationOperation));

        }

        // не используется
        public void InactiveButton(Button button)
        {
            button.enabled = false;
        }

        #region Look and Move //////////////////////////////////////////////////////

        /// <summary>
        /// Сопрограмма поворота объекта в необходимое направление
        /// </summary>
        /// <param name="target">Точка в которую необходимо смотреть смотреть</param>
        /// <returns></returns>
        public IEnumerator LookCamera(Transform target)
        {
            if (target == null)
            {
                yield return null;
            }
            else
            {
                while (true)
                {
                    Vector3 relativePos = target.position - Camera.main.transform.position;
                    Quaternion rotation = Quaternion.LookRotation(relativePos);
                    Camera.main.transform.rotation =
                        Quaternion.Lerp(Camera.main.transform.rotation, rotation, UnityEngine.Time.deltaTime * 5);
                    //                Camera.main.transform.rotation =
                    //                    Quaternion.RotateTowards(Camera.main.transform.rotation, rotation, Time.deltaTime * 150);
                    yield return null;
                }
            }
        }

        /// <summary>
        /// Перемещение объекта в точку
        /// </summary>
        /// <param name="target">Точка в которую необходимо переместить объект</param>
        /// <returns></returns>
        public IEnumerator MoveCamera(Transform target)
        {
            if (target == null)
            {
                yield return null;
            }
            else
            {
                var distance = Vector3.Distance(Camera.main.transform.position, target.position);

                while (distance > 0.1f)
                {
                    distance = Vector3.Distance(Camera.main.transform.position, target.position);
                    Camera.main.transform.position =
                        Vector3.Lerp(Camera.main.transform.position, target.position, UnityEngine.Time.deltaTime * 3);
                    yield return null;
                }
            }
            StateController = State.Inspector;
            ChangeState();
        }

        #endregion /////////////////////////////////////////////////////////////////

        /// <summary>
        /// Вывод субтитров на экран
        /// </summary>
        /// <param name="subtitle"> Субтитры</param>
        public void PrintSubtitle(string subtitle)
        {
            if (subtitle==null)return;

            this.Subtitle.text = subtitle;
        }

        /// <summary>
        /// Запуск озвучки
        /// </summary>
        /// <param name="audioClip">Звуковая дорожка</param>
        public void PlaySound(AudioClip audioClip)
        {
            if (audioClip ==null)return;

            Magnitofon.clip = audioClip;
            Magnitofon.Play();
        }

        #region Hightlight /////////////////////////////////////////////////

        /// <summary>
        /// Измениение материала на объекте (покраска)
        /// </summary>
        /// <param name="isChild">Необходимость подсветки дочерних объектов</param>
        /// <param name="obj">Объект который необходимо подсветить</param>
        /// <param name="material">Материал подсветки</param>
        public void OnHigthligth(bool isChild,  GameObject obj, Material material)
        {
            MassRenderer = null;
            int size = 0;

            if (obj == null) return;

            if (obj.GetComponent<Renderer>() != null)
            {

                Material[] materials = obj.GetComponent<Renderer>().materials;
                MassRenderer = new Material[1][];
                MassRenderer[0] = new Material[materials.Length];
                for (int i = 0; i < materials.Length; i++)
                {
                    MassRenderer[0][i] = materials[i];
                    materials[i] = material;
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] childs = obj.transform.GetComponentsInChildren<Renderer>();
                Debug.Log(childs.Length);

                if (MassRenderer != null)
                {
                    size++;
                    Material[] mat = MassRenderer[0];
                    MassRenderer = new Material[childs.Length + size][];
                    MassRenderer[0] = new Material[mat.Length];
                    for (int i = 0; i < mat.Length; i++)
                    {
                        MassRenderer[0][i] = mat[i];
                    }
                    Debug.Log(MassRenderer.Length);
                }
                else
                {
                    MassRenderer = new Material[childs.Length + size][];
                }

                for (int i = 0; i < childs.Length; i++)
                {
                    MassRenderer[i+size] = new Material[childs[i].materials.Length];
                    Material[] materials = childs[i].materials;
                    for (int j = 0; j < childs[i].materials.Length; j++)
                    {
                        MassRenderer[i + size][j] = materials[j];
                        materials[j] = material;
                    }
                    childs[i].materials = materials;
                }
            }
        }

        /// <summary>
        /// Возващение материалов в исходное состояние
        /// </summary>
        /// <param name="isChild">Необходимость обработки дочерних объектов</param>
        /// <param name="obj">Объект которому необходимо вернуть стандартные материалы</param>
        public void DefaultMaterials(bool isChild, GameObject obj)
        {
            if (obj == null) return;

            int size = 0;
            if (obj.GetComponent<Renderer>() != null)
            {
                size++;
                Material[] materials = obj.GetComponent<Renderer>().materials;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i] = MassRenderer[0][i];
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>();
                for (int j = 0; j <  renderers.Length; j++)
                {
                    Material[] materials = renderers[j].materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = MassRenderer[j + size][i];
                    }
                    renderers[j].materials = materials;
                }
            }
        }

        /// <summary>
        /// Метод для включения/выключения объводки элемента
        /// </summary>
        /// <param name="isSet">Of/on
        /// </param>
        /// <param name="obj">Объект у которого необходимо включить/выключить объводку</param>
        public void OnOutLine(bool isSet ,GameObject obj)
        {
            if (obj == null || obj.GetComponent<Highlighter>() == null) return;

            obj.GetComponent<Highlighter>().ConstantParams(Color);
            if (isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOn();
            }
            if (!isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOff();
            }
        }

        #endregion /////////////////////////////////////////////////////////
    }
}
