﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.   +7 988 573 83 33
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Класс хронения данных для модуля изучения
    /// </summary>
    public class Table : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на GameController
        /// </summary>
        public GameController GameController;

        /// <summary>
        /// лист трансформов для подлета камеры 
        /// </summary>
        [SerializeField]
        public List<Transform> ListTarget = new List<Transform>();
        /// <summary>
        /// лист трансформов для наблюдения камеры
        /// </summary>
        [SerializeField]
        public List<Transform> ListLook = new List<Transform>();
        /// <summary>
        /// лист субтитров 
        /// </summary>
        [SerializeField]
        public List<string> ListSubtitle = new List<string>();
        /// <summary>
        /// лист аудиоклипов 
        /// </summary>
        [SerializeField]
        public List<AudioClip> ListAudiclips = new List<AudioClip>();

        //        [SerializeField]
        //        public List<Object> ListAudiclips = new List<Object>(); // лист аудиоклипов

        /// <summary>
        /// лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации 
        /// </summary>
        [SerializeField]
        public List<bool> ListIsPlayThenEmpty = new List<bool>();
        /// <summary>
        /// лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента
        /// </summary>
        [SerializeField]
        public List<bool> ListIsChild = new List<bool>();
        /// <summary>
        /// лист объектов, которых нужно красить и подсвечивать 
        /// </summary>
        [SerializeField]
        public List<GameObject> ListHightlightObjects = new List<GameObject>();
        /// <summary>
        /// лист тоглсов для определения позиции 
        /// </summary>
        [SerializeField]
        public List<bool> ListIsPosition = new List<bool>();
        /// <summary>
        /// лист объектов для активации действий 
        /// </summary>
        [SerializeField]
        public List<GameObject> ListActionObjects = new List<GameObject>();
        /// <summary>
        /// лист трансформов для подхода осмотрщика 
        /// </summary>
        [SerializeField]
        public List<Transform> ListTargetWalk = new List<Transform>();
        /// <summary>
        /// лист названий анимации перемещения осмотрщика 
        /// </summary>
        [SerializeField]
        public List<string> ListNameAnimationWalk = new List<string>();
        /// <summary>
        /// лист названий idle анимаций осмотрщика 
        /// </summary>
        [SerializeField]
        public List<string> ListNameAnimationIdle = new List<string>();
        /// <summary>
        /// литс трансформов для определения направления обзора осмотрщика
        /// </summary>
        [SerializeField]
        public List<Transform> ListTransformRotation = new List<Transform>(); 
        /// <summary>
        /// Список кнопок
        /// </summary>
        [SerializeField]
        public List<GameObject> ListButtonObject = new List<GameObject>();
        /// <summary>
        /// Список зан активаций
        /// </summary>
        [SerializeField]
        public List<GameObject> ListZoneOperations = new List<GameObject>();
        /// <summary>
        /// лист названий анимаций осмотрщика для выполнения каких либо действий 
        /// </summary>
        [SerializeField]
        public List<string> ListNameAnimation = new List<string>();
        /// <summary>
        /// лист дефектов 
        /// </summary>
        [SerializeField]
        public List<GameObject> ListDefects = new List<GameObject>();
        /// <summary>
        /// лист аудиослипов сопровождающий демострацию дефектов 
        /// </summary>
        [SerializeField]
        public List<AudioClip> ListDefectAudioClips = new List<AudioClip>();
        /// <summary>
        /// лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации 
        /// </summary>
        [SerializeField]
        public List<bool> ListDefectIsPlayThenEmpty = new List<bool>();
        /// <summary>
        /// лист субтитров сопровождающий демонстрацию дефектов
        /// </summary>
        [SerializeField]
        public List<string> ListDefectSubtitles = new List<string>();
        /// <summary>
        /// лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента дефекта 
        /// </summary>
        [SerializeField]
        public List<bool> ListDefectIsChilds = new List<bool>(); 
        
        /// <summary>
        /// Метод выполняется при включении скрипта до метода Старт 
        /// </summary>
        void Awake()
        {
            //Заполнение списка цеоей осмотра
            foreach (var COLLECTION in ListLook)
            {
                if (COLLECTION != null)
                {
                    if (COLLECTION.transform.childCount != 0)
                    {
                        ListTarget.Add(COLLECTION.transform.GetChild(0));
                        continue;
                    }
                }
                ListTarget.Add(null);
            }
            //Заполнение списка целей, куда необходимо перемещаться осмотрщику
            foreach (var COLLECTION in ListTargetWalk)
            {
                if (COLLECTION != null)
                {
                    if (COLLECTION.transform.childCount != 0)
                    {
                        ListTransformRotation.Add(COLLECTION.transform.GetChild(0));
                        continue;
                    }
                }
                ListTransformRotation.Add(null);
            }
        }
    }
}