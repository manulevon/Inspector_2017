﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 20/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Контроллер панели инструментов
    /// </summary>
    public class InstrumentsController : MonoBehaviour
    {
       /// <summary>
       /// Список изображений инструментов
       /// </summary>
        public List<Sprite> ListTools;
        /// <summary>
        /// Трансформ панели, в которой расположены кнопки с инструментами
        /// </summary>
        public Transform Panel;
        /// <summary>
        /// Трансформ Заднего фон панели
        /// </summary>
        public Transform Background;
        /// <summary>
        /// Префаб кнопки 
        /// </summary>
        public Button ButtonPrefab;

        /// <summary>
        /// Метод выполняемый до старта при запуске скрипта
        /// </summary>
        void Awake()
        {
            RectTransform b = Background.GetComponent<RectTransform>();
            var bRect = b.rect;
            var cRect = b.rect;

            //расчет высоты панели
            float height = bRect.height - Background.GetComponent<GridLayoutGroup>().padding.left -
                                                     Background.GetComponent<GridLayoutGroup>().padding.right;
           //расчет длины панели
            float width = bRect.height - bRect.height/6;

            //Установка размеры заднего фона
            Background.GetComponent<GridLayoutGroup>().cellSize = new Vector2(width, height);

            //Создание кнопок-инструментов
            for (int i = 0; i < ListTools.Count; i++)
            {
                //Создание кнопоки
                var a = Instantiate(ButtonPrefab, Vector3.zero, transform.rotation, Background);
//            a.transform.GetComponentInChildren<Image>().sprite = ListTools[i];
                a.transform.GetChild(2).GetComponent<Image>().sprite = ListTools[i];
                a.transform.GetComponentInChildren<Text>().text = ListTools[i].name;
            }
            //Установка текста
            foreach (var COLLECTION in Background.transform.GetComponentsInChildren<Text>())
            {
                COLLECTION.text = COLLECTION.transform.parent.name;
            }

            //Установка размеры заднего фона
            bRect.width = width * Background.transform.childCount +
                Background.GetComponent<GridLayoutGroup>().spacing.x * (Background.transform.childCount-1) +
                    Background.GetComponent<GridLayoutGroup>().padding.left +
                          Background.GetComponent<GridLayoutGroup>().padding.right;
            Background.GetComponent<RectTransform>().sizeDelta = new Vector2( bRect.width - cRect.width, 0);

        }
    }
}
