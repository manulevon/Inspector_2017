﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 20/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using HighlightingSystem;
using newLogic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Набор классов для модуля изучения
/// </summary>
namespace Studying
{
    /// <summary>
    /// Похож на GameContrroller
    /// Не используется
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        #region Variabres and Properties////////////////////////////////////////////////////

        public PlaySoundController PlaySoundController;
        public Table Table;

        public Texture2D DefaultCursore;
        public List<Texture2D> ListCursores = new List<Texture2D>();

        public Color Color; // Цвет для подсветки элементов
        public Color ColorError;
        public Color ColorEmission = new Color(1,0,1,1); // Цвет для подсветки материала (точек перемещения)
        private MyCoroutine _myCoroutine; // экземпляр класса своей карутины
        public Transform Monter;
        private Animator _animatorMonter; // ссылка на аниматор осмотрщика

        public Material HigthligthMaterual; // материал для покраски элементов
        public Material FadeMaterial; // материал для подстветки объекта зоны
        public Material[][] MassRenderer; // массив дефолтных материалов для возвтрата материалов окрашенного элемента
        public AudioSource Magnitofon; // ссылка на audioSource контроллера игры

        public List<Transform> ListTarget = new List<Transform>(); // лист трансформов для подлета камеры
        public List<Transform> ListLook = new List<Transform>(); // лист трансформов для наблюдения камеры
        public List<string> ListSubtitle = new List<string>(); // лист субтитров
        public List<AudioClip> ListAudiclips = new List<AudioClip>(); // лист аудиоклипов
        public List<bool> ListIsPlayThenEmpty = new List<bool>(); // лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации
        public List<bool> ListIsChild = new List<bool>(); // лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента
        public List<GameObject> ListHightlightObjects = new List<GameObject>(); // лист объектов, которых нужно красить и подсвечивать
        public List<bool> ListIsPosition = new List<bool>(); // лист тоглсов для определения позиции

        public List<GameObject> ListActionObjects = new List<GameObject>(); // лист объектов для активации действий
        public List<Transform> ListTargetWalk = new List<Transform>(); // лист трансформов для подхода осмотрщика
        public List<string> ListNameAnimationWalk = new List<string>(); // лист названий анимации перемещения осмотрщика
        public List<string> ListNameAnimationIdle = new List<string>(); // лист названий idle анимаций осмотрщика
        public List<Transform> ListTransformRotation = new List<Transform>(); // литс трансформов для определения направления обзора осмотрщика

        public List<GameObject> ListButtonObject = new List<GameObject>();
        public List<GameObject> ListZoneOperations = new List<GameObject>();
        public List<string> ListNameAnimation = new List<string>(); // лист названий анимаций осмотрщика для выполнения каких либо действий

        public List<GameObject> ListDefects = new List<GameObject>(); // лист дефектов
        public List<AudioClip> ListDefectAudioClips = new List<AudioClip>(); // лист аудиослипов сопровождающий демострацию дефектов
        public List<bool> ListDefectIsPlayThenEmpty = new List<bool>(); // лист тоглсов для включения ожидания, если нет аудиоклип на этой итерации
        public List<string> ListDefectSubtitles = new List<string>(); // лист субтитров сопровождающий демонстрацию дефектов
        public List<bool> ListDefectIsChilds = new List<bool>(); // лист тоглсов для покраски дочерних элементов, находящихся у родительского элемента дефекта

        private string _currentAnimationWalk = null; // название текущей анимации хотьбы
        private string _currentAnimation = null;
        private Transform _currentLookObject = null; // трасформ текущего осматриваемого элемента
        private GameObject _pref = null; // ссылка на создаваемый дефект
        private int _numberposition; // итератор
        public Text Subtitle; // сслыка на текст для отображения субтитров
        public float Time = 5f; // время ожидания если не укзан аудиоклип

        public Image Eye;

//        public static bool Kastil; // пока не нужен
        private bool _kastil;
        private bool _stop;
        public bool IsExit;
        public Button ButtonExit;


        public float min = .5f;
        public float max = 2;
        public float step = .05f;
        public float value = 0;

        public static State StateController = State.Flay; // начальное состояние

        public enum State
        {
            Flay,
            Inspector,
            Movement,
            Operation,
            DisplayingDefect
        } // перечислитель состояний

        public int Numberposition
        {
            get { return _numberposition; }
            set
            {
                if (value> ListTarget.Count-1 || value<0)
                {
                    Debug.Log("Выход за пределы массива");
                    _stop = true;
                    if (value> ListTarget.Count-1)
                    {
                        IsExit = true;
                    }
                    return;
                }
                _stop = false;
                IsExit = false;
                _numberposition = value;
            }
        }

        #endregion /////////////////////////////////////////////////////////////

        #region Method Awake ////////////////////////////////////////////////////

        public void Awake()
        {


            SetCustomCursor("");

            PlaySoundController = Monter.GetComponent<PlaySoundController>();

            Magnitofon = GetComponent<AudioSource>();
            _animatorMonter = Monter.gameObject.GetComponent<Animator>();
            ItemController.CliclObject = new GameObject();
            OsmotrObjectController.OsmotrObject = new GameObject();
            _myCoroutine = new MyCoroutine();

            _numberposition = 0;
//            Kastil = true;
        }

        #endregion //////////////////////////////////////////////////////////////

        void Start()
        {
            ListTarget = Table.ListTarget;
            ListLook = Table.ListLook;
            ListSubtitle = Table.ListSubtitle;
            ListAudiclips = Table.ListAudiclips;
            ListIsPlayThenEmpty = Table.ListIsPlayThenEmpty;
            ListIsChild = Table.ListIsChild;
            ListHightlightObjects = Table.ListHightlightObjects;
            ListIsPosition = Table.ListIsPosition;

            ListActionObjects = Table.ListActionObjects;
            ListTargetWalk = Table.ListTargetWalk;
            ListNameAnimationWalk = Table.ListNameAnimationWalk;
            ListNameAnimationIdle = Table.ListNameAnimationIdle;
            ListTransformRotation = Table.ListTransformRotation;

            ListButtonObject = Table.ListButtonObject;
            ListZoneOperations = Table.ListZoneOperations;
            ListNameAnimation = Table.ListNameAnimation;

            ListDefects = Table.ListDefects;
            ListDefectAudioClips = Table.ListDefectAudioClips;
            ListDefectIsPlayThenEmpty = Table.ListDefectIsPlayThenEmpty;
            ListDefectSubtitles = Table.ListDefectSubtitles;
            ListDefectIsChilds = Table.ListDefectIsChilds;

            foreach (var VARIABLE in ListHightlightObjects)
            {
                if (VARIABLE == null) continue;
                if (VARIABLE.GetComponent<Highlighter>() == null)
                    VARIABLE.AddComponent<Highlighter>();
                if (VARIABLE.GetComponent<OsmotrObjectController>() == null)
                    VARIABLE.AddComponent<OsmotrObjectController>();
                if (VARIABLE.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in VARIABLE.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
            }

            foreach (var COLLECTION in ListActionObjects)
            {
                if (COLLECTION != null)
                {
                    Debug.Log(COLLECTION);
                    COLLECTION.gameObject.AddComponent<ItemController>();
                    COLLECTION.gameObject.SetActive(false);
                }

            }
            foreach (var VARIABLE in ListDefects)
            {
                if (VARIABLE == null) continue;
                if (VARIABLE.GetComponent<Highlighter>() == null)
                    VARIABLE.AddComponent<Highlighter>();
            }

            foreach (var VARIABLE in ListZoneOperations)
            {
                if (VARIABLE != null)
                {
                    VARIABLE.gameObject.SetActive(false);
                }
            }
            ChangeState();
        }

        #region State Ispector ///////////////////////////////////////////

        /// <summary>
        /// Метод для осмотра текущего элемента с сопровождением субтитров, звукового ряда, подсветки и покраски
        /// Принимает в себя тогл, который определяет нужно ли ожидать время, если не указан аудиоклип
        /// </summary>
        /// <param name="isPlay"></param>
        void Inspector(bool isPlay)
        {
            PlaySound(ListAudiclips[Numberposition]);
            PrintSubtitle(ListSubtitle[Numberposition]);
            OnOutLine(true, ListHightlightObjects[Numberposition], Color);
            OnHigthligth(ListIsChild[Numberposition],ListHightlightObjects[Numberposition], HigthligthMaterual);
            if (ListHightlightObjects[Numberposition] != null)
            {
                _currentLookObject = ListHightlightObjects[Numberposition].GetComponent<Transform>();
            }
            float timePlay = 0;
            if (isPlay)
            {
                if (ListAudiclips[Numberposition] != null)
                {
                    timePlay = ListAudiclips[Numberposition].length < Time ? Time: ListAudiclips[Numberposition].length;
                }
                else
                {
                    timePlay = Time;
                }
            }
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Flay);}));

            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { StartCoroutine(Osmotr(ListHightlightObjects[Numberposition])); }));
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Movement);}));
//            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { Osmotr(ListHightlightObjects[Numberposition]);}));
        }

        IEnumerator Osmotr(GameObject osmotrObject)
        {
//            ItemController.CliclObject = new GameObject();

//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
            if (osmotrObject == null)
            {
                yield return null;
            }
            else
            {
                if (osmotrObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in osmotrObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = true;
                    }
                }
//                Eye.transform.position = Camera.main.WorldToScreenPoint(osmotrObject.transform.position);
//                Eye.transform.position = Input.mousePosition;
                while (Eye.fillAmount < 1)
                {

                    if (OsmotrObjectController.OsmotrObject == osmotrObject)
                    {
                        Eye.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y+20, Input.mousePosition.z);
                        Eye.fillAmount = OsmotrObjectController.Time / OsmotrObjectController.TimePrint;
                    }
                    else
                        Eye.fillAmount = 0;
                    yield return null;
                }
                if (osmotrObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in osmotrObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
                Eye.fillAmount = 0;
                OnOutLine(false, ListHightlightObjects[Numberposition], Color);
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);
            }
            StateController = State.Movement;
            ChangeState();
            yield return null;
        }

        #endregion ///////////////////////////////////////////////////////

        /// <summary>
        /// Метод, который вызывается после завершения определенных состояний
        /// Принимает в себя состояние, в которое необходимо перейти
        /// </summary>
        /// <param name="state"></param>
        void EndTime(State state)
        {
            StateController = state;
            if (state == State.Flay)
            {
                if (_pref != null)
                {
                    OnOutLine(false, _pref, ColorError);
                    Destroy(_pref);
                    _pref = null;
                    if (_currentLookObject != null)
                    {
                        if (_currentLookObject.gameObject.GetComponent<Renderer>() != null)
                            _currentLookObject.gameObject.GetComponent<Renderer>().enabled = true;
                        if (_currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>().Length != 0)
                        {
                            foreach (var COLLECTION in _currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>())
                            {
                                COLLECTION.enabled = true;
                            }
                        }
                    }
                }
                Numberposition++;
            }

            if (state == State.DisplayingDefect)
            {
                GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
            }
            ChangeState();
        }

        #region State Movement ///////////////////////////////////////////////////

        /// <summary>
        /// Метод для определения того, когда пользователь начнет взаимодействовать с нужным объектом
        /// Принимает в себя объект, при взаимодействии с которым начнет происходить перемещение осмотрщика
        /// </summary>
        /// <param name="actionObject"></param>
        /// <returns></returns>
        IEnumerator Movement(GameObject actionObject)
        {
//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);

            ItemController.CliclObject = new GameObject();

            if (actionObject == null)
            {
                yield return null;
            }
            else
            {
                actionObject.gameObject.SetActive(true);
                while (ItemController.CliclObject != actionObject)
                {
//                    HightlightClickObject(actionObject.GetComponent<Renderer>());
                    yield return null;
                }
//                actionObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(0, 0, 0, 1));
                actionObject.gameObject.SetActive(false);
            }
            yield return StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

//            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);

            StateController = State.Operation;
            ChangeState();
        }

        /// <summary>
        /// Метод для осуществления мигания объекта, с которым должно произойти взаимодействие
        /// Принимает в себя рендер объекта, с которым должно произойти взаимодействие
        /// </summary>
        /// <param name="renderer"></param>
        void HightlightClickObject(Renderer renderer)
        {
            float amount = min;
            value += step;
            float sin = Mathf.Sin(value);
            sin = (sin + 1) / 2;
            amount += sin * (max - min);
            Color color = ColorEmission * amount;
            renderer.material.SetColor("_EmissionColor", color);
            DynamicGI.SetEmissive(renderer, color);
        }

        /// <summary>
        /// Метод для осуществления перемещения осмотрщика
        /// Принимает в себя трансфор куда нужно подойти и в каком направлении смотреть
        /// </summary>
        /// <param name="targetPosition"></param>
        /// <param name="targetRotation"></param>
        /// <returns></returns>
        IEnumerator Move(Transform targetPosition, Transform targetRotation)
        {
            if (targetPosition == null)
            {
                if (targetRotation != null)
                {
                    PlayerController.TargetRotation = targetRotation;
                    yield return null;
                }
                yield return null;
            }
            else
            {
                PlayerController.TargetPosition = targetPosition;
                PlayerController.TargetRotation = targetRotation;
                if (Monter.transform.position != new Vector3(targetPosition.position.x, Monter.transform.position.y,
                        targetPosition.position.z))
                {
                    GetAnimationWalk(ListNameAnimationWalk[_numberposition]);
//                    if (_currentAnimationWalk != null)
//                    {
//                        if (_currentAnimation != _currentAnimationWalk)
//                        {
//                            _animatorMonter.SetTrigger(_currentAnimationWalk);
//                            _currentAnimation = _currentAnimationWalk;
//                        }
//
//                    }

                }
                while (PlayerController.TargetPosition != null)
                {
                    print("Move");
                    yield return null;
                }
            }
            GetAnimationIdle(ListNameAnimationIdle[Numberposition]);
//
//            StateController = State.Operation;
//            ChangeState();
        }

        #endregion /////////////////////////////////////////////////////////

        /// <summary>
        /// Метод для получения названия анимации ходьбы, которую нужно использовать
        /// </summary>
        /// <param name="name"></param>
        void GetAnimationWalk(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return;
            }
            if (_currentAnimation != name)
            {
                _currentAnimation = name;
                _animatorMonter.SetTrigger(name);
            }
        }

        #region State Operation /////////////////////////////////////////////////////

        IEnumerator Operation(GameObject button)
        {
            ButtonObjectController.ClicButton = new GameObject();
            if (button == null)
            {
                yield return null;
            }
            else
            {
                while (button != ButtonObjectController.ClicButton)
                {
                    yield return null;
                }
  //              SetCustomCursor(button.transform.GetChild(1).GetComponent<Text>().text);
            }
            StartCoroutine(ClickZone(ListZoneOperations[Numberposition]));
        }

        IEnumerator ClickZone(GameObject zone)
        {
            ZoneController.CliclObject = new GameObject();
            if (zone == null)
            {
                yield return null;
            }
            else
            {
                zone.GetComponent<Collider>().enabled = true;
//                zone.transform.GetChild(0).gameObject.SetActive(true);
                zone.SetActive(true);
                Material[][] materialsDefault;
                GetMaterialsDefault(zone.transform.GetChild(0).gameObject, out materialsDefault);
                while (zone != ZoneController.CliclObject)
                {
                    FlashingObjectWithZone(zone.transform.GetChild(0).gameObject, FadeMaterial);
                    yield return null;
                }

                Magnitofon.Stop();
                zone.GetComponent<Collider>().enabled = false;
                zone.transform.GetChild(0).gameObject.SetActive(false);

                GetDefaultMaterials(zone.transform.GetChild(0).gameObject, materialsDefault);
                SetCustomCursor("");
            }
            yield return StartCoroutine(OperationAnimation(ListNameAnimation[Numberposition]));
            EndTime(State.DisplayingDefect);

        }

        #region ChangeMaterial and Cursore /////////////////////////////////////////////////

        void GetMaterialsDefault(GameObject flashingObject, out Material[][] materialsDefault)
        {
            materialsDefault = null;
            int size = 0;

            if (flashingObject != null)
            {
                if (flashingObject.GetComponent<Renderer>() != null)
                {
                    materialsDefault = new Material[1][];
                    Material[] materials = flashingObject.GetComponent<Renderer>().materials;
                    materialsDefault[0] = new Material[materials.Length];
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materialsDefault[0][i] = materials[i];
                    }
                }
                Renderer[] childs = flashingObject.transform.GetComponentsInChildren<Renderer>();

                if (childs != null)
                {
                    if (materialsDefault != null)
                    {
                        size++;
                        Material[] mat = materialsDefault[0];
                        materialsDefault = new Material[childs.Length + size][];
                        materialsDefault[0] = new Material[mat.Length];
                        for (int i = 0; i < mat.Length; i++)
                        {
                            materialsDefault[0][i] = mat[i];
                        }
                    }
                    else
                    {
                        materialsDefault = new Material[childs.Length + size][];
                    }

                    for (int i = 0; i < childs.Length; i++)
                    {
                        materialsDefault[i + size] = new Material[childs[i].materials.Length];
                        Material[] materials = childs[i].materials;
                        for (int j = 0; j < childs[i].materials.Length; j++)
                        {
                            materialsDefault[i + size][j] = materials[j];
                        }
                    }
                }
            }
        }

        void FlashingObjectWithZone(GameObject flashingObject, Material material)
        {
            if (flashingObject != null)
            {
                if (flashingObject.GetComponent<Renderer>() != null)
                {
                    Material[] materials = flashingObject.GetComponent<Renderer>().materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = material;
                        materials[i].color =  Color.Lerp( Color.cyan, Color.clear,
                            Mathf.PingPong(UnityEngine.Time.time, 1));
                    }
                    flashingObject.GetComponent<Renderer>().materials = materials;
                }
                if (flashingObject.GetComponentsInChildren<Renderer>().Length != 0)
                {
                    var a = flashingObject.GetComponentsInChildren<Renderer>();
// TODO: CHANGE
//                    for (int i = 0; i < a.Length; i++)
//                    {
//                        Material[] materials = a[i].materials;
//
//                        for (int j = 0; j < materials.Length; j++)
//                        {
//                            materials[i] = material;
//                            materials[i].color = Color.Lerp(Color.cyan, Color.clear,
//                                Mathf.PingPong(UnityEngine.Time.time, 1));
//                        }
//                        a[i].materials = materials;
//                    }

                    for (int i = 0; i < a.Length; i++)
                    {
                        Material materials = a[i].material;

                                materials = material;
                                materials.color = Color.Lerp(Color.cyan, Color.clear,
                                    Mathf.PingPong(UnityEngine.Time.time, 1));
                        a[i].material = materials;

                    }
                }
            }
        }

        void GetDefaultMaterials(GameObject obj, Material[][] materialsDefault)
        {
            if (obj == null) return;

            int size = 0;
            if (obj.GetComponent<Renderer>() != null)
            {
                size++;
                Material[] materials = obj.GetComponent<Renderer>().materials;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i] = materialsDefault[0][i];
                }
                obj.GetComponent<Renderer>().materials = materials;
            }
            var a = obj.GetComponentsInChildren<Renderer>();
            if (a != null)
            {
                Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>();
                for (int j = 0; j < renderers.Length; j++)
                {
                    Material[] materials = renderers[j].materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = materialsDefault[j + size][i];
                    }
                    renderers[j].materials = materials;
                }
            }
        }

        void SetCustomCursor(string buttonText)
        {
            switch (buttonText)
            {
                case "Абсолютный шаблон":
                    Cursor.SetCursor(ListCursores[11], Vector2.zero, CursorMode.Auto);
                    break;
                case "Досмотровая штанга":
                    Cursor.SetCursor(ListCursores[10], Vector2.zero, CursorMode.Auto);
                    break;
                case "Кельвин":
                    Cursor.SetCursor(ListCursores[14], Vector2.zero, CursorMode.Auto);
                    break;
                case "Ломик Гладуна":
                    Cursor.SetCursor(ListCursores[15], Vector2.zero, CursorMode.Auto);
                    break;
                case "Лупа":
                    Cursor.SetCursor(ListCursores[7], Vector2.zero, CursorMode.Auto);
                    break;
                case "Метр":
                    Cursor.SetCursor(ListCursores[9], Vector2.zero, CursorMode.Auto);
                    break;
                case "Молоток":
                    Cursor.SetCursor(ListCursores[5], Vector2.zero, CursorMode.Auto);
                    break;
                case "Щетка":
                    Cursor.SetCursor(ListCursores[2], Vector2.zero, CursorMode.Auto);
                    break;
                case "Щюп":
                    Cursor.SetCursor(ListCursores[0], Vector2.zero, CursorMode.Auto);
                    break;

                default:
                    Cursor.SetCursor(DefaultCursore, Vector2.zero, CursorMode.Auto);
                    break;
            }
        }

        #endregion /////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Метод для осуществления анимации текущей операции осмотрщиком
        /// Принимает в себя название анимации
        /// </summary>
        /// <param name="nameAnimation"></param>
        /// <returns></returns>
        IEnumerator OperationAnimation(string nameAnimation)
        {
            PlaySoundController.AudioSource.Stop(); // TODO
            float timeAnimation = 0;

            if (string.IsNullOrEmpty(nameAnimation))
            {
//                _kastil = true;
//                yield return null;
            }
            else
            {
//                Monter.GetComponent<Animator>().SetTrigger(nameAnimation);
                GetAnimationWalk(nameAnimation);
                Debug.Log(nameAnimation);

                yield return new WaitForSeconds(.1f);

                timeAnimation = Monter.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
                Debug.Log(timeAnimation);

                yield return new WaitForSeconds(timeAnimation - .1f);
            }
            Debug.Log("END");
            yield return null;

//            StartCoroutine(
//                _myCoroutine.InvokeCoroutine(timeAnimation - .1f, () => { EndTime(State.DisplayingDefect); }));
        }

        #endregion ///////////////////////////////////////////////////////////////////

        /// <summary>
        /// Метод для получения idle анимации
        /// Принимает в себя название анимации
        ///  </summary>
        /// <param name="nameAnimation"></param>
        void GetAnimationIdle(string nameAnimation)
        {
            if (string.IsNullOrEmpty(nameAnimation))
            {
                return;
            }
            if (_currentAnimation != nameAnimation)
            {
                _currentAnimation = nameAnimation;
                _animatorMonter.SetTrigger(nameAnimation);
            }

        }

        #region State DisplayingDefect ///////////////////////////////////////

        /// <summary>
        /// Метод для демонстрации дефектов
        /// Принимает в себя тогл, который определяет нужно ли ожидать заданное время, если не указан аудиоклип
        /// </summary>
        /// <param name="isPlay"></param>
        void DisplayinDefect(bool isPlay)
        {
            PlaySound(ListDefectAudioClips[Numberposition]);
//            Numberposition++;
//            if (_stop)
//            {
//                return;
//            }
//            OnOutLine(false, ListHightlightObjects[_numberposition - 1]);
//            DefaultMaterials(ListIsChild[_numberposition-1],ListHightlightObjects[_numberposition - 1]);
            PrintSubtitle(ListDefectSubtitles[Numberposition]);
            InstantiateDefect(ListDefects[Numberposition]);
            float timePlay = 0f;
            if (ListDefectAudioClips[Numberposition] != null)
            {
                timePlay = ListDefectAudioClips[Numberposition].length < Time ? Time : ListDefectAudioClips[Numberposition].length;
            }
            else
            {
                if (isPlay)
                    timePlay = Time;
            }
            StartCoroutine(_myCoroutine.InvokeCoroutine(timePlay, () => { EndTime(State.Flay);}));
        }

        /// <summary>
        /// Метод для замены текущего элемнта на элеиент с дефектом
        /// </summary>
        /// <param name="pref"></param>
        void InstantiateDefect(GameObject pref)
        {
            if (pref != null)
            {
                if (_currentLookObject != null)
                {
                    if (_currentLookObject.gameObject.GetComponent<Renderer>() != null)
                        _currentLookObject.gameObject.GetComponent<Renderer>().enabled = false;
                    if (_currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>().Length != 0)
                    {
                        foreach (var COLLECTION in _currentLookObject.gameObject.transform.GetComponentsInChildren<Renderer>())
                        {
                            COLLECTION.enabled = false;
                        }
                    }
                    _pref = Instantiate(pref, _currentLookObject.transform.position, pref.transform.rotation);
                    OnOutLine(true, _pref, ColorError);
                }
            }
        }

        #endregion ////////////////////////////////////////////////////////////

        #region Method ChangeState ///////////////////////////////////
        /// <summary>
        /// Метод для переключения состояния
        /// </summary>
        void ChangeState()
        {
            if (_stop)
            {
                return;
            }

            if (StateController == State.Flay)
            {
                Subtitle.text = "";
                StartCoroutine(MoveCamera(ListTarget[Numberposition]));
                StartCoroutine(LookCamera(ListLook[Numberposition]));
                return;
            }

            if (StateController == State.Inspector)
            {
                Inspector(ListIsPlayThenEmpty[Numberposition]);
                return;
            }

            if (StateController == State.Movement)
            {
                StopAllCoroutines();
                StartCoroutine(Movement(ListActionObjects[Numberposition]));
                return;
            }

            if (StateController == State.Operation)
            {
                StartCoroutine(Operation(ListButtonObject[Numberposition]));
            }

            if (StateController == State.DisplayingDefect)
            {
                DisplayinDefect(ListDefectIsPlayThenEmpty[Numberposition]);
            }
        }
        #endregion //////////////////////////////////////////////////////

        private void Update()
        {
            Debugging();

            if (IsExit)
            {
                ButtonExit.GetComponent<Animator>().runtimeAnimatorController =
                    Resources.Load("Exit in Menu") as RuntimeAnimatorController;
            }
            else
            {
                ButtonExit.GetComponent<Animator>().runtimeAnimatorController =
                    Resources.Load("Training") as RuntimeAnimatorController;
            }
//            ChangeState();
        }

        void Debugging() // TODO: for debugging
        {
            if (Input.GetKeyUp(KeyCode.D))
            {
                ButtonNext();
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                ButtonPrew();
            }
        }

        public void ButtonNext()
        {
            if (ListActionObjects[Numberposition])
                ListActionObjects[Numberposition].SetActive(false);

            if (_currentLookObject != null)
            {
                if (_currentLookObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in _currentLookObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
            }

            OnOutLine(false, ListHightlightObjects[Numberposition], Color);
            if ( MassRenderer!= null)
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);

            OnOutLine(false, ListDefects[Numberposition], ColorError);

            if (ListZoneOperations[Numberposition])
            {
                ListZoneOperations[Numberposition].GetComponent<Collider>().enabled = false;
                ListZoneOperations[Numberposition].transform.GetChild(0).gameObject.SetActive(false);
            }

            StopAllCoroutines();
            Magnitofon.Stop();
            PlaySoundController.AudioSource.Stop(); // TODO
            CancelInvoke();
            StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

            int number = Numberposition;

            StartCoroutine(CheckPosition(number));

            Numberposition++;
            StateController = State.Flay;
            ChangeState();
        }

        public void ButtonPrew()
        {
            if (ListActionObjects[Numberposition])
                ListActionObjects[Numberposition].SetActive(false);

            if (_currentLookObject != null)
            {
                if (_currentLookObject.GetComponents<Collider>().Length != 0)
                {
                    foreach (var COLLECTION in _currentLookObject.GetComponents<Collider>())
                    {
                        COLLECTION.enabled = false;
                    }
                }
            }

            OnOutLine(false, ListHightlightObjects[Numberposition], Color);
            if ( MassRenderer!= null)
                DefaultMaterials(ListIsChild[Numberposition],ListHightlightObjects[Numberposition]);

            OnOutLine(false, ListDefects[Numberposition], ColorError);

            if (ListZoneOperations[Numberposition])
            {
                ListZoneOperations[Numberposition].GetComponent<Collider>().enabled = false;
                ListZoneOperations[Numberposition].transform.GetChild(0).gameObject.SetActive(false);
            }

            StopAllCoroutines();
            Magnitofon.Stop();
            PlaySoundController.AudioSource.Stop(); // TODO
            CancelInvoke();
            StartCoroutine(Move(ListTargetWalk[Numberposition], ListTransformRotation[Numberposition]));

            int number = Numberposition-2;


            StartCoroutine(CheckPosition(number));

            Numberposition--;
            StateController = State.Flay;
            ChangeState();
        }

        IEnumerator CheckPosition(int number)
        {
            while (Monter.transform.position != new Vector3(PlayerController.TargetPosition.position.x,
                       Monter.transform.position.y, PlayerController.TargetPosition.position.z))
            {
                yield return null;
            }
            if (number == 0 || number == ListNameAnimation.Count) // TODO : check true or false
            {
                yield break;
            }
            string nameAnimationOperation = ListNameAnimation[number];

            if (string.IsNullOrEmpty(nameAnimationOperation))
            {
                nameAnimationOperation = ListNameAnimationIdle[number];
            }
            yield return null;

            StartCoroutine(OperationAnimation(nameAnimationOperation));

        }

        //пок не используется
        public void InactiveButton(Button button)
        {
            button.enabled = false;
        }

        #region Look and Move //////////////////////////////////////////////////////

        /// <summary>
        /// Отправь точку в которую смотреть. Корутина работает без остановок.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public IEnumerator LookCamera(Transform target)
        {
            if (target == null)
            {
                yield return null;
            }
            else
            {
                while (true)
                {
                    Vector3 relativePos = target.position - Camera.main.transform.position;
                    Quaternion rotation = Quaternion.LookRotation(relativePos);
                    Camera.main.transform.rotation =
                        Quaternion.Lerp(Camera.main.transform.rotation, rotation, UnityEngine.Time.deltaTime * 5);
                    //                Camera.main.transform.rotation =
                    //                    Quaternion.RotateTowards(Camera.main.transform.rotation, rotation, Time.deltaTime * 150);
                    yield return null;
                }
            }
        }

        /// <summary>
        /// Отправь точку в которую лететь. Корутина выключится, когда долетит
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public IEnumerator MoveCamera(Transform target)
        {
            if (target == null)
            {
                yield return null;
            }
            else
            {
                var distance = Vector3.Distance(Camera.main.transform.position, target.position);

                while (distance > 0.1f)
                {
                    distance = Vector3.Distance(Camera.main.transform.position, target.position);
                    Camera.main.transform.position =
                        Vector3.Lerp(Camera.main.transform.position, target.position, UnityEngine.Time.deltaTime * 3);
                    yield return null;
                }
            }
            StateController = State.Inspector;
            ChangeState();
        }

        #endregion /////////////////////////////////////////////////////////////////

        /// <summary>
        /// Отправь сюда текст позиции, если он есть
        /// </summary>
        /// <param name="subtitle"></param>
        public void PrintSubtitle(string subtitle)
        {
            if (subtitle==null)return;

            this.Subtitle.text = subtitle;
        }

        /// <summary>
        /// Отправь сюда озвучку позиции
        /// </summary>
        /// <param name="audioClip"></param>
        public void PlaySound(AudioClip audioClip)
        {
            if (audioClip ==null)return;

            Magnitofon.clip = audioClip;
            Magnitofon.Play();
        }

        #region Hightlight /////////////////////////////////////////////////

        /// <summary>
        /// Измениение материала на объекте (покраска)
        /// </summary>
        /// <param name="obj"></param>
        public void OnHigthligth(bool isChild,  GameObject obj, Material material)
        {
            MassRenderer = null;
            int size = 0;

            if (obj == null) return;

            if (obj.GetComponent<Renderer>() != null)
            {

                Material[] materials = obj.GetComponent<Renderer>().materials;
                MassRenderer = new Material[1][];
                MassRenderer[0] = new Material[materials.Length];
                for (int i = 0; i < materials.Length; i++)
                {
                    MassRenderer[0][i] = materials[i];
                    materials[i] = material;
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] childs = obj.transform.GetComponentsInChildren<Renderer>();
                Debug.Log(childs.Length);

                if (MassRenderer != null)
                {
                    size++;
                    Material[] mat = MassRenderer[0];
                    MassRenderer = new Material[childs.Length + size][];
                    MassRenderer[0] = new Material[mat.Length];
                    for (int i = 0; i < mat.Length; i++)
                    {
                        MassRenderer[0][i] = mat[i];
                    }
                    Debug.Log(MassRenderer.Length);
                }
                else
                {
                    MassRenderer = new Material[childs.Length + size][];
                }

                for (int i = 0; i < childs.Length; i++)
                {
                    MassRenderer[i+size] = new Material[childs[i].materials.Length];
                    Material[] materials = childs[i].materials;
                    for (int j = 0; j < childs[i].materials.Length; j++)
                    {
                        MassRenderer[i + size][j] = materials[j];
                        materials[j] = material;
                    }
                    childs[i].materials = materials;
                }
            }
        }

        /// <summary>
        /// Возващение материалов в исходное состояние
        /// </summary>
        /// <param name="isChild"></param>
        /// <param name="obj"></param>
        public void DefaultMaterials(bool isChild, GameObject obj)
        {
            if (obj == null) return;

            int size = 0;
            if (obj.GetComponent<Renderer>() != null)
            {
                size++;
                Material[] materials = obj.GetComponent<Renderer>().materials;
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i] = MassRenderer[0][i];
                }
                obj.GetComponent<Renderer>().materials = materials;
            }

            if (isChild)
            {
                Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>();
                for (int j = 0; j <  renderers.Length; j++)
                {
                    Material[] materials = renderers[j].materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        materials[i] = MassRenderer[j + size][i];
                    }
                    renderers[j].materials = materials;
                }
            }
        }

        /// <summary>
        /// Метод для включения объводки элемента
        /// </summary>
        /// <param name="isSet"></param>
        /// <param name="obj"></param>
        public void OnOutLine(bool isSet ,GameObject obj, Color color)
        {
            if (obj == null || obj.GetComponent<Highlighter>() == null) return;

            obj.GetComponent<Highlighter>().ConstantParams(color);
            if (isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOn();
            }
            if (!isSet)
            {
                obj.GetComponent<Highlighter>().ConstantOff();
            }
        }

        #endregion /////////////////////////////////////////////////////////
    }
}