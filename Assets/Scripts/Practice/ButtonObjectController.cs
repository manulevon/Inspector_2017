﻿//-----------------------------------------------------------------------------
//
//      Модуль изучения
//      (с) РГУПС, ОИТП ЦРИК 13/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------


using UnityEngine;

/// <summary>
/// Набоор классов практического модуля
/// </summary>
namespace Studying
{
    /// <summary>
    /// Контроллер UI кнопок
    /// </summary>
    public class ButtonObjectController : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на кнопку на которую мы нажали
        /// </summary>
        public static GameObject ClicButton;

        /// <summary>
        /// Метод сохроняющий ссылку на нажатую кнопку
        /// </summary>
        public void Click()
        {
            // Сохроняем ссылку на кнопку
            ClicButton = this.gameObject;
        }
    }
}
