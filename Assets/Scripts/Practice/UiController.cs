﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using newLogic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Класс отвечающий за работу UI элементов
    /// </summary>
    public class UiController : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на экземпляр клааса MyCoroutine
        /// </summary>
        private MyCoroutine _coroutine;
        /// <summary>
        /// Ссылкка на компонент аниматор UI панели
        /// </summary>
        public Animator PanelUi;

        /// <summary>
        /// Transform Ui объекта
        /// </summary>    
        public RectTransform Arrow;
        /// <summary>
        /// Ссылка кнопку выхода
        /// </summary>
        public Button ButtonExit;

        /// <summary>
        /// Метод, отрабатываемый при включенние скрипта после метода Awake()
        /// </summary>
        void Start()
        {
            //Создание экземпляра класса MyCoroutine
            _coroutine = new MyCoroutine();
            //Запуск сопрограммы с задержкой
            StartCoroutine(_coroutine.InvokeCoroutine(.2f, () => { PanelUi.SetTrigger("PanelOpen"); }));
//            PanelUi.SetTrigger("PanelOpen");
            Elements.kastil = true;
        }

        /// <summary>
        /// Запускает анимацию закрытия панели
        /// </summary>
        /// <param name="animator">Аниматор объекта, на котором необходимо запустить анимацию</param>
        public void ClosePanel(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                PanelUi.SetTrigger("PanelCloseOn");
            }
            else if (Arrow.localScale.y == 1)
            {
                PanelUi.SetTrigger("PanelCloseOff");
            }
            else return;
            PlayerPrefs.SetInt("Level", 0);
            StartCoroutine(_coroutine.InvokeCoroutine(.5f, () => { SceneManager.LoadScene("Loader"); }));
            ButtonExit.enabled = false;
        }

        /// <summary>
        /// Запускае анимацию окрытия/закрытия панели инструментов
        /// </summary>
        /// <param name="animator"> Аниматор объекта, на котором необходимо запустить анимацию</param>
        public void HelpApDown(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                animator.SetTrigger("PanelDown");
            }
            if (Arrow.localScale.y == 1)
            {
                animator.SetTrigger("PanelUp");
            }
        }
    }
}