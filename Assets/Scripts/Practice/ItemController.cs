﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 20/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Набор классов для модуля изучения
/// </summary> 
namespace Studying
{
    /// <summary>
    /// Контроллер элемента
    /// </summary>
    public class ItemController : MonoBehaviour
    {
        /// <summary>
        /// Ссылка хронит объект на который нажал пользователь
        /// </summary>
        public static GameObject CliclObject;

        /// <summary>
        /// Метод который срабатывает при нажатии на объект
        /// </summary>
        void OnMouseDown()
        {
            CliclObject = this.gameObject;
        }
    }
}
