﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Тип инструмента
    /// </summary>
    public enum TypeInventary { NON, AbsolutTemplate, InspectionRrod, Kelvin, Kroncirkul, PinchBarGladuna, Lupa, Mel, Meter, Hammer, TemplateHolodov, Brush, Shup, ShupKlina, Tolshinomer, VPG}

    /// <summary>
    /// Контроллер инструментов
    /// </summary>
    public class InventaryManager : MonoBehaviour
    {
        /// <summary>
        /// Тип инвентаря
        /// </summary>
        public TypeInventary typeInventry = TypeInventary.NON;

        /// <summary>
        /// Спиок инструментов
        /// </summary>
        public List<Inventary> inventaryes = new List<Inventary>();
        
    }
}