﻿//-----------------------------------------------------------------------------
//
//       Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;
using HighlightingSystem;
using UnityEngine.UI;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Клас описывающий элемент, который необходимо осматривать
    /// </summary>
    [RequireComponent(typeof(Highlighter))]
    public class Element : MonoBehaviour
    {
        /// <summary>
        /// Необходимость осмотра
        /// </summary>
        public bool NonInspect;
        /// <summary>
        /// Номер позиции осмотра
        /// </summary>
        public int NumberPosition;
        /// <summary>
        /// Имя элемента
        /// </summary>
        public string name;
        /// <summary>
        /// Позиция осмотра
        /// </summary>
        public GameObject position;
        /// <summary>
        /// Таргет осмотра
        /// </summary>
        public GameObject target;
        /// <summary>
        /// Список дефектных элементов
        /// </summary>
        public List<DefectElement> Defects = new List<DefectElement>();
        /// <summary>
        /// Названия дефектов
        /// </summary>
        public List<string> textDefectsFalse = new List<string>();
        /// <summary>
        /// Список инструментов 
        /// </summary>
        public List<Inventary> InventaryList = new List<Inventary>();
        /// <summary>
        /// Словарь исползованных инструментов
        /// </summary>
        public Dictionary<Inventary, bool> usedInventary = new Dictionary<Inventary, bool>();
        /// <summary>
        /// Звуковая дорожка
        /// </summary>
        public AudioClip audio;

        /// <summary>
        /// Список правильно использованных инструментов
        /// </summary>
        [HideInInspector]
        public List<GameObject> trueinventary = new List<GameObject>();
        /// <summary>
        /// Список неправильно использованных инструментов
        /// </summary>
        [HideInInspector]
        public List<GameObject> falseinventary = new List<GameObject>();
        /// <summary>
        /// Список неправильно использованных инструментов
        /// </summary>
        [HideInInspector]
        public List<Inventary> falseInstrument = new List<Inventary>();
        /// <summary>
        /// Провильность ответа
        /// </summary>
        [HideInInspector]
        public bool Answer = false;
        /// <summary>
        /// Осмотрен элемент или нет
        /// </summary>
        [HideInInspector]
        public bool isInspect = false;
       
        //Менеджер меню ответов 
        private ManagerAnswerMenu managerAnswerMenu;
        // Ссылка на компонент обводки 
        private Highlighter Outline;
        //Дефектный элемент или нет
        private bool isDeffect = false;
        //Ссылка на дефектный объект сгенерированный при старте модуля
        private GameObject defectObject = null;
        //меню ответов 
        private AnswersMenu am;
        

        /// <summary>
        /// Метод исполняемый при запуске скрипта
        /// </summary>
        private void Start()
        {
            
            managerAnswerMenu = GameController.instance.managerAnsverMenu;
            Outline = GetComponent<Highlighter>();
            Outline.ConstantParams(GameController.instance.Outline);
            GeneratedDefect();
            CreateAnswerMenu();
            SetUsedInventary();
        }

        /// <summary>
        /// Метод выполняемый при наведении курсора на объект
        /// </summary>
        private void OnMouseEnter()
        {
            if (isDeffect)
            {
                if (!GameController.instance.isInspect)
                    defectObject.GetComponent<Highlighter>().ConstantOn();
            }
            else
            {
                if (!GameController.instance.isInspect)
                    Outline.ConstantOn();
            }
             
        }

        /// <summary>
        /// Метод выполняемый когда курсор мыши покидает объект
        /// </summary>
        private void OnMouseExit()
        {

            if (isDeffect)
            {
                if (!GameController.instance.isInspect)
                    defectObject.GetComponent<Highlighter>().ConstantOff();
            }
            else
            {
                if (!GameController.instance.isInspect)
                    Outline.ConstantOff();
            }
          
        }

        /// <summary>
        /// Метод вызывается по нажатию левой кнопки мыши по объъекту
        /// </summary>
        private void OnMouseDown()
        {
            if (GameController.instance.isInspect) return;
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;
                if (!GameController.instance.isInspect)
            {
                if (!NonInspect)
                    ActionInspect();
                else
                    ActionNonInspect();
            }

        }


        /// <summary>
        /// Действие для осматриваемого элемента
        /// </summary>
        void ActionInspect()
        {
            //            Cursor.lockState = CursorLockMode.Locked;

            //isInspect = true;

            GameController.instance.isInspect = true;
            GameController.instance.EnableCamFly(position, target);
            Outline.ConstantOff();
            GameController.instance.inspectElement = this;
            if (defectObject != null)
                defectObject.GetComponent<Highlighter>().ConstantOff();
            GameController.instance.heder.text = name;
        }

        /// <summary>
        /// Дествие для неосматриваемого объекта
        /// </summary>
        void ActionNonInspect()
        {
            if (!Outline.enabled) return;
            Outline.ConstantOff();
            Outline.enabled = false;
            this.GetComponent<Animator>().SetBool("Otpusk", true);
            GameController.instance.isOtpuskTormozov = true;
            if (audio != null) {
                GameController.instance.source.clip = audio;
                GameController.instance.source.Play();
            } 
        }

        /// <summary>
        /// Заполнение словаря необходимыми инструментами
        /// </summary>
        void SetUsedInventary()
        {
            foreach (var item in InventaryList)
            {
                usedInventary.Add(item, false);
            }
        }
         
        /// <summary>
        /// Генерация дефекта
        /// </summary>
        void GeneratedDefect()
        {
            int index = Random.Range(0,Defects.Count+1);

            //print(index);

            if (index == Defects.Count)
            {
                isDeffect = false;
            }
            else
            {
                isDeffect = true;
                defectObject =  Instantiate(Defects[index].gameObject);
                defectObject.name = Defects[index].gameObject.name;
                defectObject.transform.position = this.transform.position;
                Highlighter tmp = defectObject.GetComponent<Highlighter>();
                tmp.ConstantParams(GameController.instance.Outline);
                //this.GetComponent<MeshRenderer>().enabled = false;
                OffMesh();
            }
        }

        /// <summary>
        /// Установка id дефектным элементам
        /// </summary>
        void SetIdDefectsElement()
        {
            for (int i = 0; i < Defects.Count; i++)
            {
                Defects[i].id = i;
            }
        }


        #region MenuWithButton
        /// <summary>
        /// Menu whith button;
        /// </summary>
        //void CreateAnswerMenu()
        //{

        //    managerAnswerMenu.AddPanelUsedInventary(this, managerAnswerMenu.CreatePanelUsedInventary());

        //    GameObject temp = managerAnswerMenu.CreateButtonsPanel();
        //    managerAnswerMenu.AddPanelDictionary(this,temp.GetComponent<AnswersMenu>());
        //    am = managerAnswerMenu.GetAnswerMenu(this);

        //    if(!isDeffect)
        //    {
        //        print("!isDeffect");
        //        Button tempButton = am.CreateButton("Неисправность отсутствует");
        //        tempButton.onClick.AddListener(delegate () { CheckAnswer(tempButton); });
        //        am.AddButtonDictionary(tempButton, true);
        //    }
        //    else
        //    {
        //        print("isDeffect");
        //        Button tempButton = am.CreateButton("Неисправность отсутствует");
        //        tempButton.onClick.AddListener(delegate () { CheckAnswer(tempButton); });
        //        am.AddButtonDictionary(tempButton, false);
        //    }


        //    foreach (var item in textDefectsFalse)
        //    {
        //        Button tempButton = am.CreateButton(item);
        //        tempButton.onClick.AddListener(delegate () { CheckAnswer(tempButton); });
        //        am.AddButtonDictionary(tempButton, false);
        //    }

        //    foreach (var item in Defects)
        //    {
        //        Button tempButton = am.CreateButton(item.nameDefect);
        //        tempButton.onClick.AddListener(delegate() { CheckAnswer(tempButton); });

        //        if (isDeffect && item.gameObject.name.Equals(defectObject.name))
        //        {
        //            am.AddButtonDictionary(tempButton, true);
        //        }
        //        else
        //        {
        //            print("!isDeffect - Defect");
        //            am.AddButtonDictionary(tempButton, false);
        //        }
        //    }

        //    am.BlendButton();
        //    am.gameObject.SetActive(false);
        //}
        #endregion

        /// <summary>
        /// Создание меню с ответами
        /// </summary>
        void CreateAnswerMenu()
        {

            managerAnswerMenu.AddPanelUsedInventary(this, managerAnswerMenu.CreatePanelUsedInventary());

            GameObject temp = managerAnswerMenu.CreateButtonsPanel();
            managerAnswerMenu.AddPanelDictionary(this, temp.GetComponent<AnswersMenu>());
            am = managerAnswerMenu.GetAnswerMenu(this);

            if (!isDeffect)
            {
               
                Toggle tempToggle = am.CreateToggle("Неисправность отсутствует", am.GetComponent<ToggleGroup>());
                tempToggle.onValueChanged.AddListener( delegate { CheckAnswer(tempToggle); });
                am.AddToggleDictionary(tempToggle, true);
            }
            else
            {

                Toggle tempToggle = am.CreateToggle("Неисправность отсутствует", am.GetComponent<ToggleGroup>());
                tempToggle.onValueChanged.AddListener(delegate { CheckAnswer(tempToggle); });
                am.AddToggleDictionary(tempToggle, false);
            }


            foreach (var item in textDefectsFalse)
            {
                Toggle tempToggle = am.CreateToggle(item, am.GetComponent<ToggleGroup>());
                tempToggle.onValueChanged.AddListener(delegate { CheckAnswer(tempToggle); });
                am.AddToggleDictionary(tempToggle, false);
            }

            foreach (var item in Defects)
            {
                Toggle tempToggle = am.CreateToggle(item.nameDefect, am.GetComponent<ToggleGroup>());
                tempToggle.onValueChanged.AddListener(delegate { CheckAnswer(tempToggle); });
                

                if (isDeffect && item.gameObject.name.Equals(defectObject.name))
                {
                    am.AddToggleDictionary(tempToggle, true);
                }
                else
                {  
                    am.AddToggleDictionary(tempToggle, false);
                }
            }

            am.BlendButton();
            am.gameObject.SetActive(false);
        }

        /// <summary>
        /// Включение меню ответов
        /// </summary>
        public void OnAnswerMenu()
        {
            am.gameObject.SetActive(true);
            managerAnswerMenu.mainPanel.GetComponent<ScrollRect>().content = am.GetComponent<RectTransform>();
            am.GetComponent<RectTransform>().position = managerAnswerMenu.mainPanel.GetComponent<RectTransform>().position;
            managerAnswerMenu.GetPanelUsedInventary(this).SetActive(true);
            managerAnswerMenu.GetPanelUsedInventary(this).GetComponent<RectTransform>().position = managerAnswerMenu.mainPanelUsedInventary.GetComponent<RectTransform>().position;
            managerAnswerMenu.mainPanelUsedInventary.GetComponent<ScrollRect>().content = managerAnswerMenu.GetPanelUsedInventary(this).GetComponent<RectTransform>();
        }

        /// <summary>
        /// Выключение мкню ответов
        /// </summary>
        public void OffAnswerMenu()
        {
            am.gameObject.SetActive(false);
            managerAnswerMenu.GetPanelUsedInventary(this).SetActive(false);
        }

        /// <summary>
        /// Проверка правильности ответа
        /// </summary>
        /// <param name="b">Принимает выбранный ответ</param>
        void CheckAnswer(Button b)
        {
            if (!GameController.instance.isOtpuskTormozov)
            {
                GameController.instance.InspectBeforReleaseBreak = true;
            }
            if (am.GetValueButtonDictionary(b))
            {
                GameController.instance.inspectElement.Answer = true;
                //Debug.Log("Правильный ответ");
            }
            else
            {
                //Debug.Log("Неправельно");
            }
        }

        /// <summary>
        /// Проверка правильности ответа
        /// </summary>
        /// <param name="b">Принимает выбранный ответ</param>
        void CheckAnswer(Toggle b)
        {
            isInspect = true;
            if (!GameController.instance.isOtpuskTormozov)
            {
                GameController.instance.InspectBeforReleaseBreak = true;
            }
            if (am.GetValueToggleDictionary(b))
            {
                GameController.instance.inspectElement.Answer = true;
                //Debug.Log("Правильный ответ");
            }
            else
            {
                //Debug.Log("Неправельно");
            }
        }

        /// <summary>
        /// Отключение MeshRenderer если генерируется дефект
        /// </summary>
        void OffMesh()
        {
            MeshRenderer mr = GetComponent<MeshRenderer>();
            if (mr != null)
                mr.enabled = false;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
            }

        }

    }
}