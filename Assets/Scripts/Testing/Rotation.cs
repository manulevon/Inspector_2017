//-----------------------------------------------------------------------------
//
//      ������ �������� ������ ������ �������
//      ������ �� ���������
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// ����� ������� ������ �������� ������
/// </summary>
namespace KnowledgeCheck
{

    public class Rotation : MonoBehaviour {


	public Transform target;
    //public  GameObject Player;

	
	public int SpeedRotation = 1;
	public float targetHeight = 0f;
	public float  Distance = 2.8f;
	public float maxDistance = 10;
	public float minDistance = 0.5f;
	public float xSpeed = 250.0f;
	public float ySpeed = 120.0f;
	public float yMinLimit = -40;
	public float yMaxLimit = 80;
	public float zoomRote = 20;
	public float rotateionDampening = 3.0f;
	public float x = 0.0f;
	public float y = 0.0f;
	public bool isTalking = true;


	public void Start () {
            

            Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;

            //print(angles);
           
            if (target != null)
                Distance = Vector3.Distance(this.transform.position, target.position);

            if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
	
	}

        void OnEnable()
        {
            Vector3 angles = transform.eulerAngles;
            //print(angles);
            x = angles.y;
            y = angles.x;

            if (target != null)
                Distance = Vector3.Distance(this.gameObject.transform.position, target.position);
        }


        public void LateUpdate () {

		if (!target)
			return;
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;

            if (Input.GetMouseButton(1) && isTalking) {
			x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
			y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
			
		}  else if (Input.GetAxis ("Vertical") !=0 || Input.GetAxis ("Horizontal") !=0) {
		}
		
		if(isTalking) Distance -= (Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime) * zoomRote * Mathf.Abs (Distance);
		
		Distance = Mathf.Clamp(Distance, minDistance, maxDistance);

        y = ClampAngle(y, yMinLimit, yMaxLimit);
		
		Quaternion rotation = Quaternion.Euler (y, x, 0);
		transform.rotation = rotation; 
		
		Vector3 position = target.position - (rotation * Vector3.forward * Distance + new Vector3 (0, -targetHeight,0));
		transform.position = position;

        //RaycastHit hit;
        //Vector3 trueTargetPosition = target.transform.position - new Vector3(0, -targetHeight, 0);
        //if (Physics.Linecast(trueTargetPosition, transform.position, out hit))
        //{
        //    float tempDistance = Vector3.Distance(trueTargetPosition, hit.point) - 0.28f;
        //    position = target.position - (rotation * Vector3.forward * tempDistance + new Vector3(0, -targetHeight, 0));
        //    transform.position = position;

        //}

	}


    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);

    }

		public void SetTalking(bool _bool)
		{
			isTalking = _bool;
		}
}
}