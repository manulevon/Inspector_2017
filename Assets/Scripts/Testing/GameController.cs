﻿//-----------------------------------------------------------------------------
//
//     Мудуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Осматриваемый элемент
    /// </summary>
    public enum InspectElement { }

    /// <summary>
    /// Главный управляющий класс
    /// </summary>
    public class GameController : MonoBehaviour
    {
        /// <summary>
        /// Делегат осмотр
        /// </summary>
        /// <param name="e">Осматриваемый элемент</param>
        public delegate void Inspect(Element e);
        /// <summary>
        /// Событие начала осмотра
        /// </summary>
        public static event Inspect OnInspect;
        /// <summary>
        /// Событие окончание осмотра
        /// </summary>
        public static event Inspect OffInspect;

        /// <summary>
        /// Ссылка на firstPersonController
        /// </summary>
        public GameObject Player;

        /// <summary>
        /// Ссылка на изображение
        /// </summary>
        public Image TargetImage;
        /// <summary>
        /// Ссылка на текстуру для курсора
        /// </summary>
        public Texture2D cursor;
        /// <summary>
        /// Цвет обводки элементов
        /// </summary>
        public Color Outline;
        /// <summary>
        /// Ссылка на UIText
        /// </summary>
        public Text helper;
        /// <summary>
        /// Ссылка на текущий GameController
        /// </summary>
        public static GameController instance;
        /// <summary>
        /// Ссылка на камеру персонажа
        /// </summary>
        public Camera camPlayer;
        /// <summary>
        /// Переменная отвечающая за состояние игрока
        /// </summary>
        public bool isInspect = false;
        
        /// <summary>
        /// Ссылка на меню осмотра 
        /// </summary>
        public GameObject MenuInspect;
        /// <summary>
        /// Ссылка на меню ответов
        /// </summary>
        public ManagerAnswerMenu managerAnsverMenu;
        /// <summary>
        /// Ссылка на меню юкончания осмотра
        /// </summary>
        public GameObject PanelEnd;
        /// <summary>
        /// Текс результата осмотра
        /// </summary>
        public Text EndText;
        /// <summary>
        /// Заголовок
        /// </summary>
        public Text heder;
        /// <summary>
        /// Фонарик
        /// </summary>
        public Light flashlight;
        /// <summary>
        /// Номер сцены с окружением
        /// </summary>
        public int NumberSceneEnviroment;
        /// <summary>
        /// Осматриваемый элемент
        /// </summary>
        [HideInInspector]
        public Element inspectElement;
        /// <summary>
        /// Переменная отвечающая за конец осмотра
        /// </summary>
        [HideInInspector]
        public bool stop = false;
        /// <summary>
        /// Ссылка на звуковую дорожку
        /// </summary>
        [HideInInspector]
        public AudioSource source;
        /// <summary>
        /// Переменная отвечающая за отпуск тормозов
        /// </summary>
        [HideInInspector]
        public bool isOtpuskTormozov = false;
        /// <summary>
        /// Переменная отвечающая за отпуск тормозов в неправельной оследовательности
        /// </summary>
        [HideInInspector]
        public bool InspectBeforReleaseBreak = false;

        //Ссылка на компонент вращения камеры
        private Rotation _rotation;
        
        // Стартовая позиция камеры игрока
        private Vector3 startPosition;
        //Стартовое значение углов поворота камеры
        private Quaternion startRotation;
        //Лист элементов которые необходимо осмотреть
        private List<Element> listElements = new List<Element>();
        
        /// <summary>
        /// Метод вызываемый до Start() при запуске скрипта 
        /// </summary>
        private void Awake()
        {
            instance = this;
            Cursor.SetCursor(cursor, new Vector2(0,0),CursorMode.Auto );
            //Cursor.visible = false;
            //Cursor.lockState = CursorLockMode.Confined;
            //cursor.GetComponent<RectTransform>().position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            startPosition = camPlayer.transform.position;
            startRotation = camPlayer.transform.rotation;
            _rotation = camPlayer.GetComponent<Rotation>();
            listElements = new List<Element>(FindObjectsOfType<Element>());
            source = GetComponent<AudioSource>();
            LoadEnviroment(NumberSceneEnviroment);
        }


        /// <summary>
        /// Аддативная подгрузка окружения
        /// </summary>
        /// <param name="number"></param>
        public void LoadEnviroment(int number = 0)
        {
            SceneManager.LoadScene(number, LoadSceneMode.Additive);
        }

        /// <summary>
        /// Перемещение камеры от игрока к точке осмотра элемента
        /// </summary>
        /// <param name="position">Позирция в которую необходимо переместить камеру</param>
        /// <param name="lookAt">Объект на который необходимосмотреть</param>
        public void EnableCamFly(GameObject position, GameObject lookAt)
        {
            // Остановка всех сопрограмм
            StopAllCoroutines();

            //Стартовые значения позиции и поворота камеры
            startPosition = camPlayer.transform.position;
            startRotation = camPlayer.transform.rotation;
            
            //Убираем родительский объект
            camPlayer.gameObject.transform.SetParent(null);
            //Отключаем FirstPersonController
            Player.SetActive(false);

            //camPlayer.gameObject.SetActive(false);
            // Запускаем сопрограмму по перемещению камеры
            StartCoroutine(EnableCamFlyCoroutine(position, lookAt));
        }

        /// <summary>
        /// Метод завершения осмтра элемента
        /// </summary>
        public void DisabledCamFly()
        {
            //Остановка всех сопрограмм
            StopAllCoroutines();
            // Выключение меню осмотра
            MenuInspect.SetActive(false);
            //Выключение компонента вращения камеры
            _rotation.enabled = false;

            if (OffInspect != null) OffInspect(inspectElement);
            // Запуск сопрограммы перемещения камеры
            StartCoroutine(DisableCamFlyCoroutine(startPosition, startRotation));
        }
        /// <summary>
        /// Сопрограмма по перемещению камеры
        /// </summary>
        /// <param name="position">Позиция в которую необходимо переместить камеру</param>
        /// <param name="lookAt">Объект на который необходимо смотреть камере</param>
        /// <returns></returns>

        IEnumerator EnableCamFlyCoroutine (GameObject position, GameObject lookAt)
        {
            
            bool ischeck = true;
            //print( "Rotation: " + Quaternion.Dot(camFly.transform.rotation, Quaternion.LookRotation(lookAt.transform.position - camFly.transform.position)));
            //print( "Position" + Vector3.Distance(camFly.transform.position, position.transform.position));

            float speed = Vector3.Distance(camPlayer.transform.position,position.transform.position);
            if (speed < 1) speed = 2;
            while (ischeck)
            {
//                print("WorkCoroutineEnabled");

                //print("1: " + (camFly.transform.position != position.transform.position));
                //print("2: " + (camFly.transform.rotation != Quaternion.LookRotation(lookAt.transform.position - camFly.transform.position)));
                //print("work Coroutine");
                camPlayer.transform.position = Vector3.MoveTowards(camPlayer.transform.position, position.transform.position, speed*Time.deltaTime);

                Vector3 relativePos = lookAt.transform.position - camPlayer.transform.position;
                camPlayer.transform.rotation = Quaternion.RotateTowards(camPlayer.transform.rotation, Quaternion.LookRotation(relativePos), speed*50*Time.deltaTime);
                if (camPlayer.transform.position == position.transform.position && camPlayer.transform.rotation == Quaternion.LookRotation(lookAt.transform.position - camPlayer.transform.position)) ischeck = false;


                yield return null;
            }
            //print("1: " + (camFly.transform.position != position.transform.position));
            //print("2: " + (camFly.transform.rotation != Quaternion.LookRotation(lookAt.transform.position - camFly.transform.position)));

            _rotation.Distance = Vector3.Distance(position.transform.position, lookAt.transform.position);
            _rotation.target = lookAt.transform;
            _rotation.enabled = true;
            MenuInspect.SetActive(true);
//            Cursor.lockState = CursorLockMode.None;
//             Cursor.visible = true;

            if (OnInspect != null) OnInspect(inspectElement);

            StopAllCoroutines();

        }

        /// <summary>
        /// Метод который выполняется каждый кадр
        /// </summary>
        private void Update()
        {
            
                // Обработчик события нажатия на клавишу "F" 
                if (Input.GetKeyDown(KeyCode.F))
                {
                // Включение и выключение фонарика
                    if (flashlight.gameObject.activeSelf)
                        flashlight.gameObject.SetActive(false);
                    else
                        flashlight.gameObject.SetActive(true);
                }
        
            //if (Input.GetKey(KeyCode.Escape))
            //{
            //    if (isInspect)
            //    {
            //        isInspect = false;
            //        DisabledCamFly();
            //        heder.text = " Проверка знаний";
            //    }
            //}
            
            //if (Input.GetKey(KeyCode.Space)) print(Random.Range(0,3));
        }

        /// <summary>
        /// Метод выполняемый в конце кадра
        /// </summary>
        private void LateUpdate()
        {
            //cursor.GetComponent<RectTransform>().position = new Vector3(
                                                                         //Input.mousePosition.x + cursor.GetComponent<RectTransform>().sizeDelta.x / 2,
                                                                         //Input.mousePosition.y - cursor.GetComponent<RectTransform>().sizeDelta.y / 2,
                                                                         //Input.mousePosition.z);
        }

        /// <summary>
        /// Сопрограмма по перемещению камер и выходу из состояния осмотра
        /// </summary>
        /// <param name="position">Позиция куда необходимо переместить камеру</param>
        /// <param name="lookAt">Куда необходимо смотреть камере</param>
        /// <returns></returns>
        IEnumerator DisableCamFlyCoroutine(Vector3 position, Quaternion lookAt)
        {
           

            bool ischeck = true;
            float speed = Vector3.Distance(camPlayer.transform.position,position);
            if (speed < 1) speed = 2;
            
            while (ischeck)
            {
//                print("WorkCoroutineDisabled");
                camPlayer.transform.position = Vector3.MoveTowards(camPlayer.transform.position, position, speed * Time.deltaTime);


                camPlayer.transform.rotation = Quaternion.RotateTowards(camPlayer.transform.rotation, lookAt,  speed*50 * Time.deltaTime);
                
//                if (camPlayer.transform.position == position && camPlayer.transform.rotation == lookAt)
//                {
//                    ischeck = false;
//                    print("cam.pos = " + camPlayer.transform.position + "pos = " + position);
//                    print("cam.rotation = " + camPlayer.transform.rotation + "loock = " + lookAt);
//                }

                if (camPlayer.transform.position == position && Abs(camPlayer.transform.rotation) == Abs((lookAt)))
                {
                    ischeck = false;
//                    print("cam.pos = " + camPlayer.transform.position + "pos = " + position);
//                    print("cam.rotation = " + camPlayer.transform.rotation + "loock = " + lookAt);
                }

                yield return null;
            }

//            print("cam.pos = " + camPlayer.transform.position + "pos = " + position);
//            print("cam.rotation = " + camPlayer.transform.rotation + "loock = " + lookAt);

            
            
//            print("EndCoroutineDisable");
                Player.SetActive(true);
                camPlayer.transform.SetParent(Player.transform);
//              Cursor.lockState = CursorLockMode.Locked;
                isInspect = false;
//            print("EndCoroutineDisable");
                StopAllCoroutines();
//            print("EndCoroutineDisable");

        }

        /// <summary>
        /// Определение модуля кватерниона
        /// </summary>
        /// <param name="q">Кватернион</param>
        /// <returns>Возварщает кватернион с значениями по модулю</returns>
        Quaternion Abs( Quaternion q)
        {
            return new Quaternion(Mathf.Abs(q.x),Mathf.Abs(q.y),Mathf.Abs(q.z),Mathf.Abs(q.w));
        }

        /// <summary>
        /// Анализ результата осмотра
        /// </summary>
        /// <param name="str">Передается значение текста если истекло время</param>
        public void EndInspect(string str = "")
        { 
            string releaseOfBreakes = "";
            
            int countInspect = 0;
            foreach (var item in listElements)
            {
                if (item.isInspect) countInspect++;

            }
            int countNonInspect = listElements.Count - countInspect;
            string Inspect = "Осмотрено элементов: " + countInspect + "\n";
            string NonIspect = "Неосмотрено элементов: " + countNonInspect + "\n";

            int countTrue = 0;

            foreach (var item in listElements)
            {
                if (item.Answer) countTrue++;
     
            }

           
                if (InspectBeforReleaseBreak)
                {
                    releaseOfBreakes = "Начал осмотр не выполнив отпук тормозов\n"; 
                }
                else if (!isOtpuskTormozov)
                {
                    releaseOfBreakes = "Не выполнил отпук тормозов\n";   
                }
            

            int countFalse = countInspect -  countTrue;
            string trueAnswere = "Число правильно определенных  дефектов: " + countTrue + "\n";
            string falseAnswere = "Число неправильно определенных  дефектов: " + countFalse + "\n";

            int kol2 = 0;

            foreach (var item in listElements)
            {
              
                foreach (var temp in item.usedInventary)
                {
                     kol2++;
                }
            }


                int kol3 = 0;
            foreach (var item in listElements)
            {
                foreach (var temp in item.usedInventary)
                {
                   
                    if(temp.Value) kol2--;

                }

                foreach (var temp in item.falseInstrument)
                {
                    kol3++;
                }
            }

            string inventaryUse = "Не использовал инструмент: " + kol2 + "\n";

            string falseUseInventary = "Число не правильно использованного инструмента: " + kol3 +"\n";

            string time = "Время затраченное на осмотр: " + Timer.timeText;
           

            EndText.text = str + releaseOfBreakes + Inspect + NonIspect + trueAnswere + falseAnswere + inventaryUse + falseUseInventary + time;

            PanelEnd.SetActive(true);
            stop = true;
            
        }

        /// <summary>
        /// Загрузка сцены по индексу
        /// </summary>
        /// <param name="a">Номер сцены</param>
        public void LoadLevel(int a)
        {
            SceneManager.LoadScene(a);
        }

        /// <summary>
        /// Завершение осмотра элемента
        /// </summary>
        public void Inspectуed()
        {
            if (isInspect)
            {
                DisabledCamFly();
                heder.text = " Проверка знаний";
            }
        }
    }
}