﻿//-----------------------------------------------------------------------------
//
//       Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Класс описывающий инструмент
    /// </summary>
    public class Inventary: MonoBehaviour
    {
        /// <summary>
        /// Тип инструмента
        /// </summary>
        public TypeInventary typeInventary;
        /// <summary>
        /// Модель инструмента
        /// </summary>
        public GameObject Modele;
        
        /// <summary>
        /// Метод выполняемый до Start() при запуске скрипта
        /// </summary>
        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(delegate() { UsedInventary(); });
        }

        /// <summary>
        /// Использовать инструмент
        /// </summary>
        public void UsedInventary()
        {
            //Ссылка на объект
            GameObject temp = null;
            //Счетчик
            int k = 0;
            //Циклом проверяем необходимость использование данного инструмента в к осматриваемогу элементу
            foreach (var item in GameController.instance.inspectElement.trueinventary)
            {
               //Получение компонента используемый инструмент с осматриваемого элемента
                UsedInventary tmp = item.GetComponent<UsedInventary>();
                //Проверяем тип инструмента
                if (tmp.typeinventary == this.typeInventary)
                {
                    k++;
                }
            }

            //Циклом проверяем неправильность использования инструмента к осматриваемогу элементу
            foreach (var item in GameController.instance.inspectElement.falseinventary)
            {
               
                UsedInventary tmp = item.GetComponent<UsedInventary>();
                if (tmp.typeinventary == this.typeInventary)
                {
                    k++;
                }
                

            }

            // Добавление инструмента в панель использованного инвентаря
            if (k == 0)
                temp = AddUsedInventaryToElement();


            int kol = 0;
            //Добавление инструмента в список правильно использованного для текущего элемента
            foreach (var item in GameController.instance.inspectElement.InventaryList)
            {
               
                if (item.typeInventary.Equals(typeInventary))
                {
                    //print("Я здесь есть");
                     GameController.instance.inspectElement.usedInventary[this] = true;
                    //print(GameController.instance.inspectElement.usedInventary[this]);
                    kol++;
                    if(temp!= null)
                    GameController.instance.inspectElement.trueinventary.Add(temp);
                }  
            }
            //Добавление инструмента в список неправильно использованного для текущего элемента
            foreach (var item in GameController.instance.inspectElement.falseInstrument)
            {
                if (item.typeInventary == this.typeInventary) kol++;
            }

            if (kol == 0)
            {
                //print("Меня здесь нет");
                GameController.instance.inspectElement.falseInstrument.Add(this);
                if(temp != null)
                GameController.instance.inspectElement.falseinventary.Add(temp);

                //print(GameController.instance.inspectElement.falseInstrument[GameController.instance.inspectElement.falseInstrument.IndexOf(this)]);
            }
        }

        /// <summary>
        /// Добавление инструмента в панель использованного инвентаря
        /// </summary>
        /// <returns></returns>
        public GameObject AddUsedInventaryToElement()
        {
            // Создаем кнопку использованного инструмента
            GameObject temp = Instantiate(this.gameObject);
            //Устанавливаем панель использованных инструментов в качестве родительского объекта
            temp.transform.SetParent(GameController.instance.managerAnsverMenu.GetPanelUsedInventary(GameController.instance.inspectElement).transform);
            //Получаем компонент UsedInventary
            UsedInventary ui = temp.AddComponent<UsedInventary>();
            // Устанавливаем тип инструмента
            ui.typeinventary = this.typeInventary;
            // Передаем метод который необходимо выполнить по нажатию на кнопку
            temp.GetComponent<Button>().onClick.AddListener(delegate () { ui.OnClick(); });
            // Удаляем метод который необходимо выполнить по нажатию на кнопку
            temp.GetComponent<Button>().onClick.RemoveListener(delegate () { UsedInventary();});
            //Удаление ненужного компонента
            DestroyImmediate(temp.GetComponent<Inventary>());
            //Возвращаем созданную кнопку
            return temp;
        }
        
    }
}
