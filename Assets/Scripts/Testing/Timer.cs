﻿//-----------------------------------------------------------------------------
//
//     Мудуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 13/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Обратный таймер
    /// </summary>
    public class Timer : MonoBehaviour
    {
        /// <summary>
        /// Текущее время
        /// </summary>
        public static float TimeInspect = 0;
        /// <summary>
        /// Врея прохождение задания
        /// </summary>
        public float EndTime = 10;
        /// <summary>
        ///Ссылка на UI text
        /// </summary>
        public Text textTime;
        /// <summary>
        /// Текст выводимый на экран
        /// </summary>

        public static string timeText;
        
        // Минуты
       private  int hours;
        

        // Ссылка на запущеную сопрограмму 
        private Coroutine coroutine;

        /// <summary>
        /// Метод выполняемый при включении скрипта
        /// </summary>
        /// 
        void Start()
        {
            //Запуск сопрограммы
            coroutine = StartCoroutine(TimerCoroutine());
        }
        /// <summary>
        /// Метод который исполняется каждый кадр
        /// </summary>
        private void Update()
        {
            //Перевод секунд в минуты
            if (Mathf.Round(TimeInspect) == 60)
            {
                TimeInspect = 0;
                hours += 1;
            }
            // Текст для отрисовки на экране
            timeText = hours + ":" + (Mathf.Round(TimeInspect)).ToString();
            // Передача текста в UI элемент
            textTime.text =timeText;
        }

        /// <summary>
        /// Сопрограмма реализующая таймер
        /// </summary>
        IEnumerator TimerCoroutine()
        {
            // Цикл просчитывает время пройденное с начала запуска 
            while (((Mathf.Round(TimeInspect))/100 + hours) != EndTime)
            {
                // Добавляем время между кадрами
                TimeInspect += Time.deltaTime;
                // Проверка остановлено ли выполнение задания
                if( GameController.instance.stop)
                    //Останавливаем таймер
                    StopCoroutine(coroutine);
                //Ждём кадр
                yield return null;
            }
            //Вызываем метод завершения выполнения задания
            GameController.instance.EndInspect("Время вышло!\n");
            //Остонавливаем сопрограмму
            StopCoroutine(coroutine);
        }

        /// <summary>
        /// Сброс таймера
        /// </summary>
        private void OnDestroy()
        {
            TimeInspect = 0;
        }
    }
}