﻿//-----------------------------------------------------------------------------
//
//      Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 13/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Клас реализует использование инструментов
    /// </summary>
    public class UsedInventary : MonoBehaviour {

        /// <summary>
        /// Тип инвентаря
        /// </summary>
        public TypeInventary typeinventary;

        /// <summary>
        ///  Удаляет инструменты в меню использованного инструмента
        /// </summary>
        public void OnClick()
        {
            //проверка правильности использованого инструмента
            if (GameController.instance.inspectElement.trueinventary.Contains(this.gameObject))
            {
                // Удаление из листа элемента
                GameController.instance.inspectElement.trueinventary.Remove(this.gameObject);
            
                // Перебераем словарь использованых инструментов и помечаем инструмент, как не использованый
                foreach (var item in GameController.instance.inspectElement.usedInventary)
                {
                    if (this.typeinventary == item.Key.typeInventary)
                    {
                        GameController.instance.inspectElement.usedInventary[item.Key] = false;
                        break;
                    }
                }

            }
            else
            {
                // Удаление инструмента из списка неправильно использованных инструментов
                GameController.instance.inspectElement.falseinventary.Remove(this.gameObject);
                foreach (var item in GameController.instance.inspectElement.falseInstrument)
                {
                    if (this.typeinventary == item.typeInventary)
                    {
                        GameController.instance.inspectElement.falseInstrument.Remove(item);
                        break;
                    }
                    
                }
            }

            //Удаление объекта из сцены
            DestroyImmediate(this.gameObject);
        }
    }
}