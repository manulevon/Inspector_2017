﻿//-----------------------------------------------------------------------------
//
//       Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Набор классов модуля проверки знаний
/// Created 05/05/2017 9:00
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Клас описывающий меню ответов для элемента
    /// </summary>
    public class AnswersMenu : MonoBehaviour
    {
        /// <summary>
        /// Словарь кнопок с оветами
        /// </summary>
        public Dictionary<Button, bool> buttonDictionarty = new Dictionary<Button, bool>();

        /// <summary>
        /// Словарь Toggle 
        /// </summary>
        public Dictionary<Toggle, bool> toggleDictionary = new Dictionary<Toggle, bool>();
        
        /// <summary>
        /// Префаб кнопки ответа  
        /// </summary>
        [SerializeField]
        private Button prefabButton;

        /// <summary>
        /// Ссылка на панель с кнопками
        /// </summary>
        private GameObject PanelButton;
        /// <summary>
        /// Префаб ответа Toggle 
        /// </summary>
        public Toggle prefabToggle;
      
        /// <summary>
        /// Создание кнопки
        /// </summary>
        /// <param name="textButton">Название ответа</param>
        /// <returns>Возвращает созданную кнопку</returns>
        public Button CreateButton(string textButton)
        {
            Button temp =  Instantiate(prefabButton.gameObject).GetComponent<Button>();
            temp.transform.SetParent(this.transform);
            SetTextButton(temp, textButton);
            return temp;
        }

       /// <summary>
       /// Создание Toggle элемента с ответом
       /// </summary>
       /// <param name="t">название ответа</param>
       /// <param name="tg">объект ToggleGroup</param>
       /// <returns>Возвращает созданный  Toggle</returns>
        public Toggle CreateToggle(string t, ToggleGroup tg)
        {
            Toggle temp = Instantiate(prefabToggle);
            temp.transform.SetParent(this.transform);
            temp.transform.GetChild(0).GetComponent<Text>().text = t;
            temp.group = tg;
            return temp;
        }

        /// <summary>
        /// Добавление кнопки в словарь
        /// </summary>
        /// <param name="b">Кнопка с ответом</param>
        /// <param name="isDefect">Правильный или неправельный ответ</param>
        public void AddButtonDictionary(Button b, bool isDefect)
        {
            buttonDictionarty.Add(b, isDefect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b">Toggle с ответом</param>
        /// <param name="isDefect">Правильный или неправельный ответ</param>
        public void AddToggleDictionary(Toggle b, bool isDefect)
        {
            toggleDictionary.Add(b, isDefect);
        }

        /// <summary>
        /// Установка текста в кнопку
        /// </summary>
        /// <param name="b">Кнопка</param>
        /// <param name="text">Название ответа</param>
        public void SetTextButton(Button b, string text)
        {
            b.transform.GetChild(0).GetComponent<Text>().text = text;
        }

        /// <summary>
        /// Получить значение ответа
        /// </summary>
        /// <param name="b">Кнопка</param>
        /// <returns>Возвращает правелиный или не правельный ответ</returns>
        public bool GetValueButtonDictionary(Button b)
        {
            return buttonDictionarty[b];
        }

        /// <summary>
        /// Получить значение Toggle правильности ответа
        /// </summary>
        /// <param name="b">Toggle</param>
        /// <returns>Возвращает правельный или не правильный ответ</returns>
        public bool GetValueToggleDictionary(Toggle b)
        {
            return toggleDictionary[b];
        }

        /// <summary>
        /// Перемешивает ответы
        /// </summary>
        public void BlendButton()
        {
            for (int i = 1; i < transform.childCount; i++)
            {
                
                transform.GetChild(i).transform.SetSiblingIndex(Random.Range(1, transform.childCount+1));
            }
        }

    }
}