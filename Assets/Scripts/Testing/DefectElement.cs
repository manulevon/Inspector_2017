﻿//-----------------------------------------------------------------------------
//
//       Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Класс описывающий дефектный элемент
    /// </summary>
    public class DefectElement : MonoBehaviour
    {
        /// <summary>
        /// Название дефекта
        /// </summary>
        public string nameDefect;

        /// <summary>
        /// НИдекс дефекта
        /// </summary>
        [HideInInspector]
        public int id;

    }
}
