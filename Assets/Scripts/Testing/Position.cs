﻿//-----------------------------------------------------------------------------
//
//     Мудуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Клас не используется
    /// Клас хранения позиций осмотра
    /// </summary>
    class Position : MonoBehaviour
    {
        /// <summary>
        /// Список элементов которые необходимо осматривать 
        /// </summary>
        public List<Element> elements;
        /// <summary>
        /// Списки дефектов элементов
        /// </summary>
        public List<List<GameObject>> elementDefects;

        /// <summary>
        /// Метод если в тригер кто-то/что-то вошел/вошло
        /// </summary>
        /// <param name="other">Объект с которым произошло столкновение</param>
        private void OnTriggerEnter(Collider other)
        {
            // Провеерка объекта по тегу
            if (other.tag == "Player")
            {
                //Вывод текста на экране
                GameController.instance.helper.text = "Нажмите E для активации позиции, послечего элементы осмотра сатанут активными";
            }
        }

        /// <summary>
        /// Метод срабатывает при выходе из триггера
        /// </summary>
        /// <param name="other">Объект вышедший из триггера</param>
        private void OnTriggerExit(Collider other)
        {
            //Проверка объекта по тегу
            if (other.tag == "Player")
            {
                // Исчезновение надписи на дисплее
                GameController.instance.helper.text = "";
            }
        }
    }
}
