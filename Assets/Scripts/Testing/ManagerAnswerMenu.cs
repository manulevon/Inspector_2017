﻿//-----------------------------------------------------------------------------
//
//       Модуль проверки знаний
//      (с) РГУПС, ОИТП ЦРИК 14/07/2017
//      Разработал: Манучарян Л.Х.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Набор классов модуля проверки знаний
/// </summary>
namespace KnowledgeCheck
{
    /// <summary>
    /// Контроллер меню ответов
    /// Created 05/05/2017 9:00
    /// </summary>
    public class ManagerAnswerMenu : MonoBehaviour {

        /// <summary>
        /// Ссылка на панель с кнопками ответов
        /// </summary>
        public GameObject prefabButtonsPanel;
        /// <summary>
        /// Ссылка на главную панель
        /// </summary>
        public GameObject mainPanel;
        /// <summary>
        /// Ссылка на префаб кнопки использованного инвентоаря
        /// </summary>
        public GameObject prefabUsedInventaryButton;
        /// <summary>
        /// Ссылка на главную панель использованного инвентаря
        /// </summary>
        public GameObject mainPanelUsedInventary;

        // Словарь меню ответов для конкретного элемента
        private Dictionary<Element, AnswersMenu> dictionaryAnswerMenu = new Dictionary<Element, AnswersMenu>();
        // Словарь использованных инструментов для каждого элемента
        private Dictionary<Element, GameObject> dictionaryUsedInventary = new Dictionary<Element, GameObject>();

        /// <summary>
        /// Метод выполняемый при запуске скрипта до выполнения Start();
        /// </summary>
        private void Awake()
        {
            // Подписка метода на событие начало осмотра
            GameController.OnInspect += OnAnswerMenu;
            // Подписка метода на событие конец осмотра
            GameController.OffInspect += OffAnswerMenu;
        }

        /// <summary>
        /// Создание панели с кнопками
        /// </summary>
        /// <returns></returns>
        public GameObject CreateButtonsPanel()
        {
            // Создание объекта панели для кнопок
            GameObject temp = Instantiate(prefabButtonsPanel);
            // Установка для панели родительского объекта (Главная панель)
            temp.transform.SetParent(mainPanel.transform);
            // Вернуть созданную панель
            return temp;
        }

        /// <summary>
        /// Создание панели использованного инвентаря
        /// </summary>
        /// <returns>Djpdhfoftn созданную панель</returns>
        public GameObject CreatePanelUsedInventary()
        {
            // Создание панели
            GameObject temp = Instantiate(prefabUsedInventaryButton);
            // Установка для панели родительского объекта (Главная панель использованного инстроумента)
            temp.transform.SetParent(mainPanelUsedInventary.transform);
            // Выключение объекта
            temp.SetActive(false);
            // Вернуть созданную панель
            return temp;
        }

        /// <summary>
        /// Добавление в словарь панели использованных инструментов
        /// </summary>
        /// <param name ="elem">Элемент который необходимо осмотреть</param>
        /// <param name="panelUsedInventary">Панель использованных инструментов</param>
        public void AddPanelUsedInventary(Element elem, GameObject panelUsedInventary)
        {
            // Добавление панели в словарь
            dictionaryUsedInventary.Add(elem,panelUsedInventary);
        }

        /// <summary>
        /// Получить панель по ключу (Элемент)
        /// </summary>
        /// <param name="elem"> Осматриваемый элемент</param>
        /// <returns>Возвращает панель использованных инструментов</returns>
        public GameObject GetPanelUsedInventary(Element elem)
        {
            return dictionaryUsedInventary[elem];
        }

        /// <summary>
        /// Добавляет в словарь меню ответов для конкретного элемента
        /// </summary>
        /// <param name="elem">Осматриваемый элемент</param>
        /// <param name="am"> Меню ответов</param>
        public void AddPanelDictionary(Element elem, AnswersMenu am)
        {
            //Добавление в словарь элементов
            dictionaryAnswerMenu.Add(elem, am);
        }

        /// <summary>
        /// Получить панель ответов по ключу (Элемент)
        /// </summary>
        /// <param name="elem"> Осматриваемый элемент</param>
        /// <returns></returns>
        public AnswersMenu GetAnswerMenu (Element elem)
        {
            // Возвращает панель ответов по ключу
            return dictionaryAnswerMenu[elem] ;
        }

        /// <summary>
        /// Включить меню ответов осматриваемого элемента
        /// </summary>
        /// <param name="e"> Осматриваемый элемент</param>
        public void OnAnswerMenu(Element e)
        {
            // Вызов метода включения 
            e.OnAnswerMenu();
        }

        /// <summary>
        /// Выключить меню ответов осматриваемого элемента
        /// </summary>
        /// <param name="e"> Осматриваемый элемент</param>
        public void OffAnswerMenu(Element e)
        {
            // Вызов метода выключения 
            e.OffAnswerMenu();
        }
    }
}