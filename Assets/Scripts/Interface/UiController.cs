﻿//-----------------------------------------------------------------------------
//
//      UI 
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace newLogic
{
    /// <summary>
    /// Класс, отвечающий за работу UI
    /// </summary>
    public class UiController : MonoBehaviour
    {

        // Экземпляр класса MyCoroutine
        private MyCoroutine _myCoroutine;

        /// <summary>
        /// Ссылка на аниматор стартовой панели
        /// </summary>
        public Animator PanelStart;
        /// <summary>
        /// Ссылка на аниматор панели практического модуля
        /// </summary>
        public Animator PanelTraining;
        /// <summary>
        /// Ссылка на аниматор панели изучения
        /// </summary>
        public Animator PanelLearning;
        /// <summary>
        /// Текстура курсора
        /// </summary>
        public Texture2D cursor;
        /// <summary>
        /// Трансформ UI объекта 
        /// </summary>
        public RectTransform Arrow;
        //Ссылка на аниматор
        private Animator _animator;

        /// <summary>
        /// Метод выполняется при включении скрипта до метода Старт
        /// </summary>
        void Awake()
        {
            //Установка текстуры курсора
            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
            //Создание экземпляра класса 
            _myCoroutine = new MyCoroutine();

        }
        /// <summary>
        /// Метод выполняется при включении скрипта после метода Awake
        /// </summary>
        void Start()
        {
            if (!Elements.kastil)
                StartCoroutine(_myCoroutine.InvokeCoroutine(.2f, () => { PanelStart.SetTrigger("PanelOpen"); }));
            else
                StartCoroutine(_myCoroutine.InvokeCoroutine(.2f,
                    () => { PanelLearning.SetTrigger("PanelOpenInviz"); }));
        }

        /// <summary>
        /// Инициализируем _animator необходимым аниматором
        /// </summary>
        /// <param name="animator">Аниматор</param>
    public void IdentifyButton(Animator animator)
        {
            _animator = animator;
        }

        /// <summary>
        /// Запускает анимацию открытия панели
        /// </summary>
        /// <param name="animator">Аниматор объекта на котором необходимо проиграть анимацию</param>
        public void OpenPanel(Animator animator)
        {
            _animator.SetTrigger("PanelClose");
            StartCoroutine(_myCoroutine.InvokeCoroutine(.5f, () => { animator.SetTrigger("PanelOpen"); }));
        }

        /// <summary>
        /// Запускает переход к другой сцен
        /// </summary>
        /// <param name="numberScene">Номер сцены</param>
        public void OpenPanel(int numberScene)
        {

            _animator.SetTrigger("PanelClose");
            PlayerPrefs.SetInt("Level", numberScene);
            StartCoroutine(_myCoroutine.InvokeCoroutine(1.1f, () => { SceneManager.LoadScene("Loader"); }));

//            StartCoroutine(_myCoroutine.InvokeCoroutine(1f, () => {_animator.enabled = false; }));
        }

        /// <summary>
        /// Открывает/закрывает UI панель
        /// </summary>
        /// <param name="animator">Аниматор объекта на котором необходимо проиграть анимацию</param>
        public void ClosePanelTraining(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                PanelTraining.SetTrigger("PanelCloseOn");
            }
            else
            {
                PanelTraining.SetTrigger("PanelCloseOff");
            }
            StartCoroutine(_myCoroutine.InvokeCoroutine(.5f, () => { animator.SetTrigger("PanelOpenInviz"); }));
        }

        /// <summary>
        /// Скрывавет/открвает вспомогательное меню
        /// </summary>
        /// <param name="animator">Аниматор объекта на котором необходимо проиграть анимацию</param>
        public void HelpApDown(Animator animator)
        {
            if (Arrow.localScale.y == -1)
            {
                animator.SetTrigger("PanelDown");
            }
            else
            {
                animator.SetTrigger("PanelUp");
            }
        }

        /// <summary>
        /// Закрытие программы
        /// </summary>
        public void Exit()
        {
            PanelLearning.SetTrigger("PanelClose");
            StartCoroutine(_myCoroutine.InvokeCoroutine(1.15f, () => {Application.Quit();}));
        }
        /// <summary>
        /// Выключает кнопку
        /// </summary>
        /// <param name="button">Кнопку</param>
        public void InactiveButton(Button button)
        {
            button.enabled = false;
        }
    }
}
