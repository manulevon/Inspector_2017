﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Реализует окно в Editor Unity's
    /// </summary>
    public class InventoryItemEditor : EditorWindow
    {
        /// <summary>
        /// Данные, которые необходимо отображать в окне
        /// </summary>
        public InventoryItemList InventoryItemList;
        /// <summary>
        /// Ссылка на главный контроллер
        /// </summary>
        public GameObject GameController;

        //Номер объекта в списке при добавлении
        private int numberAdd;
        //Номер удаляемого объекта
        private int numberRemove;
        // Позиция скролла
        private Vector2 scrollPosition = Vector2.zero;

        /// <summary>
        /// Метод инициализирует эдиторовское окно
        /// </summary>
        [MenuItem("Window/Inventory Items Editor %#e")]
        static void Init()
        {
            GetWindow(typeof(InventoryItemEditor));
        }

        /// <summary>
        /// Метод срабатывает при включении окна
        /// </summary>
        void OnEnable()
        {
            //Проверяет существует ли файл по указаному пути
            // Если существует то загрузить
            if (EditorPrefs.HasKey("ObjectPath"))
            {

                string objectPath = EditorPrefs.GetString("ObjectPath");
                InventoryItemList =
                    AssetDatabase.LoadAssetAtPath(objectPath, typeof(InventoryItemList)) as InventoryItemList;
            }

//        GameObject[] a = FindObjectsOfType<GameObject>();
//        foreach (var VARIABLE in InventoryItemList.InventoryItems)
//        {
//            foreach (var Game in a)
//            {
//                if (Game.GetComponent<Transform>().GetHashCode().ToString() == VARIABLE.HashBackLighting)
//                {
//                    VARIABLE.backLighting = Game;
//                    continue;
//                }
//                if (Game.GetComponent<Transform>().GetHashCode().ToString() == VARIABLE.HashGameObject)
//                {
//                    VARIABLE.gameObject = Game;
//                }
//            }
//        }

            //Находим  главный контроллер по тэгу
            GameController = GameObject.FindWithTag("GameController");
            //Заполнение некоторых списков в окне эдитораданными из главного контроллера
            for (int i = 0; i < GameController.GetComponent<Elements>().ListLook.Count; i++)
            {
                InventoryItemList.InventoryItems[i].gameObject =
                    GameController.GetComponent<Elements>().ListLook[i].gameObject;
                InventoryItemList.InventoryItems[i].subtitle = GameController.GetComponent<Elements>().ListSubtitle[i];
                InventoryItemList.InventoryItems[i].audioClip =
                    GameController.GetComponent<Elements>().ListAudiclips[i];
                InventoryItemList.InventoryItems[i].backLighting =
                    GameController.GetComponent<Elements>().ListHightlightObjects[i];
                InventoryItemList.InventoryItems[i].isChilds = GameController.GetComponent<Elements>().IsChild[i];
                InventoryItemList.InventoryItems[i].isPosition = GameController.GetComponent<Elements>().IsPosition[i];
            }

        }


        /// <summary>
        /// Исполняется каждый кадр
        /// </summary>
        void OnGUI()
        {
            //Находим  главный контроллер по тэгу
            GameController = GameObject.FindWithTag("GameController");

            //длина ячеек в окне эдитора
            float width = 130;

            //Реализация скролл бара для эдиторовского окна
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(position.width),
                GUILayout.Height(position.height));
            //Отображение данных в окне эдитора 
            GUILayout.Label("Elements Settings", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(width / 3));
            GUILayout.Label("gameObject", EditorStyles.boldLabel, GUILayout.Width(width));
            GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(width));
            GUILayout.Label("audioClip", EditorStyles.boldLabel, GUILayout.Width(width / 1.5f));
            GUILayout.Label("backLighting", EditorStyles.boldLabel, GUILayout.Width(width));
            GUILayout.Label("isGhilds", EditorStyles.boldLabel, GUILayout.Width(width / 2));
            GUILayout.Label("isPosition", EditorStyles.boldLabel, GUILayout.Width(width / 2));
//            GUILayout.Label("defect1", EditorStyles.boldLabel, GUILayout.Width(width));
//            GUILayout.Label("defect2", EditorStyles.boldLabel, GUILayout.Width(width));
//            GUILayout.Label("defect3", EditorStyles.boldLabel, GUILayout.Width(width));
            GUILayout.EndHorizontal();

            if (InventoryItemList != null)
            {
                //Отображение списков в окне эдитора
                for (int i = 0; i < InventoryItemList.InventoryItems.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(width / 3));
                    InventoryItemList.InventoryItems[i].gameObject =
                        EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].gameObject, typeof(GameObject),
                            true, GUILayout.Width(width)) as GameObject;
                    InventoryItemList.InventoryItems[i].subtitle =
                        EditorGUILayout.TextField(InventoryItemList.InventoryItems[i].subtitle, GUILayout.Width(width));
                    InventoryItemList.InventoryItems[i].audioClip =
                        EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].audioClip, typeof(AudioClip),
                            GUILayout.Width(width / 1.5f)) as AudioClip;
                    InventoryItemList.InventoryItems[i].backLighting =
                        EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].backLighting,
                            typeof(GameObject),
                            true, GUILayout.Width(width)) as GameObject;
                    InventoryItemList.InventoryItems[i].isChilds =
                        EditorGUILayout.Toggle(InventoryItemList.InventoryItems[i].isChilds,
                            GUILayout.Width(width / 2));
                    InventoryItemList.InventoryItems[i].isPosition =
                        EditorGUILayout.Toggle(InventoryItemList.InventoryItems[i].isPosition,
                            GUILayout.Width(width / 2));
//                InventoryItemList.InventoryItems[i].defect1 =
//                    EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].defect1, typeof(GameObject), true,
//                        GUILayout.Width(width)) as GameObject;
//                InventoryItemList.InventoryItems[i].defect2 =
//                    EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].defect2, typeof(GameObject), true,
//                        GUILayout.Width(width)) as GameObject;
//                InventoryItemList.InventoryItems[i].defect3 =
//                    EditorGUILayout.ObjectField(InventoryItemList.InventoryItems[i].defect3, typeof(GameObject), true,
//                        GUILayout.Width(width)) as GameObject;
                    GUILayout.EndHorizontal();
                }
            }

//        foreach (var VARIABLE in InventoryItemList.InventoryItems)
//        {
//            if (VARIABLE.backLighting == null || VARIABLE.gameObject)
//            {
//                continue;
//            }
//            if (VARIABLE.backLighting.GetHashCode().ToString() != "0")
//                VARIABLE.HashBackLighting = VARIABLE.backLighting.GetComponent<Transform>().GetHashCode().ToString();
//            if (VARIABLE.gameObject.GetHashCode().ToString() != "0")
//                VARIABLE.HashGameObject = VARIABLE.gameObject.GetComponent<Transform>().GetHashCode().ToString();
//        }

            //Кнопка сохранения данных в главный контроллер
            if (GUILayout.Button("Save", GUILayout.ExpandWidth(false)))
            {
                GameController.GetComponent<Elements>().ListLook = new List<Transform>();
                GameController.GetComponent<Elements>().ListTarget = new List<Transform>();
                GameController.GetComponent<Elements>().ListSubtitle = new List<string>();
                GameController.GetComponent<Elements>().ListAudiclips = new List<AudioClip>();
                GameController.GetComponent<Elements>().IsChild = new List<bool>();
                GameController.GetComponent<Elements>().ListHightlightObjects = new List<GameObject>();
                GameController.GetComponent<Elements>().IsPosition = new List<bool>();

                for (int i = 0; i < InventoryItemList.InventoryItems.Count; i++)
                {
                    GameController.GetComponent<Elements>()
                        .ListLook.Add(InventoryItemList.InventoryItems[i].gameObject.transform);
                    GameController.GetComponent<Elements>()
                        .ListTarget.Add(InventoryItemList.InventoryItems[i].gameObject.transform.GetChild(0));
                    GameController.GetComponent<Elements>()
                        .ListSubtitle.Add(InventoryItemList.InventoryItems[i].subtitle);
                    GameController.GetComponent<Elements>()
                        .ListAudiclips.Add(InventoryItemList.InventoryItems[i].audioClip);
                    GameController.GetComponent<Elements>().IsChild.Add(InventoryItemList.InventoryItems[i].isChilds);
                    GameController.GetComponent<Elements>()
                        .IsPosition.Add(InventoryItemList.InventoryItems[i].isPosition);
                    GameController.GetComponent<Elements>()
                        .ListHightlightObjects.Add(InventoryItemList.InventoryItems[i].backLighting);
                }
                PrefabUtility.ReplacePrefab(GameController, PrefabUtility.GetPrefabParent(GameController),
                    ReplacePrefabOptions.ConnectToPrefab);
            }

            //Кнопка перезаписи
            if (GUILayout.Button("Reload", GUILayout.ExpandWidth(false)))
            {
                OnEnable();
            }
            GUILayout.BeginHorizontal();

            if (InventoryItemList != null)
            {
                //Кнопка отображения данных
                if (GUILayout.Button("Show Item List", GUILayout.ExpandWidth(false)))
                {
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = InventoryItemList;
                }
            }

            // Открыть файл с данными
            if (GUILayout.Button("Open Item List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }

            //Создать новый файл с данными
            if (GUILayout.Button("New Item List", GUILayout.ExpandWidth(false)))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = InventoryItemList;
            }
            GUILayout.EndHorizontal();

            //Добавить элемент
            if (GUILayout.Button("Add Item", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }

            GUILayout.BeginHorizontal();
            // Удаление элемента из списка
            if (GUILayout.Button("Delete Item", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(numberRemove);
            }

           
            GUILayout.Label("Number line", EditorStyles.boldLabel, GUILayout.Width(width));
            numberRemove = EditorGUILayout.IntField(numberRemove, GUILayout.Width(width));
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            //Создать строку
            if (GUILayout.Button("Add line under number", GUILayout.ExpandWidth(false)))
            {
                InventoryItemList.InventoryItems.Insert(numberAdd, new InventoryItem());
            }
            GUILayout.Label("Number line", EditorStyles.boldLabel, GUILayout.Width(width));
            numberAdd = EditorGUILayout.IntField(numberAdd, GUILayout.Width(width));
            GUILayout.EndHorizontal();

            GUILayout.EndScrollView();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(InventoryItemList);
            }
        }

        /// <summary>
        /// Добавлкение строки в список данных
        /// </summary>
        void AddItem()
        {
            InventoryItemList.InventoryItems.Add(new InventoryItem());
        }

        /// <summary>
        /// Открытие файла с данными
        /// </summary>
        void OpenItemList()
        {
            string absPath = EditorUtility.OpenFilePanel("Select Inventory Item List", "", "");
            if (absPath.StartsWith(Application.dataPath))
            {
                string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
                InventoryItemList =
                    AssetDatabase.LoadAssetAtPath(relPath, typeof(InventoryItemList)) as InventoryItemList;
                if (InventoryItemList.InventoryItems == null)
                    InventoryItemList.InventoryItems = new List<InventoryItem>();
                if (InventoryItemList)
                {
                    EditorPrefs.SetString("ObjectPath", relPath);
                }
            }
        }

        /// <summary>
        /// Удаление строки по индексу
        /// </summary>
        /// <param name="index">омер строки</param>
        void DeleteItem(int index)
        {
            InventoryItemList.InventoryItems.RemoveAt(index);
        }
    }
}