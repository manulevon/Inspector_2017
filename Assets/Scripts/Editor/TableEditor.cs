﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace Studying
{
    /// <summary>
    /// Класс реализующий customInspector
    /// </summary>
    [CustomEditor(typeof(Table))]
    public class TableEditor : Editor
    {
        // Длина ячейки
        private float _width = 120;
        // Номер добавления строки
        private int _numberAdd = 0;
        // Номер удаления строки
        private int _numberRemove = 0;
        // Данные
        private Table myTarget;
        //Список точек куда необходимо смотреть объекту
        private SerializedProperty ListLook;
        //Список субтитров
        private SerializedProperty ListSubtitle;
        //Список озвучки
        private SerializedProperty ListAudiclips;
        //Список булевых значений автоплея
        private SerializedProperty ListIsPlayThenEmpty;
        //Список компонентов обводки
        private SerializedProperty ListHightlightObjects;
        // Список булевых тригеров - необходимость использования дочерних объектов
        private SerializedProperty ListIsChild;
        //Список позиций осмотра элементо
        private SerializedProperty ListIsPosition;
        //Список активных объектов 
        private SerializedProperty ListActionObjects;
        //Список позиций куда необходимо переместиться игроку-осмотрщику
        private SerializedProperty ListTargetWalk;
        // Список названий аимаций покоя  
        private SerializedProperty ListNameAnimationIdle;
        //Список названий анимаций состояния ходьбы
        private SerializedProperty ListNameAnimationWalk;
        // Список кнопок
        private SerializedProperty ListButtonObject;
        //Список зон операций
        private SerializedProperty ListZoneOperations;
        //Список названий анимаций
        private SerializedProperty ListNameAnimation;
        //Список дефектов
        private SerializedProperty ListDefects;
        //Список озвучки дефектов
        private SerializedProperty ListDefectAudioClips;
        //Список булевых значений, отвечающих за автоплей
        private SerializedProperty ListDefectIsPlayThenEmpty;
        // Список субтитров для дефектов
        private SerializedProperty ListDefectSubtitles;
        // Список булевых значений - необходимость использования дочерних объектов
        private SerializedProperty ListDefectIsChilds;

        /// <summary>
        /// Метод выполняется при включение скрипта
        /// </summary>
        void OnEnable()
        {
            myTarget = (Table) target;
            // Поиск полей в скрипте унаследованого от Monobehavior
            ListLook = serializedObject.FindProperty("ListLook");
            ListSubtitle = serializedObject.FindProperty("ListSubtitle");
            ListAudiclips = serializedObject.FindProperty("ListAudiclips");
            ListIsPlayThenEmpty = serializedObject.FindProperty("ListIsPlayThenEmpty");
            ListHightlightObjects = serializedObject.FindProperty("ListHightlightObjects");
            ListIsChild = serializedObject.FindProperty("ListIsChild");
            ListIsPosition = serializedObject.FindProperty("ListIsPosition");
            ListActionObjects = serializedObject.FindProperty("ListActionObjects");
            ListTargetWalk = serializedObject.FindProperty("ListTargetWalk");
            ListNameAnimationIdle = serializedObject.FindProperty("ListNameAnimationIdle");
            ListNameAnimationWalk = serializedObject.FindProperty("ListNameAnimationWalk");
            ListButtonObject = serializedObject.FindProperty("ListButtonObject");
            ListZoneOperations = serializedObject.FindProperty("ListZoneOperations");
            ListNameAnimation = serializedObject.FindProperty("ListNameAnimation");
            ListDefects = serializedObject.FindProperty("ListDefects");
            ListDefectAudioClips = serializedObject.FindProperty("ListDefectAudioClips");
            ListDefectIsPlayThenEmpty = serializedObject.FindProperty("ListDefectIsPlayThenEmpty");
            ListDefectSubtitles = serializedObject.FindProperty("ListDefectSubtitles");
            ListDefectIsChilds = serializedObject.FindProperty("ListDefectIsChilds");
        }

        /// <summary>
        /// Выполняется каждый кадр
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

//            myTarget.GameController = GameObject.Find("GameController").GetComponent<GameController>();

            GUILayout.Space(15);

            GUILayout.BeginHorizontal();
            //Кнопка добавления строки в определенной место
            if (GUILayout.Button("Add line in", GUILayout.ExpandWidth(false)))
            {
                if (myTarget.ListLook.Count - 1 >= _numberAdd && myTarget.ListLook.Count >= 0)
                {
                    int i = _numberAdd;
                    ListLook.InsertArrayElementAtIndex(i);
                    ListSubtitle.InsertArrayElementAtIndex(i);
                    ListAudiclips.InsertArrayElementAtIndex(i);
                    ListIsPlayThenEmpty.InsertArrayElementAtIndex(i);
                    ListHightlightObjects.InsertArrayElementAtIndex(i);
                    ListIsChild.InsertArrayElementAtIndex(i);
                    ListIsPosition.InsertArrayElementAtIndex(i);
                    ListActionObjects.InsertArrayElementAtIndex(i);
                    ListTargetWalk.InsertArrayElementAtIndex(i);
                    ListNameAnimationIdle.InsertArrayElementAtIndex(i);
                    ListNameAnimationWalk.InsertArrayElementAtIndex(i);
                    ListButtonObject.InsertArrayElementAtIndex(i);
                    ListZoneOperations.InsertArrayElementAtIndex(i);
                    ListNameAnimation.InsertArrayElementAtIndex(i);
                    ListDefects.InsertArrayElementAtIndex(i);
                    ListDefectAudioClips.InsertArrayElementAtIndex(i);
                    ListDefectIsPlayThenEmpty.InsertArrayElementAtIndex(i);
                    ListDefectSubtitles.InsertArrayElementAtIndex(i);
                    ListDefectIsChilds.InsertArrayElementAtIndex(i);

                    ListLook.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListSubtitle.GetArrayElementAtIndex(i).stringValue = null;
                    ListAudiclips.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = false;
                    ListHightlightObjects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListIsChild.GetArrayElementAtIndex(i).boolValue = false;
                    ListIsPosition.GetArrayElementAtIndex(i).boolValue = false;
                    ListActionObjects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListTargetWalk.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListNameAnimationIdle.GetArrayElementAtIndex(i).stringValue = null;
                    ListNameAnimationWalk.GetArrayElementAtIndex(i).stringValue = null;
                    ListButtonObject.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListZoneOperations.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListNameAnimation.GetArrayElementAtIndex(i).stringValue = null;
                    ListDefects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListDefectAudioClips.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    ListDefectIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = false;
                    ListDefectSubtitles.GetArrayElementAtIndex(i).stringValue = null;
                    ListDefectIsChilds.GetArrayElementAtIndex(i).boolValue = false;
                }
            }
            GUILayout.Label("number", EditorStyles.boldLabel, GUILayout.ExpandWidth(false));
            _numberAdd = EditorGUILayout.IntField(_numberAdd, GUILayout.Width(_width / 3));
            GUILayout.EndHorizontal();

            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            //Кнопка удаления строки
            if (GUILayout.Button("Remove line in", GUILayout.ExpandWidth(false)))
            {
                if (_numberRemove >= 0)
                    if (myTarget.ListLook.Count - 1 >= _numberRemove)
                    {
                        int i = _numberRemove;
//                        ListLook.DeleteArrayElementAtIndex(i);
//                        ListSubtitle.DeleteArrayElementAtIndex(i);
//                        ListAudiclips.DeleteArrayElementAtIndex(i);
//                        ListIsPlayThenEmpty.DeleteArrayElementAtIndex(i);
//                        ListHightlightObjects.DeleteArrayElementAtIndex(i);
//                        ListIsChild.DeleteArrayElementAtIndex(i);
//                        ListIsPosition.DeleteArrayElementAtIndex(i);
//                        ListActionObjects.DeleteArrayElementAtIndex(i);
//                        ListTargetWalk.DeleteArrayElementAtIndex(i);
//                        ListNameAnimationIdle.DeleteArrayElementAtIndex(i);
//                        ListNameAnimationWalk.DeleteArrayElementAtIndex(i);
//                        ListButtonObject.DeleteArrayElementAtIndex(i);
//                        ListZoneOperations.DeleteArrayElementAtIndex(i);
//                        ListNameAnimation.DeleteArrayElementAtIndex(i);
//                        ListDefects.DeleteArrayElementAtIndex(i);
//                        ListDefectAudioClips.DeleteArrayElementAtIndex(i);
//                        ListDefectIsPlayThenEmpty.DeleteArrayElementAtIndex(i);
//                        ListDefectSubtitles.DeleteArrayElementAtIndex(i);
//                        ListDefectIsChilds.DeleteArrayElementAtIndex(i);
                        myTarget.ListLook.RemoveAt(i);
                        myTarget.ListSubtitle.RemoveAt(i);
                        myTarget.ListAudiclips.RemoveAt(i);
                        myTarget.ListIsPlayThenEmpty.RemoveAt(i);
                        myTarget.ListHightlightObjects.RemoveAt(i);
                        myTarget.ListIsChild.RemoveAt(i);
                        myTarget.ListIsPosition.RemoveAt(i);
                        myTarget.ListActionObjects.RemoveAt(i);
                        myTarget.ListTargetWalk.RemoveAt(i);
                        myTarget.ListNameAnimationIdle.RemoveAt(i);
                        myTarget.ListNameAnimationWalk.RemoveAt(i);
                        myTarget.ListButtonObject.RemoveAt(i);
                        myTarget.ListZoneOperations.RemoveAt(i);
                        myTarget.ListNameAnimation.RemoveAt(i);
                        myTarget.ListDefects.RemoveAt(i);
                        myTarget.ListDefectAudioClips.RemoveAt(i);
                        myTarget.ListDefectIsPlayThenEmpty.RemoveAt(i);
                        myTarget.ListDefectSubtitles.RemoveAt(i);
                        myTarget.ListDefectIsChilds.RemoveAt(i);
                    }
            }

            GUILayout.Label("number", EditorStyles.boldLabel, GUILayout.ExpandWidth(false));
            _numberRemove = EditorGUILayout.IntField(_numberRemove, GUILayout.Width(_width / 3));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width/3));
            GUILayout.Label("LookTransform", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
            GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("HightlightObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("IsChilds", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width/2));

            GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimWalk", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("ButtonObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimOperation", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("DefectObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
            GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("DefSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("DefIsChilds", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.EndHorizontal();

            for (int i = 0; i < myTarget.ListLook.Count; i++)
            {
//                GUILayout.BeginHorizontal();
//                EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(_width/3));
//
//                ListLook.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListLook[i], typeof(Transform), true,
//                    GUILayout.Width(_width)) as Transform;
//
//                ListSubtitle.GetArrayElementAtIndex(i).stringValue = EditorGUILayout.TextField(
//                    myTarget.ListSubtitle[i], GUILayout.Width(_width));
//
//                ListAudiclips.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListAudiclips[i], typeof(AudioClip),true,
//                    GUILayout.Width(_width/1.5f)) as AudioClip;
//
//                ListIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(
//                    myTarget.ListIsPlayThenEmpty[i], GUILayout.Width(_width/2));
//
//                ListHightlightObjects.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListHightlightObjects[i],typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                ListIsChild.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(
//                    myTarget.ListIsChild[i], GUILayout.Width(_width/2));
//
//                ListIsPosition.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(
//                    myTarget.ListIsPosition[i],GUILayout.Width(_width/2));
//
//
//                ListActionObjects.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListActionObjects[i],typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                ListTargetWalk.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListTargetWalk[i], typeof(Transform),true,
//                    GUILayout.Width(_width)) as Transform;
//
//                ListNameAnimationIdle.GetArrayElementAtIndex(i).stringValue = EditorGUILayout.TextField(
//                    myTarget.ListNameAnimationIdle[i],GUILayout.Width(_width));
//
//                ListNameAnimationWalk.GetArrayElementAtIndex(i).stringValue = EditorGUILayout.TextField(
//                    myTarget.ListNameAnimationWalk[i],GUILayout.Width(_width));
//
//
//                ListButtonObject.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListButtonObject[i],typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                ListZoneOperations.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListZoneOperations[i],typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                ListNameAnimation.GetArrayElementAtIndex(i).stringValue = EditorGUILayout.TextField(
//                    myTarget.ListNameAnimation[i], GUILayout.Width(_width));
//
//
//                ListDefects.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListDefects[i], typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                ListDefectAudioClips.GetArrayElementAtIndex(i).objectReferenceValue = EditorGUILayout.ObjectField(
//                    myTarget.ListDefectAudioClips[i],typeof(AudioClip), true,
//                    GUILayout.Width(_width/1.5f)) as AudioClip;
//
//                ListDefectIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(
//                    myTarget.ListDefectIsPlayThenEmpty[i], GUILayout.Width(_width/2));
//
//                ListDefectSubtitles.GetArrayElementAtIndex(i).stringValue = EditorGUILayout.TextField(
//                    myTarget.ListDefectSubtitles[i], GUILayout.Width(_width));
//
//                ListDefectIsChilds.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(
//                    myTarget.ListDefectIsChilds[i], GUILayout.Width(_width/2));
//
//                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                //Отображение полей в окне инспектора
                EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(_width/3));

                myTarget.ListLook[i] = EditorGUILayout.ObjectField(myTarget.ListLook[i], typeof(Transform), true,
                    GUILayout.Width(_width)) as Transform;


                myTarget.ListSubtitle[i] = EditorGUILayout.TextField(myTarget.ListSubtitle[i], GUILayout.Width(_width));

                myTarget.ListAudiclips[i] = EditorGUILayout.ObjectField(myTarget.ListAudiclips[i], typeof(AudioClip),
                    true, GUILayout.Width(_width/1.5f)) as AudioClip;

                myTarget.ListIsPlayThenEmpty[i] = EditorGUILayout.Toggle(myTarget.ListIsPlayThenEmpty[i],
                    GUILayout.Width(_width/2));

                myTarget.ListHightlightObjects[i] = EditorGUILayout.ObjectField(myTarget.ListHightlightObjects[i],
                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;

                myTarget.ListIsChild[i] = EditorGUILayout.Toggle(myTarget.ListIsChild[i], GUILayout.Width(_width/2));

                myTarget.ListIsPosition[i] = EditorGUILayout.Toggle(myTarget.ListIsPosition[i],
                    GUILayout.Width(_width/2));


                myTarget.ListActionObjects[i] = EditorGUILayout.ObjectField(myTarget.ListActionObjects[i],
                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;

                myTarget.ListTargetWalk[i] = EditorGUILayout.ObjectField(myTarget.ListTargetWalk[i], typeof(Transform),
                    true, GUILayout.Width(_width)) as Transform;

                myTarget.ListNameAnimationIdle[i] = EditorGUILayout.TextField(myTarget.ListNameAnimationIdle[i],
                    GUILayout.Width(_width));

                myTarget.ListNameAnimationWalk[i] = EditorGUILayout.TextField(myTarget.ListNameAnimationWalk[i],
                    GUILayout.Width(_width));


                myTarget.ListButtonObject[i] = EditorGUILayout.ObjectField(myTarget.ListButtonObject[i],
                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;

                myTarget.ListZoneOperations[i] = EditorGUILayout.ObjectField(myTarget.ListZoneOperations[i],
                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;

                myTarget.ListNameAnimation[i] = EditorGUILayout.TextField(myTarget.ListNameAnimation[i],
                    GUILayout.Width(_width));


                myTarget.ListDefects[i] = EditorGUILayout.ObjectField(myTarget.ListDefects[i], typeof(GameObject), true,
                    GUILayout.Width(_width)) as GameObject;

                myTarget.ListDefectAudioClips[i] = EditorGUILayout.ObjectField(myTarget.ListDefectAudioClips[i],
                    typeof(AudioClip), true, GUILayout.Width(_width/1.5f)) as AudioClip;

                myTarget.ListDefectIsPlayThenEmpty[i] = EditorGUILayout.Toggle(myTarget.ListDefectIsPlayThenEmpty[i],
                    GUILayout.Width(_width/2));

                myTarget.ListDefectSubtitles[i] = EditorGUILayout.TextField(myTarget.ListDefectSubtitles[i],
                    GUILayout.Width(_width));

                myTarget.ListDefectIsChilds[i] = EditorGUILayout.Toggle(myTarget.ListDefectIsChilds[i],
                    GUILayout.Width(_width/2));

                GUILayout.EndHorizontal();
                GUILayout.Space(5);
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width/3));
            GUILayout.Label("LookTransform", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
            GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("HightlightObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("IsChilds", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width/2));

            GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimWalk", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("ButtonObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("NameAnimOperation", EditorStyles.boldLabel, GUILayout.Width(_width));

            GUILayout.Label("DefectObj", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
            GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.Label("DefSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
            GUILayout.Label("DefIsChilds", EditorStyles.boldLabel, GUILayout.Width(_width/2));
            GUILayout.EndHorizontal();
//            GUILayout.BeginHorizontal();
//            GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width/3));
//            GUILayout.Label("LookTransform", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("HightlightObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsChilds", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("NameAnimIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.EndHorizontal();
//
//            GUILayout.BeginHorizontal();
//            GUILayout.Label("", EditorStyles.boldLabel, GUILayout.Width(_width/3));
//            GUILayout.Label("NameAnimWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("ButtonObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("NameAnimOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("DefectObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefIsChilds", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.EndHorizontal();
//
//            for (int i = 0; i < myTarget.ListLook.Count; i++)
//            {
//                GUILayout.BeginHorizontal();
//                EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(_width/3));
//
//                myTarget.ListLook[i] = EditorGUILayout.ObjectField(myTarget.ListLook[i], typeof(Transform), true,
//                    GUILayout.Width(_width)) as Transform;
//
//
//                myTarget.ListSubtitle[i] = EditorGUILayout.TextField(myTarget.ListSubtitle[i], GUILayout.Width(_width));
//
//                myTarget.ListAudiclips[i] = EditorGUILayout.ObjectField(myTarget.ListAudiclips[i], typeof(AudioClip),
//                    true, GUILayout.Width(_width)) as AudioClip;
//
//                myTarget.ListIsPlayThenEmpty[i] = EditorGUILayout.Toggle(myTarget.ListIsPlayThenEmpty[i],
//                    GUILayout.Width(_width));
//
//                myTarget.ListHightlightObjects[i] = EditorGUILayout.ObjectField(myTarget.ListHightlightObjects[i],
//                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;
//
//                myTarget.ListIsChild[i] = EditorGUILayout.Toggle(myTarget.ListIsChild[i], GUILayout.Width(_width));
//
//                myTarget.ListIsPosition[i] = EditorGUILayout.Toggle(myTarget.ListIsPosition[i],
//                    GUILayout.Width(_width));
//
//
//                myTarget.ListActionObjects[i] = EditorGUILayout.ObjectField(myTarget.ListActionObjects[i],
//                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;
//
//                myTarget.ListTargetWalk[i] = EditorGUILayout.ObjectField(myTarget.ListTargetWalk[i], typeof(Transform),
//                    true, GUILayout.Width(_width)) as Transform;
//
//                myTarget.ListNameAnimationIdle[i] = EditorGUILayout.TextField(myTarget.ListNameAnimationIdle[i],
//                    GUILayout.Width(_width));
//                GUILayout.EndHorizontal();
//
//
//                GUILayout.BeginHorizontal();
//                GUILayout.Label("", EditorStyles.boldLabel, GUILayout.Width(_width/3));
//                myTarget.ListNameAnimationWalk[i] = EditorGUILayout.TextField(myTarget.ListNameAnimationWalk[i],
//                    GUILayout.Width(_width));
//
//
//                myTarget.ListButtonObject[i] = EditorGUILayout.ObjectField(myTarget.ListButtonObject[i],
//                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;
//
//                myTarget.ListZoneOperations[i] = EditorGUILayout.ObjectField(myTarget.ListZoneOperations[i],
//                    typeof(GameObject), true, GUILayout.Width(_width)) as GameObject;
//
//                myTarget.ListNameAnimation[i] = EditorGUILayout.TextField(myTarget.ListNameAnimation[i],
//                    GUILayout.Width(_width));
//
//
//                myTarget.ListDefects[i] = EditorGUILayout.ObjectField(myTarget.ListDefects[i], typeof(GameObject), true,
//                    GUILayout.Width(_width)) as GameObject;
//
//                myTarget.ListDefectAudioClips[i] = EditorGUILayout.ObjectField(myTarget.ListDefectAudioClips[i],
//                    typeof(AudioClip), true, GUILayout.Width(_width)) as AudioClip;
//
//                myTarget.ListDefectIsPlayThenEmpty[i] = EditorGUILayout.Toggle(myTarget.ListDefectIsPlayThenEmpty[i],
//                    GUILayout.Width(_width));
//
//                myTarget.ListDefectSubtitles[i] = EditorGUILayout.TextField(myTarget.ListDefectSubtitles[i],
//                    GUILayout.Width(_width));
//
//                myTarget.ListDefectIsChilds[i] = EditorGUILayout.Toggle(myTarget.ListDefectIsChilds[i],
//                    GUILayout.Width(_width));
//
//                GUILayout.EndHorizontal();
//                GUILayout.Space(10);
//            }
//
//            GUILayout.BeginHorizontal();
//            GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width/3));
//            GUILayout.Label("LookTransform", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("HightlightObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsChilds", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("NameAnimIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.EndHorizontal();
//
//            GUILayout.BeginHorizontal();
//            GUILayout.Label("", EditorStyles.boldLabel, GUILayout.Width(_width/3));
//            GUILayout.Label("NameAnimWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("ButtonObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("NameAnimOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
//
//            GUILayout.Label("DefectObj", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.Label("DefIsChilds", EditorStyles.boldLabel, GUILayout.Width(_width));
//            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            //Кнопка добавления строки
            if (GUILayout.Button("Add line", GUILayout.Height(25), GUILayout.ExpandWidth(false)))
            {
                int i = myTarget.ListLook.Count;
                ListLook.InsertArrayElementAtIndex(i);
                ListSubtitle.InsertArrayElementAtIndex(i);
                ListAudiclips.InsertArrayElementAtIndex(i);
                ListIsPlayThenEmpty.InsertArrayElementAtIndex(i);
                ListHightlightObjects.InsertArrayElementAtIndex(i);
                ListIsChild.InsertArrayElementAtIndex(i);
                ListIsPosition.InsertArrayElementAtIndex(i);
                ListActionObjects.InsertArrayElementAtIndex(i);
                ListTargetWalk.InsertArrayElementAtIndex(i);
                ListNameAnimationIdle.InsertArrayElementAtIndex(i);
                ListNameAnimationWalk.InsertArrayElementAtIndex(i);
                ListButtonObject.InsertArrayElementAtIndex(i);
                ListZoneOperations.InsertArrayElementAtIndex(i);
                ListNameAnimation.InsertArrayElementAtIndex(i);
                ListDefects.InsertArrayElementAtIndex(i);
                ListDefectAudioClips.InsertArrayElementAtIndex(i);
                ListDefectIsPlayThenEmpty.InsertArrayElementAtIndex(i);
                ListDefectSubtitles.InsertArrayElementAtIndex(i);
                ListDefectIsChilds.InsertArrayElementAtIndex(i);

                ListLook.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListSubtitle.GetArrayElementAtIndex(i).stringValue = null;
                ListAudiclips.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = false;
                ListHightlightObjects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListIsChild.GetArrayElementAtIndex(i).boolValue = false;
                ListIsPosition.GetArrayElementAtIndex(i).boolValue = false;
                ListActionObjects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListTargetWalk.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListNameAnimationIdle.GetArrayElementAtIndex(i).stringValue = null;
                ListNameAnimationWalk.GetArrayElementAtIndex(i).stringValue = null;
                ListButtonObject.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListZoneOperations.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListNameAnimation.GetArrayElementAtIndex(i).stringValue = null;
                ListDefects.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListDefectAudioClips.GetArrayElementAtIndex(i).objectReferenceValue = null;
                ListDefectIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = false;
                ListDefectSubtitles.GetArrayElementAtIndex(i).stringValue = null;
                ListDefectIsChilds.GetArrayElementAtIndex(i).boolValue = false;
            }

            GUILayout.Space(100);

            //удаление последней строки
            if (GUILayout.Button("Delete last line", GUILayout.Height(25), GUILayout.ExpandWidth(false)))
            {
                int i = myTarget.ListLook.Count-1;
                ListLook.DeleteArrayElementAtIndex(i);
                ListLook.arraySize = i;
                ListSubtitle.DeleteArrayElementAtIndex(i);
                ListAudiclips.DeleteArrayElementAtIndex(i);
                ListIsPlayThenEmpty.DeleteArrayElementAtIndex(i);
                ListHightlightObjects.DeleteArrayElementAtIndex(i);
                ListIsChild.DeleteArrayElementAtIndex(i);
                ListIsPosition.DeleteArrayElementAtIndex(i);
                ListActionObjects.DeleteArrayElementAtIndex(i);
                ListTargetWalk.DeleteArrayElementAtIndex(i);
                ListNameAnimationIdle.DeleteArrayElementAtIndex(i);
                ListNameAnimationWalk.DeleteArrayElementAtIndex(i);
                ListButtonObject.DeleteArrayElementAtIndex(i);
                ListZoneOperations.DeleteArrayElementAtIndex(i);
                ListNameAnimation.DeleteArrayElementAtIndex(i);
                ListDefects.DeleteArrayElementAtIndex(i);
                ListDefectAudioClips.DeleteArrayElementAtIndex(i);
                ListDefectIsPlayThenEmpty.DeleteArrayElementAtIndex(i);
                ListDefectSubtitles.DeleteArrayElementAtIndex(i);
                ListDefectIsChilds.DeleteArrayElementAtIndex(i);
            }
            GUILayout.EndHorizontal();

            #region Download
            // Загрузка данных из главного контроллера
            if (GUILayout.Button("Download", GUILayout.ExpandWidth(false)))
            {
                myTarget.ListLook = new List<Transform>();
                myTarget.ListSubtitle = new List<string>();
                myTarget.ListAudiclips = new List<AudioClip>();
                myTarget.ListIsPlayThenEmpty = new List<bool>();
                myTarget.ListHightlightObjects = new List<GameObject>();
                myTarget.ListIsChild = new List<bool>();
                myTarget.ListIsPosition = new List<bool>();
                myTarget.ListActionObjects = new List<GameObject>();
                myTarget.ListTargetWalk = new List<Transform>();
                myTarget.ListNameAnimationIdle = new List<string>();
                myTarget.ListNameAnimationWalk = new List<string>();
                myTarget.ListButtonObject = new List<GameObject>();
                myTarget.ListZoneOperations = new List<GameObject>();
                myTarget.ListNameAnimation = new List<string>();
                myTarget.ListDefects = new List<GameObject>();
                myTarget.ListDefectAudioClips = new List<AudioClip>();
                myTarget.ListDefectIsPlayThenEmpty = new List<bool>();
                myTarget.ListDefectSubtitles = new List<string>();
                myTarget.ListDefectIsChilds = new List<bool>();

                myTarget.GameController = FindObjectOfType<GameController>().GetComponent<GameController>();
                for (int i = 0; i < myTarget.GameController.ListLook.Count; i++)
                {
                    myTarget.ListLook.Add(myTarget.GameController.ListLook[i]);
                    myTarget.ListSubtitle.Add(myTarget.GameController.ListSubtitle[i]);
                    myTarget.ListAudiclips.Add(myTarget.GameController.ListAudiclips[i]);
                    myTarget.ListIsPlayThenEmpty.Add(myTarget.GameController.ListIsPlayThenEmpty[i]);
                    myTarget.ListHightlightObjects.Add(myTarget.GameController.ListHightlightObjects[i]);
                    myTarget.ListIsChild.Add(myTarget.GameController.ListIsChild[i]);
                    myTarget.ListIsPosition.Add(myTarget.GameController.ListIsPosition[i]);
                    myTarget.ListActionObjects.Add(myTarget.GameController.ListActionObjects[i]);
                    myTarget.ListTargetWalk.Add(myTarget.GameController.ListTargetWalk[i]);
                    myTarget.ListNameAnimationIdle.Add(myTarget.GameController.ListNameAnimationIdle[i]);
                    myTarget.ListNameAnimationWalk.Add(myTarget.GameController.ListNameAnimationWalk[i]);
                    myTarget.ListButtonObject.Add(myTarget.GameController.ListButtonObject[i]);
                    myTarget.ListZoneOperations.Add(myTarget.GameController.ListZoneOperations[i]);
                    myTarget.ListNameAnimation.Add(myTarget.GameController.ListNameAnimation[i]);
                    myTarget.ListDefects.Add(myTarget.GameController.ListDefects[i]);
                    myTarget.ListDefectAudioClips.Add(myTarget.GameController.ListDefectAudioClips[i]);
                    myTarget.ListDefectIsPlayThenEmpty.Add(myTarget.GameController.ListDefectIsPlayThenEmpty[i]);
                    myTarget.ListDefectSubtitles.Add(myTarget.GameController.ListDefectSubtitles[i]);
                    myTarget.ListDefectIsChilds.Add(myTarget.GameController.ListDefectIsChilds[i]);
                }
            }

            #endregion

//            if (GUI.changed)
//            {
//                Debug.Log(11);
//                for (int i = 0; i < myTarget.ListLook.Count; i++)
//                {
//                    ListLook.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListLook[i];
//
//                    ListSubtitle.GetArrayElementAtIndex(i).stringValue = myTarget.ListSubtitle[i];
//
//                    ListAudiclips.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListAudiclips[i];
//
//                    ListIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = myTarget.ListIsPlayThenEmpty[i];
//
//                    ListHightlightObjects.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListHightlightObjects[i];
//
//                    ListIsChild.GetArrayElementAtIndex(i).boolValue = myTarget.ListIsChild[i];
//
//                    ListIsPosition.GetArrayElementAtIndex(i).boolValue = myTarget.ListIsPosition[i];
//
//
//                    ListActionObjects.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListActionObjects[i];
//
//                    ListTargetWalk.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListTargetWalk[i];
//
//                    ListNameAnimationIdle.GetArrayElementAtIndex(i).stringValue = myTarget.ListNameAnimationIdle[i];
//
//                    ListNameAnimationWalk.GetArrayElementAtIndex(i).stringValue = myTarget.ListNameAnimationWalk[i];
//
//
//                    ListButtonObject.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListButtonObject[i];
//
//                    ListZoneOperations.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListZoneOperations[i];
//
//                    ListNameAnimation.GetArrayElementAtIndex(i).stringValue = myTarget.ListNameAnimation[i];
//
//
//                    ListDefects.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListDefects[i];
//
//                    ListDefectAudioClips.GetArrayElementAtIndex(i).objectReferenceValue = myTarget.ListDefectAudioClips[i];
//
//                    ListDefectIsPlayThenEmpty.GetArrayElementAtIndex(i).boolValue = myTarget.ListDefectIsPlayThenEmpty[i];
//
//                    ListDefectSubtitles.GetArrayElementAtIndex(i).stringValue = myTarget.ListDefectSubtitles[i];
//
//                    ListDefectIsChilds.GetArrayElementAtIndex(i).boolValue = myTarget.ListDefectIsChilds[i];
//                }
//            }

            // Принять изменения в инспекторе
            serializedObject.ApplyModifiedProperties();
        }


    }
}