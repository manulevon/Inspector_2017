﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using UnityEditor;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace newLogic
{
    /// <summary>
    /// Создание файла с дынными позиций осмотра
    /// </summary>
    public class CreateInventoryItemList
    {
        /// <summary>
        /// Создаёт файл и сохроняет его в папке проекта "Assets/Data/InventoryItemList.asset"
        /// </summary>
        /// <returns>Возвращает файл</returns>
        [MenuItem("Assets/Create/Inventory Item List")]
        public static InventoryItemList Create()
        {

            InventoryItemList asset = ScriptableObject.CreateInstance<InventoryItemList>();

            AssetDatabase.CreateAsset(asset, "Assets/Data/InventoryItemList.asset");
            AssetDatabase.SaveAssets();
            return asset;
        }
    }
}