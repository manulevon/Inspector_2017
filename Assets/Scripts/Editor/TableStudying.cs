﻿//-----------------------------------------------------------------------------
//
//      Расширение Editor Unity
//      (с) РГУПС, ОИТП ЦРИК 21/07/2017
//      Разработал: Шевченко А.А.
//
//-----------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Набор классов для для расширения Editor Unity
/// </summary>
namespace Studying
{
    /// <summary>
    /// Окно сохронения данных
    /// </summary>
    public class SaveClass : EditorWindow
    {
        /// <summary>
        /// Ссылка на окно TableStudying
        /// </summary>
        public TableStudying TableStudying;

        /// <summary>
        /// Метод отрабатывает каждый кадр
        /// </summary>
        void OnGUI()
        {
            // Бокс с текстовой информацией
            EditorGUILayout.HelpBox("Are you sure you want to save the changes?", MessageType.Info);
            Repaint();
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            // Кнопка Да
            if (GUILayout.Button("Yes"))
            {
                TableStudying.Save();
                this.Close();
            }
            // Кнопка Нет
            if (GUILayout.Button("No"))
            {
                this.Close();
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Окно загрузки данных
    /// </summary>
    public class DownloadClass : EditorWindow
    {
        /// <summary>
        /// Ссылка на окно TableStudying
        /// </summary>
        public TableStudying TableStudying;
        /// <summary>
        /// Метод отрабатывает каждый кадр
        /// </summary>
        void OnGUI()
        {
            EditorGUILayout.HelpBox("Are you sure you want to download the changes?", MessageType.Info);
            Repaint();
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Yes"))
            {
                TableStudying.Download();
                this.Close();
            }
            if (GUILayout.Button("No"))
            {
                this.Close();
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Окно создания таблицы 
    /// </summary>
    public class CreateClass : EditorWindow
    {
        /// <summary>
        /// Ссылка на окно TableStudying
        /// </summary>
        public TableStudying TableStudying;

        /// <summary>
        /// Имя таблицы
        /// </summary>
        private string _nameTable;

        /// <summary>
        /// Метод отрабатывает каждый кадр
        /// </summary>
        void OnGUI()
        {
            _nameTable = EditorGUILayout.TextField(_nameTable, GUILayout.ExpandWidth(false));
            if (_nameTable == null)
            {
                EditorGUILayout.HelpBox("Write name you table", MessageType.Warning);
                Repaint();
            }

            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Yes"))
            {
                if (_nameTable != null)
                {
                   TableStudying.CreateTable(_nameTable);
                   this.Close();
                }

            }
            if (GUILayout.Button("No"))
            {
                this.Close();
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Окно таблицы 
    /// </summary>
    public class TableStudying : EditorWindow
    {
        /// <summary>
        /// Список с данными
        /// </summary>
        public ListInventoryItemStuding ListInventoryItemStuding;
        /// <summary>
        /// Главный контроллер
        /// </summary>
        public GameController GameController;
        /// <summary>
        /// Главный менеджер
        /// </summary>
        public GameManager GameManager;
        //Номер удаляемой строки
        private int _numberDelete;
        //Номер добавляемой строки 
        private int _numberAdd;

        /// <summary>
        /// Длина 
        /// </summary>
        private float _width;

        /// <summary>
        /// Позиция скроллинга
        /// </summary>
        private Vector2 _scrollPosition = Vector2.zero;

        /// <summary>
        /// Инициализация окна
        /// </summary>
        [MenuItem("Window/TableStudy %#r")] // атрибут добавляет кнопку в меню Window
        static void Init()
        {
            GetWindow(typeof(TableStudying));
        }

        /// <summary>
        /// Срабатывает ри включение скрипта
        /// </summary>
        void OnEnable()
        {
            if (EditorPrefs.HasKey("ObjectPath"))
            {
                string objectPath = EditorPrefs.GetString("ObjectPath");
                ListInventoryItemStuding = AssetDatabase.LoadAssetAtPath(objectPath,typeof(ListInventoryItemStuding)) as ListInventoryItemStuding;
            }
        }

        /// <summary>
        /// Проверяет существование файла в папке
        /// </summary>
        /// <returns>Возвращает значение  true/false существует файл или нет</returns>
        bool Checking()
        {
            if (EditorPrefs.HasKey("ObjectPath"))
            {
                string objectPath = EditorPrefs.GetString("ObjectPath");
                var a = AssetDatabase.LoadAssetAtPath(objectPath,typeof(ListInventoryItemStuding)) as ListInventoryItemStuding;
                if (a == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Проверяет существование файла, если он отсутствует, создается чмстый список
        /// </summary>
        void CheckingList()
        {
            if (EditorPrefs.HasKey("ObjectPath"))
            {
                string objectPath = EditorPrefs.GetString("ObjectPath");
                var a = AssetDatabase.LoadAssetAtPath(objectPath,typeof(ListInventoryItemStuding)) as ListInventoryItemStuding;
                if (a == null)
                {
                    ListInventoryItemStuding = null;
                }
            }
        }

        /// <summary>
        /// Метод отрабатывает каждый кадр, для отрисовки
        /// </summary>
        void OnGUI()
        {
            if (_width == null || _width == 0)
                _width = 130f;

            GUILayout.BeginArea(new Rect(5,5, 500, 50));
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reload", GUILayout.ExpandWidth(false))) // TODO: change
            {
                OnEnable();
            }
            if (GUILayout.Button("Download changes", GUILayout.ExpandWidth(false)))
            {
                DownloadClass window = GetWindow(typeof(DownloadClass), true, "Downloading") as DownloadClass;
                window.TableStudying = this;
                window.Show();
            }
            if (GUILayout.Button("Save changes", GUILayout.ExpandWidth(false)))
            {
                SaveClass window = GetWindow(typeof(SaveClass), true, "Saving") as SaveClass;
                window.TableStudying = this;
                window.Show();
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Open Table", GUILayout.ExpandWidth(false)))
            {
                OpenTable();
            }
            if (GUILayout.Button("Create Table", GUILayout.ExpandWidth(false)))
            {
                CreateClass window = GetWindow(typeof (CreateClass), true, "Name Table") as CreateClass;
                window.TableStudying = this;
                window.Show();
            }
            if (GUILayout.Button("Show Item List", GUILayout.ExpandWidth(false)))
            {
                if (ListInventoryItemStuding != null)
                {
                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = ListInventoryItemStuding;
                }
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.EndArea();

            CheckingList();

            GUILayout.BeginArea(new Rect(0, 50, position.width, position.height));

            Handles.BeginGUI();
            Handles.DrawLine(new Vector3(0, 0),new Vector3(position.width, 0));
            Handles.EndGUI();

            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.Width(position.width), GUILayout.Height(position.height-50));

                if (GameController == null)
                {
                    GameController = FindObjectOfType<GameController>().GetComponent<GameController>();
                }
                else
                {
                    if (ListInventoryItemStuding == null)
                    {
                        EditorGUILayout.HelpBox("Open the table or create a new one. I can not read date!!",MessageType.Warning);
                        Repaint();
                    }
                    else
                    {
                        GUILayout.BeginHorizontal();
                        if (GUILayout.Button("Add Element number", GUILayout.ExpandWidth(false)))
                        {
                            if (ListInventoryItemStuding != null)
                                ListInventoryItemStuding.InventoryItem.Insert(_numberAdd, new InventoryItemStuding());
                        }
                        GUILayout.Label("Number Element", EditorStyles.boldLabel, GUILayout.Width(_width));
                        _numberAdd = EditorGUILayout.IntField(_numberAdd, GUILayout.Width(_width / 3));
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        if (GUILayout.Button("Delete Element", GUILayout.ExpandWidth(false)))
                        {
                            DeleteItem(_numberDelete);
                        }
                        GUILayout.Label("Number Element", EditorStyles.boldLabel, GUILayout.Width(_width));
                        _numberDelete = EditorGUILayout.IntField(_numberDelete, GUILayout.Width(_width / 3));
                        GUILayout.EndHorizontal();
                        GUILayout.Space(10);

                        GUILayout.Label("List Settings", EditorStyles.boldLabel);

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width / 3));
                        GUILayout.Label("GameObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
                        GUILayout.Label("BackLighting", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("IsGhilds", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("TargetTransformRotation", EditorStyles.boldLabel, GUILayout.Width(_width));

                        GUILayout.Label("ButtonObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationOperation", EditorStyles.boldLabel, GUILayout.Width(_width));

                        GUILayout.Label("Defect", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("DefectSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("DefIsChild", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.EndHorizontal();

                    //Отображение списка на экране
                        for (int i = 0; i < ListInventoryItemStuding.InventoryItem.Count; i++)
                        {
                            GUILayout.BeginHorizontal();
                            ListInventoryItemStuding.InventoryItem[i].NumberElement =
                                EditorGUILayout.IntField(i, GUILayout.Width(_width / 3));

                            ListInventoryItemStuding.InventoryItem[i].GameObject = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].GameObject, typeof(GameObject), true,
                                    GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].Subtitle = EditorGUILayout.TextField(
                                ListInventoryItemStuding.InventoryItem[i].Subtitle, GUILayout.Width(_width));

                            ListInventoryItemStuding.InventoryItem[i].AudioClip = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].AudioClip, typeof(AudioClip),
                                    GUILayout.Width(_width/1.5f)) as AudioClip;

                            ListInventoryItemStuding.InventoryItem[i].IsPlay = EditorGUILayout.Toggle(
                                ListInventoryItemStuding.InventoryItem[i].IsPlay, GUILayout.Width(_width/2));

                            ListInventoryItemStuding.InventoryItem[i].BackLighting = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].BackLighting, typeof(GameObject), true,
                                    GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].IsChilds = EditorGUILayout.Toggle(
                                ListInventoryItemStuding.InventoryItem[i].IsChilds, GUILayout.Width(_width/1.5f));

                            ListInventoryItemStuding.InventoryItem[i].IsPosition = EditorGUILayout.Toggle(
                                ListInventoryItemStuding.InventoryItem[i].IsPosition, GUILayout.Width(_width/1.5f));

                            ListInventoryItemStuding.InventoryItem[i].ActionObject = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].ActionObject, typeof(GameObject), true,
                                    GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].TargetWalk = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].TargetWalk, typeof(GameObject), true,
                                    GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].NameAnimationWalk = EditorGUILayout.TextField(
                                ListInventoryItemStuding.InventoryItem[i].NameAnimationWalk, GUILayout.Width(_width));

                            ListInventoryItemStuding.InventoryItem[i].NameAnimationIdle = EditorGUILayout.TextField(
                                ListInventoryItemStuding.InventoryItem[i].NameAnimationIdle, GUILayout.Width(_width));

                            ListInventoryItemStuding.InventoryItem[i].TargetMonterRotation =
                                EditorGUILayout.ObjectField(
                                    ListInventoryItemStuding.InventoryItem[i].TargetMonterRotation, typeof(GameObject),
                                    true, GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].ButtonObject = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].ButtonObject, typeof(GameObject), true,
                                    GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].ZoneOperation = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].ZoneOperation, typeof(GameObject), true,
                                GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].NameAnimationOperation =
                                EditorGUILayout.TextField(
                                    ListInventoryItemStuding.InventoryItem[i].NameAnimationOperation,
                                        GUILayout.Width(_width));

                            ListInventoryItemStuding.InventoryItem[i].Defect = EditorGUILayout.ObjectField(
                                ListInventoryItemStuding.InventoryItem[i].Defect, typeof(GameObject), true,
                                        GUILayout.Width(_width)) as GameObject;

                            ListInventoryItemStuding.InventoryItem[i].DefectAudioClip =
                                EditorGUILayout.ObjectField(ListInventoryItemStuding.InventoryItem[i].DefectAudioClip,
                                    typeof(AudioClip), GUILayout.Width(_width/1.5f)) as AudioClip;

                            ListInventoryItemStuding.InventoryItem[i].DefectIsPlay = EditorGUILayout.Toggle(
                                ListInventoryItemStuding.InventoryItem[i].DefectIsPlay, GUILayout.Width(_width/1.5f));

                            ListInventoryItemStuding.InventoryItem[i].DefectSubtitle = EditorGUILayout.TextField(
                                ListInventoryItemStuding.InventoryItem[i].DefectSubtitle,
                                    GUILayout.Width(_width));

                            ListInventoryItemStuding.InventoryItem[i].DefectIsChild = EditorGUILayout.Toggle(
                                ListInventoryItemStuding.InventoryItem[i].DefectIsChild, GUILayout.Width(_width/1.5f));

                            GUILayout.EndHorizontal();
                        }

                        GUILayout.BeginHorizontal();

                        GUILayout.Label("№", EditorStyles.boldLabel, GUILayout.Width(_width / 3));
                        GUILayout.Label("GameObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("Subtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("AudioClip", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("IsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/2));
                        GUILayout.Label("BackLighting", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("IsGhilds", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("IsPosition", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("ActionObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("TargetWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationWalk", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationIdle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("TargetTransformRotation", EditorStyles.boldLabel, GUILayout.Width(_width));

                        GUILayout.Label("ButtonObject", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("ZoneOperation", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("NameAnimationOperation", EditorStyles.boldLabel, GUILayout.Width(_width));

                        GUILayout.Label("Defect", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("DefAudio", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("DefIsPlay", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));
                        GUILayout.Label("DefectSubtitle", EditorStyles.boldLabel, GUILayout.Width(_width));
                        GUILayout.Label("DefIsChild", EditorStyles.boldLabel, GUILayout.Width(_width/1.5f));

                        GUILayout.EndHorizontal();
                    //Кнопка добавление строки данных
                        if (GUILayout.Button("Add Element", GUILayout.ExpandWidth(false)))
                        {
                            if (ListInventoryItemStuding != null)
                                ListInventoryItemStuding.InventoryItem.Add(new InventoryItemStuding());
                        }
                    }
                }
            GUILayout.EndScrollView();
            GUILayout.EndArea();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(ListInventoryItemStuding);
            }
        }

        /// <summary>
        /// Открытие таблицы
        /// </summary>
        void OpenTable()
        {
            string path = EditorUtility.OpenFilePanel("Select Table", "", "");
            if (path.StartsWith(Application.dataPath))
            {
                string relpath = path.Substring(Application.dataPath.Length - "Assets".Length);
                ListInventoryItemStuding = AssetDatabase.LoadAssetAtPath(relpath, typeof(ListInventoryItemStuding)) as ListInventoryItemStuding;
                if (ListInventoryItemStuding == null)
                {
                    Debug.Log("NUUUUUULL");
                    ListInventoryItemStuding= new ListInventoryItemStuding();
                }

                if (ListInventoryItemStuding)
                    EditorPrefs.SetString("ObjectPath", relpath);
            }
        }

        /// <summary>
        /// Удаление строки данных 
        /// </summary>
        /// <param name="index">Номер строки</param>
        void DeleteItem (int index)
        {
            if (ListInventoryItemStuding.InventoryItem.Count != 0)
                if (ListInventoryItemStuding.InventoryItem.Count -1 >= index)
                    ListInventoryItemStuding.InventoryItem.RemoveAt (index);
        }

        /// <summary>
        /// Сохронение данных в главном контроллере в сцене
        /// </summary>
        public void Save()
        {
            if (GameController == null)
                GameController = FindObjectOfType<GameController>().GetComponent<GameController>();

            GameController.ListLook = new List<Transform>();
            GameController.ListTarget = new List<Transform>();
            GameController.ListSubtitle = new List<string>();
            GameController.ListAudiclips = new List<AudioClip>();
            GameController.ListIsPlayThenEmpty = new List<bool>();
            GameController.ListHightlightObjects = new List<GameObject>();
            GameController.ListIsChild = new List<bool>();
            GameController.ListIsPosition = new List<bool>();
            GameController.ListActionObjects = new List<GameObject>();
            GameController.ListTargetWalk = new List<Transform>();
            GameController.ListNameAnimationWalk = new List<string>();
            GameController.ListNameAnimationIdle = new List<string>();
            GameController.ListButtonObject = new List<GameObject>();
            GameController.ListZoneOperations = new List<GameObject>();
            GameController.ListNameAnimation = new List<string>();
            GameController.ListTransformRotation = new List<Transform>();
            GameController.ListDefects = new List<GameObject>();
            GameController.ListDefectAudioClips = new List<AudioClip>();
            GameController.ListDefectIsPlayThenEmpty = new List<bool>();
            GameController.ListDefectSubtitles = new List<string>();
            GameController.ListDefectIsChilds = new List<bool>();

            if (ListInventoryItemStuding != null)
            {
                for (int i = 0; i < ListInventoryItemStuding.InventoryItem.Count; i++)
                {
                    if (ListInventoryItemStuding.InventoryItem[i].GameObject != null)
                    {
                        GameController.ListLook.Add(ListInventoryItemStuding.InventoryItem[i]
                            .GameObject.GetComponent<Transform>());
                        GameController.ListTarget.Add(ListInventoryItemStuding.InventoryItem[i]
                            .GameObject.transform.GetChild(0));
                    }
                    else
                    {
                        GameController.ListLook.Add(null);
                        GameController.ListTarget.Add(null);
                    }
                    GameController.ListSubtitle.Add(ListInventoryItemStuding.InventoryItem[i].Subtitle);
                    GameController.ListAudiclips.Add(ListInventoryItemStuding.InventoryItem[i].AudioClip);
                    GameController.ListIsPlayThenEmpty.Add(ListInventoryItemStuding.InventoryItem[i].IsPlay);
                    GameController.ListHightlightObjects.Add(ListInventoryItemStuding.InventoryItem[i].BackLighting);
                    GameController.ListIsChild.Add(ListInventoryItemStuding.InventoryItem[i].IsChilds);
                    GameController.ListIsPosition.Add(ListInventoryItemStuding.InventoryItem[i].IsPosition);
                    GameController.ListActionObjects.Add(ListInventoryItemStuding.InventoryItem[i].ActionObject);
                    if (ListInventoryItemStuding.InventoryItem[i].TargetWalk != null)
                        GameController.ListTargetWalk.Add(ListInventoryItemStuding.InventoryItem[i]
                            .TargetWalk.GetComponent<Transform>());
                    else GameController.ListTargetWalk.Add(null);
                    GameController.ListNameAnimationWalk.Add(ListInventoryItemStuding.InventoryItem[i].NameAnimationWalk);
                    GameController.ListNameAnimationIdle.Add(
                        ListInventoryItemStuding.InventoryItem[i].NameAnimationIdle);
                    GameController.ListButtonObject.Add(ListInventoryItemStuding.InventoryItem[i].ButtonObject);
                    GameController.ListZoneOperations.Add(ListInventoryItemStuding.InventoryItem[i].ZoneOperation);
                    GameController.ListNameAnimation.Add(ListInventoryItemStuding.InventoryItem[i]
                        .NameAnimationOperation);
                    if (ListInventoryItemStuding.InventoryItem[i].TargetMonterRotation != null)
                        GameController.ListTransformRotation.Add(ListInventoryItemStuding.InventoryItem[i]
                            .TargetMonterRotation.GetComponent<Transform>());
                    else GameController.ListTransformRotation.Add(null);
                    GameController.ListDefects.Add(ListInventoryItemStuding.InventoryItem[i].Defect);
                    GameController.ListDefectAudioClips.Add(ListInventoryItemStuding.InventoryItem[i].DefectAudioClip);
                    GameController.ListDefectIsPlayThenEmpty.Add(ListInventoryItemStuding.InventoryItem[i].DefectIsPlay);
                    GameController.ListDefectSubtitles.Add(ListInventoryItemStuding.InventoryItem[i].DefectSubtitle);
                    GameController.ListDefectIsChilds.Add(ListInventoryItemStuding.InventoryItem[i].DefectIsChild);
                }
            }
            PrefabUtility.ReplacePrefab(GameController.gameObject, PrefabUtility.GetPrefabParent(
                GameController.gameObject), ReplacePrefabOptions.ConnectToPrefab);
        }

        /// <summary>
        /// Загрузка данных из главного контроллера сцены
        /// </summary>
        public void Download()
        {
            if (GameController == null)
                GameController = FindObjectOfType<GameController>().GetComponent<GameController>();

//            GameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();

            if (ListInventoryItemStuding != null)
            {
                for (int i = 0; i < GameController.ListLook.Count; i++)
                {
                    if (ListInventoryItemStuding.InventoryItem.Count == i)
                    {
                        ListInventoryItemStuding.InventoryItem.Add(new InventoryItemStuding());
                    }
                    if (GameController.ListLook[i] != null)
                        ListInventoryItemStuding.InventoryItem[i].GameObject = GameController.ListLook[i].gameObject;
                    else ListInventoryItemStuding.InventoryItem[i].GameObject = null;
                    ListInventoryItemStuding.InventoryItem[i].Subtitle = GameController.ListSubtitle[i];
                    ListInventoryItemStuding.InventoryItem[i].AudioClip = GameController.ListAudiclips[i];
                    ListInventoryItemStuding.InventoryItem[i].IsPlay = GameController.ListIsPlayThenEmpty[i];
                    ListInventoryItemStuding.InventoryItem[i].IsChilds = GameController.ListIsChild[i];
                    ListInventoryItemStuding.InventoryItem[i].IsPosition = GameController.ListIsPosition[i];
                    ListInventoryItemStuding.InventoryItem[i].BackLighting = GameController.ListHightlightObjects[i];
                    ListInventoryItemStuding.InventoryItem[i].ActionObject = GameController.ListActionObjects[i];
                    if (GameController.ListTargetWalk[i] != null)
                        ListInventoryItemStuding.InventoryItem[i].TargetWalk =
                            GameController.ListTargetWalk[i].gameObject;
                    else ListInventoryItemStuding.InventoryItem[i].TargetWalk = null;
                    ListInventoryItemStuding.InventoryItem[i].NameAnimationWalk =
                        GameController.ListNameAnimationWalk[i];
                    ListInventoryItemStuding.InventoryItem[i].NameAnimationIdle =
                        GameController.ListNameAnimationIdle[i];
                    ListInventoryItemStuding.InventoryItem[i].ButtonObject = GameController.ListButtonObject[i];
                    ListInventoryItemStuding.InventoryItem[i].ZoneOperation = GameController.ListZoneOperations[i];
                    ListInventoryItemStuding.InventoryItem[i].NameAnimationOperation =
                        GameController.ListNameAnimation[i];
//                    if (GameController.ListTransformRotation[i] != null)
//                        ListInventoryItemStuding.InventoryItem[i].TargetMonterRotation =
//                            GameController.ListTransformRotation[i].gameObject;
//                    else ListInventoryItemStuding.InventoryItem[i].TargetMonterRotation = null;
                    ListInventoryItemStuding.InventoryItem[i].Defect = GameController.ListDefects[i];
                    ListInventoryItemStuding.InventoryItem[i].DefectAudioClip = GameController.ListDefectAudioClips[i];
                    ListInventoryItemStuding.InventoryItem[i].DefectIsPlay = GameController.ListDefectIsPlayThenEmpty[i];
                    ListInventoryItemStuding.InventoryItem[i].DefectSubtitle = GameController.ListDefectSubtitles[i];
                    ListInventoryItemStuding.InventoryItem[i].DefectIsChild = GameController.ListDefectIsChilds[i];
                }
            }
        }

        /// <summary>
        /// Создание таблицы данных
        /// </summary>
        /// <param name="name">Имя создаваймого файла</param>
        public void CreateTable(string name) // TODO: change
        {
            ListInventoryItemStuding asset = ScriptableObject.CreateInstance<ListInventoryItemStuding>();

            AssetDatabase.CreateAsset(asset, "Assets/Data/Studying/" + name +".asset");
            AssetDatabase.SaveAssets();
        }
    }
}